/*
 Navicat Premium Data Transfer

 Source Server         : phpmyadmin
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : edstock_db

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 12/11/2023 15:23:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modifed` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (4, 'Expendable', '2023-11-12 12:56:42', '2023-11-12 12:56:42', NULL);
INSERT INTO `categories` VALUES (5, 'Semi-Expendable', '2023-11-12 12:56:42', '2023-11-12 12:56:42', NULL);
INSERT INTO `categories` VALUES (6, 'Office Supply', '2023-11-12 21:01:31', '2023-11-12 13:01:31', NULL);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modifed` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (1, 'Accounting Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (2, 'Administrative Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (3, 'Supply Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (4, 'Curriculum Implementation Division', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (5, 'Schools Governance and Operations Division', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (6, 'Cashier Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (7, 'Budget Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (8, 'Human Resource Management Office', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (9, 'OSDS', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (10, 'OASDS', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);

-- ----------------------------
-- Table structure for fund_clusters
-- ----------------------------
DROP TABLE IF EXISTS `fund_clusters`;
CREATE TABLE `fund_clusters`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fund_clusters
-- ----------------------------
INSERT INTO `fund_clusters` VALUES (1, 'DepED Region II - 10001-01', '2023-10-11 03:37:37', '2023-11-12 13:10:06', NULL);
INSERT INTO `fund_clusters` VALUES (2, 'MOOE', '2023-10-15 17:35:58', '2023-11-12 13:10:06', NULL);
INSERT INTO `fund_clusters` VALUES (3, 'Central Office', '2023-10-15 17:36:07', '2023-11-12 13:10:06', NULL);

-- ----------------------------
-- Table structure for heads
-- ----------------------------
DROP TABLE IF EXISTS `heads`;
CREATE TABLE `heads`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of heads
-- ----------------------------
INSERT INTO `heads` VALUES (1, 'JAMES PAUL R. SANTIAGO', 'Administrative Officer III', '2023-11-03 22:50:11', '2023-11-03 22:50:11', NULL);
INSERT INTO `heads` VALUES (2, 'FLORDELIZA R. GECOBE', 'Schools Division Superintendent', '2023-11-03 22:50:49', '2023-11-07 09:19:04', NULL);

-- ----------------------------
-- Table structure for iars
-- ----------------------------
DROP TABLE IF EXISTS `iars`;
CREATE TABLE `iars`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `divprofile_id` int NOT NULL,
  `fcluster_id` int NOT NULL,
  `supplier_id` int NOT NULL,
  `department_id` int NOT NULL,
  `rcc` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `iarno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `invoiceno` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` date NOT NULL,
  `purchaseorder_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iars
-- ----------------------------

-- ----------------------------
-- Table structure for inventories
-- ----------------------------
DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT current_timestamp,
  `iar_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventories
-- ----------------------------
INSERT INTO `inventories` VALUES (6, '2023-10-15 10:07:40', 0);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category_id` int NOT NULL,
  `unit_id` int NOT NULL,
  `price` double NOT NULL,
  `stock` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (19, 'EPSON PRinter 1129', 4, 1, 5000, 90, '2023-11-12 11:13:18', '2023-11-12 22:47:37', NULL);
INSERT INTO `items` VALUES (20, 'EPSON Ink', 4, 2, 1000, 0, '2023-11-12 11:13:18', '2023-11-12 11:13:18', NULL);
INSERT INTO `items` VALUES (22, 'Wooden Table ', 5, 9, 15000, 0, '2023-11-12 11:13:18', '2023-11-12 11:13:18', NULL);

-- ----------------------------
-- Table structure for methods
-- ----------------------------
DROP TABLE IF EXISTS `methods`;
CREATE TABLE `methods`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of methods
-- ----------------------------
INSERT INTO `methods` VALUES (1, 'CHECK', '2023-11-08 08:04:58', '2023-11-08 08:04:58', NULL);
INSERT INTO `methods` VALUES (2, 'CASH', '2023-11-08 08:05:26', '2023-11-08 08:05:26', NULL);

-- ----------------------------
-- Table structure for offices
-- ----------------------------
DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of offices
-- ----------------------------
INSERT INTO `offices` VALUES (7, 'SDO - Santiago City', '2023-11-12 13:28:57', '2023-11-12 13:28:57', NULL);

-- ----------------------------
-- Table structure for orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE `orderdetails`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `item_id` int NOT NULL,
  `cost` double NOT NULL,
  `qty` int NOT NULL,
  `total` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderdetails
-- ----------------------------

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `request_id` int NOT NULL,
  `office_id` int NOT NULL,
  `supplier_id` int NOT NULL,
  `fund_cluster_id` int NULL DEFAULT NULL,
  `po_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method_id` int NOT NULL,
  `place_of_delivery` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_of_delivery` datetime NOT NULL,
  `delivery_term` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `payment_term` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fund_available` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ors_burs_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_of_burs` datetime NOT NULL,
  `deleted` datetime NULL DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for plandetails
-- ----------------------------
DROP TABLE IF EXISTS `plandetails`;
CREATE TABLE `plandetails`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `plan_id` int NOT NULL,
  `code` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `item_id` int NOT NULL,
  `qty` int NULL DEFAULT NULL,
  `cost` double NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `jan` int NULL DEFAULT NULL,
  `feb` int NULL DEFAULT NULL,
  `mar` int NULL DEFAULT NULL,
  `apr` int NULL DEFAULT NULL,
  `may` int NULL DEFAULT NULL,
  `jun` int NULL DEFAULT NULL,
  `jul` int NULL DEFAULT NULL,
  `aug` int NULL DEFAULT NULL,
  `sep` int NULL DEFAULT NULL,
  `oct` int NULL DEFAULT NULL,
  `nov` int NULL DEFAULT NULL,
  `decm` int NULL DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of plandetails
-- ----------------------------

-- ----------------------------
-- Table structure for plans
-- ----------------------------
DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `prepared_by` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of plans
-- ----------------------------

-- ----------------------------
-- Table structure for requestdetails
-- ----------------------------
DROP TABLE IF EXISTS `requestdetails`;
CREATE TABLE `requestdetails`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `request_id` int NOT NULL,
  `item_id` int NOT NULL,
  `cost` double NOT NULL,
  `qty` int NOT NULL,
  `total` int NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp,
  `modified` datetime NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of requestdetails
-- ----------------------------

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `office_id` int NOT NULL,
  `fund_cluster_id` int NOT NULL,
  `department_id` int NOT NULL,
  `pr_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `responsibility_center_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `requester` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `approver` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int NULL DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp,
  `modified` datetime NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of requests
-- ----------------------------

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `description` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_stocks` double NOT NULL,
  `qty` double NOT NULL,
  `total_stocks` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of stocks
-- ----------------------------
INSERT INTO `stocks` VALUES (4, 19, 'IN', 0, 100, 100, '2023-11-12 22:47:14', '2023-11-12 22:47:14', NULL);
INSERT INTO `stocks` VALUES (5, 19, 'OUT', 100, 10, 90, '2023-11-12 22:47:37', '2023-11-12 22:47:37', NULL);

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tin_no` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modifed` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (1, 'SISTAN SUPPLIES DEPOT', 'Santiago City', '941-104-334-000', '2023-11-12 13:22:40', '2023-11-12 13:22:40', NULL);
INSERT INTO `suppliers` VALUES (2, 'A', 'A', '122142342342', '2023-11-12 13:22:40', '2023-11-12 13:22:40', NULL);
INSERT INTO `suppliers` VALUES (3, 'Asd', 'Asdas', 'ASD', '2023-11-12 21:27:49', '2023-11-12 13:27:49', NULL);

-- ----------------------------
-- Table structure for units
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES (1, 'pc', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (2, 'bottle', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (3, 'ream', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (4, 'box', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (5, 'liter', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (6, 'pack', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (7, 'person', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (8, 'roll', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (9, 'set', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (10, 'Tubes', '2023-11-12 13:04:49', '2023-11-12 21:06:20', NULL);
INSERT INTO `units` VALUES (11, 'Kg', '2023-11-12 21:06:28', '2023-11-12 21:06:28', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bdate` date NOT NULL,
  `tin_no` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `plantilla_no` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `contact` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `department_id` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp,
  `modified` timestamp NOT NULL DEFAULT current_timestamp,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (16, 'jepotsantiago', '$2y$10$MaJoettySeHnuuBeCwvC0O4rTkGhg76vG4H/sMVZ/5JK/nKxK4XOW', 'James Paul Ronquillo Santiago', '2023-10-08', '010101010101', '0987654321', 'Administrative Officer IV', 'Super Administrator', 1, '6550406fa4649999999996550406fa4663', '09338146244', 2, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);
INSERT INTO `users` VALUES (18, 'jeffcalaunan', '252829', 'Marc Jefferson Calaunan', '1996-03-21', '346686124', 'ADASII-2023-0014', 'Administrative Aide VI', 'Super Administrator', 1, NULL, '09173191479', 3, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);
INSERT INTO `users` VALUES (20, 'archietzy', '$2y$10$SyFPAuoAXe4Fwl7KvfALYOx1K7wFiJeyz170jr/qX0SnCAgfBiHnC', 'Archie Raven Molina Raga', '2002-05-26', '122142342342', '342344634234', 'Student', 'User', 1, '65504fd89cb539999999965504fd89cb6c', '09452247731', 3, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);

SET FOREIGN_KEY_CHECKS = 1;
