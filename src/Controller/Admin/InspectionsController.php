<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\Event\EventInterface;
use Cake\Routing\Router;
use Moment\Moment;

/**
 * Inspections Controller
 *
 * @property \App\Model\Table\InspectionsTable $Inspections
 * @method \App\Model\Entity\Inspection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InspectionsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user){

        if (!empty($user) && strtolower('Super Administrator') != strtolower($user['role'])) {
            throw new \Cake\Http\Exception\ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

    }

    public function getInspections(){
        $data = $this->Inspections->find()
            ->contain([
                'Requests' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Requests.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Suppliers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'InspectionItems' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->order(['Inspections.created' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function reports()
    {

    }

    public function getReports(){

        $startDate = (!empty($this->request->getQuery('start_date'))? date('Y-m-d', strtotime($this->request->getQuery('start_date'))): (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'));
        $endDate = (!empty($this->request->getQuery('end_date'))? date('Y-m-d', strtotime($this->request->getQuery('end_date'))): (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'));

        $data = $this->Inspections->find()
            ->where([
                'OR' => [
                    [
                        'Inspections.created >=' => $startDate,
                        'Inspections.created <=' => $endDate,
                    ],
                    [
                        'Inspections.created <=' => $startDate,
                        'Inspections.created >=' => $endDate
                    ]
                ]
            ])
            ->contain([
                'Requests' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Requests.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Suppliers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'InspectionItems' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->order(['Inspections.created' => 'DESC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function printInspection($id=null)
    {
        $this->loadModel('Heads');
        $this->viewBuilder()->setLayout('pdf');
        $coordinator = $this->Heads->find()->where(['NOT'=>['position'=>'Requesting Officer']])->first();
        $head = $this->Heads->find()->where(['position'=>'Approving Officer'])->first();
        $inspection = $this->Inspections->get($id, [
            'contain' => [
                'Requests' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Requests.Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'FundClusters' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Offices' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orders.Suppliers' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orders.Methods' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'InspectionItems' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'InspectionItems.Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'InspectionItems.Items.Units' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
            ],
        ]);
        $this->set(compact( 'inspection','head','coordinator'));
    }

    /**
     * View method
     *
     * @param string|null $id Inspection id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inspection = $this->Inspections->get($id, [
            'contain' => ['Requests', 'Orders', 'FundClusters', 'Suppliers', 'Offices', 'InspectionItems'],
        ]);

        $this->set(compact('inspection'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($orderId = null, $requestId = null)
    {
        $inspection = $this->Inspections->newEmptyEntity();
        $order = $this->getTableLocator()->get('Orders')->get(intval($orderId),[
            'contain' => [
                'Orderdetails' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orderdetails.Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orderdetails.Items.Units' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ]
        ]);
        $request = $this->getTableLocator()->get('Requests')->get(intval($requestId),[
            'contain' => [
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ]
        ]);
        if ($this->request->is('post')) {

            $inspectionItems = (new Collection($order->orderdetails))->map(function ($value, $key){
                return [
                    'item_id' => intval($value['item_id']),
                    'quantity' => intval($value['qty']),
                    'cost' => intval($value['cost']),
                    'total' => intval($value['total']),
                ];
            });

            $data = (new Collection([$this->request->getData()]))->map(function($value, $key) use ($inspectionItems){
                $value['inspection_items'] = $inspectionItems->toArray();
                return $value;
            });

            $inspection = $this->Inspections->patchEntity($inspection, $data->first());

            $inspection->iar_no = $request->series_number;

            if ($this->Inspections->save($inspection,['associated' => ['InspectionItems']])) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $inspection->id, json_encode($inspection));
                $result = ['message' => ucwords('the Inspection has been saved'), 'result' => ucwords('success'),
                    'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'add', $inspection->request_id, $inspection->order_id, $inspection->id])];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
              foreach ($inspection->getErrors() as $key => $value){
                  $result = ['message' => ucwords(reset($value)), 'result' => ucwords('success')];
                  return $this->response->withStatus(422)->withType('application/json')
                      ->withStringBody(json_encode($result));
              }
            }

        }
        $fundClusters = $this->Inspections->FundClusters->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'FundClusters.id =' => intval($request->fund_cluster_id)
        ])->order(['FundClusters.name' => 'ASC']);
        $suppliers = $this->Inspections->Suppliers->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])
        ->where([
            'Suppliers.id =' => intval($order->supplier_id)
        ])
        ->order(['Suppliers.name' => 'ASC']);
        $offices = $this->Inspections->Offices->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])        ->where([
            'Offices.id =' => intval($order->office_id)
        ])
            ->order(['Offices.name' => 'ASC']);;
        $this->set(compact('inspection', 'request', 'order', 'fundClusters', 'suppliers', 'offices'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Inspection id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $inspection = $this->Inspections->get($id, [
            'contain' => [
                'Requests' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orders' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ],
        ]);
        $order = $this->getTableLocator()->get('Orders')->get(intval($inspection->order_id),[
            'contain' => [
                'Orderdetails' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orderdetails.Items' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Orderdetails.Items.Units' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ]
        ]);
        $request = $this->getTableLocator()->get('Requests')->get(intval($inspection->request_id),[
            'contain' => [
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $inspection = $this->Inspections->patchEntity($inspection, $this->request->getData());
            if ($this->Inspections->save($inspection)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $inspection->id, json_encode($inspection));
                $result = ['message' => ucwords('the Inspection has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($inspection->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('success')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $fundClusters = $this->Inspections->FundClusters->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'FundClusters.id =' => intval($request->fund_cluster_id)
        ])->order(['FundClusters.name' => 'ASC']);;
        $suppliers = $this->Inspections->Suppliers->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])
            ->where([
                'Suppliers.id =' => intval($order->supplier_id)
            ])
            ->order(['Suppliers.name' => 'ASC']);
        $offices = $this->Inspections->Offices->find('list',[
            'valueField' => function($query){
                return strtoupper($query->name);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])        ->where([
            'Offices.id =' => intval($order->office_id)
        ])
            ->order(['Offices.name' => 'ASC']);;
        $this->set(compact('inspection', 'request', 'order', 'fundClusters', 'suppliers', 'offices'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Inspection id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $inspection = $this->Inspections->get($id);
        if ($this->Inspections->delete($inspection)) {
            $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $inspection->id, json_encode($inspection));
            $this->Flash->success(__('The inspection has been deleted.'));
        } else {
            $this->Flash->error(__('The inspection could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
