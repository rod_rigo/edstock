<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PlanTitles Controller
 *
 * @property \App\Model\Table\PlanTitlesTable $PlanTitles
 * @method \App\Model\Entity\PlanTitle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlanTitlesController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user){

        if (!empty($user) && strtolower('Super Administrator') != strtolower($user['role'])) {
            throw new \Cake\Http\Exception\ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function getPlanTitles($planId = null){
        $data = $this->PlanTitles->find()
            ->contain([
                'Parts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->where([
                'PlanTitles.plan_id' => intval($planId)
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function getPlanTitlesLists($planId = null, $partId = null){
        $data = $this->PlanTitles->find('list',[
                'valueField' => function($query){
                    return strtoupper($query->plan_title);
                },
                'keyField' => function($query){
                    return intval($query->id);
                }
            ])
            ->contain([
                'Parts' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ]
            ])
            ->where([
                'PlanTitles.plan_id' => intval($planId),
                'PlanTitles.part_id' => intval($partId)
            ])->order(['PlanTitles.plan_title' => 'ASC']);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $planTitle = $this->PlanTitles->newEmptyEntity();
        if ($this->request->is('post')) {
            $planTitle = $this->PlanTitles->patchEntity($planTitle, $this->request->getData());
            if ($this->PlanTitles->save($planTitle)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $planTitle->id, json_encode($planTitle));
                $result = ['message' => ucwords('The plan title has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($planTitle->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Plan Title id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $planTitle = $this->PlanTitles->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $planTitle = $this->PlanTitles->patchEntity($planTitle, $this->request->getData());
            if ($this->PlanTitles->save($planTitle)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $planTitle->id, json_encode($planTitle));
                $result = ['message' => ucwords('The plan title has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($planTitle->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withStatus(200)->withType('application/json')
            ->withStringBody(json_encode($planTitle));
    }

    /**
     * Delete method
     *
     * @param string|null $id Plan Title id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $planTitle = $this->PlanTitles->get($id);
        if ($this->PlanTitles->delete($planTitle)) {
            $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $planTitle->id, json_encode($planTitle));
            $result = ['message' => ucwords('The plan title has been deleted.'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The plan title has been deleted.'), 'result' => ucwords('success')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }
}
