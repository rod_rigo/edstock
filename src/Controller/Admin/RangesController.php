<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\EventInterface;

/**
 * Ranges Controller
 *
 * @property \App\Model\Table\RangesTable $Ranges
 * @method \App\Model\Entity\Range[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RangesController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('admin');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user){

        if (!empty($user) && strtolower('Super Administrator') != strtolower($user['role'])) {
            throw new \Cake\Http\Exception\ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Ranges->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getRanges(){
        $data = $this->Ranges->find();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }


    public function add()
    {
        $range = $this->Ranges->newEmptyEntity();
        if ($this->request->is('post')) {
            $range = $this->Ranges->patchEntity($range, $this->request->getData());
            if ($this->Ranges->save($range)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $range->id, json_encode($range));
                $result = ['message' => ucwords('The range has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($range->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $this->set(compact('range'));
    }


    public function edit($id = null)
    {
        $range = $this->Ranges->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $range = $this->Ranges->patchEntity($range, $this->request->getData());
            if ($this->Ranges->save($range)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $range->id, json_encode($range));
                $result = ['message' => ucwords('The range has been saved.'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($range->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($range));
    }

    /**
     * Delete method
     *
     * @param string|null $id Range id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $range = $this->Ranges->get($id);
        if ($this->Ranges->delete($range)) {
            $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $range->id, json_encode($range));
            $result = ['message' => ucwords('The range has been deleted.'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The range could not be deleted. Please, try again.'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

}
