<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Http\Exception\ForbiddenException;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginRedirect' => [
                'prefix' => 'Admin',
                'controller' => 'Users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'authError' => ucwords('Authentication Required'),
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'userModel' => 'Users',
                    'finder' => 'auth',
                ]
            ],
            'storage' => 'Session',
            'unauthorizedRedirect' => Router::url(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])
        ]);

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Activities');

        $auth = $this->Auth->user();

        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');

        $this->set(compact('auth', 'controller', 'action'));

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }

    public function isAuthorized($user) {

        if($this->request->getParam('prefix') == 'Admin'){

           if (!empty($user) && strtolower('Super Administrator') == strtolower($user['role'])) {
                return true;
            } 

            throw new ForbiddenException(__('Forbidden Action!'));
        }

        if($this->request->getParam('prefix') == 'Personnel'){

            if (!empty($user) && strtolower('User') == strtolower($user['role'])) {
                return true;
            }

            throw new ForbiddenException(__('Forbidden Action!'));
        }

        if($this->request->getParam('prefix') == 'Secretariat'){

            if (!empty($user) && strtolower('Administrator') == strtolower($user['role'])) {
                return true;
            }

            throw new ForbiddenException(__('Forbidden Action!'));
        }
    }

}
