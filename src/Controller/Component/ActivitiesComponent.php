<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Activities component
 */
class ActivitiesComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array<string, mixed>
     */
    protected $_defaultConfig = [];

    public function activity($userId = null, $prefix = null, $controller = null, $action = null, $dataKey = null, $data = null){

        try{
            $activity = TableRegistry::getTableLocator()->get('Activities')->newEmptyEntity();
            $activity = TableRegistry::getTableLocator()->get('Activities')->patchEntity($activity,[
                'user_id' => intval($userId),
                'prefix' => ucwords($prefix),
                'controller' => ucwords($controller),
                'action' => ucwords($action),
                'data_key' => intval($dataKey),
                'data' => $data
            ]);

            TableRegistry::getTableLocator()->get('Activities')->save($activity);
        }catch (\Exception $exception){

        }

    }

}
