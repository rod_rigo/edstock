<?php
declare(strict_types=1);

namespace App\Controller\Secretariat;

use App\Controller\AppController;

/**
 * Departments Controller
 *
 * @property \App\Model\Table\DepartmentsTable $Departments
 * @method \App\Model\Entity\Department[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DepartmentsController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->Auth->deny();
        $this->viewBuilder()->setLayout('secretariat');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user){

        if (!empty($user) && strtolower('Administrator') != strtolower($user['role'])) {
            throw new \Cake\Http\Exception\ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $entity = $this->Departments->newEmptyEntity();
        $this->set(compact('entity'));
    }

    public function getDepartments()
    {
        $department= $this->Departments->find();
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data'=>$department]));
    }

    /**
     * View method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $department = $this->Departments->get($id, [
            'contain' => [],
        ]);

        return $this->response->withType('application/json')
        ->withStringBody(json_encode($department));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $department = $this->Departments->newEmptyEntity();
        if ($this->request->is('post')) {
            $department = $this->Departments->patchEntity($department, $this->request->getData());
            if ($this->Departments->save($department)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $department->id, json_encode($department));
                $result = ['result'=>'success','message'=>'The department has been saved.'];
                 return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
            }else{
                $result = ['result'=>'error','message'=>'The department could not be saved. Please, try again.'];
                 return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
            }
        }
        $this->set(compact('department'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $department = $this->Departments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $department = $this->Departments->patchEntity($department, $this->request->getData());
            if ($this->Departments->save($department)) {
                $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $department->id, json_encode($department));
                $result = ['result'=>'success','message'=>'The department has been saved.'];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                $result = ['result'=>'error','message'=>'The department could not be saved. Please, try again.'];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }
        }
        return $this->response->withType('application/json')
                ->withStringBody(json_encode($department));
    }

    /**
     * Delete method
     *
     * @param string|null $id Department id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $department = $this->Departments->get($id);
        if ($this->Departments->delete($department)) {
            $this->Activities->activity($this->Auth->user('id'), $this->request->getParam('prefix'), $this->request->getParam('controller'), $this->request->getParam('action'), $department->id, json_encode($department));
            $result = ['result'=>'success','message'=>'The department has been deleted.'];
             return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['result'=>'error','message'=>'The department could not be deleted. Please try again.'];
             return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }
}
