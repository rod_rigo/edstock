<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Event\EventInterface;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    function beforeFilter(\Cake\Event\EventInterface $event)
    {
        $this->viewBuilder()->setLayout('login');
        $this->Auth->allow();
        parent::beforeFilter($event);
    }
    /**
     * Login method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

    public function login(){

        if($this->Auth->user('id')){
            return $this->redirect(['prefix' => false, 'controller' => 'Users', 'action' => 'logout']);
        }

        $user = $this->Users->newEmptyEntity();

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            $auth = $this->Users->find()
                ->where([
                    'OR'=>[
                        'Users.username like'=>'%'.$this->request->getData('username').'%',
                    ]
                ])
                ->count();

            if($auth > 0){

                if (!empty($user)) {

                    $this->Auth->setUser($user);
                    $user = $this->Users->get($this->Auth->user('id'));
                    $user->password = $this->request->getData('password');
                    $user->token = uniqid().rand(99999999, 99999999).uniqid();

                    if($this->Users->save($user)){
                        if(strtolower('Super Administrator') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                            'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }

                        if(strtolower('User') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                            'redirect' => Router::url(['prefix' => 'Personnel', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                        if(strtolower('Administrator') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                            'redirect' => Router::url(['prefix' => 'Secretariat', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                    }else{
                        if(strtolower('Super Administrator') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                                'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                         if(strtolower('User') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                            'redirect' => Router::url(['prefix' => 'Personnel', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                        if(strtolower('Administrator') == strtolower($this->Auth->user('role'))){
                            $result=['message'=>ucwords('Logged in success'),'result'=>ucwords('success'),
                            'redirect' => Router::url(['prefix' => 'Secretariat', 'controller' => 'Dashboards', 'action' => 'index'])];
                            return $this->response->withStatus(200)->withType('application/json')
                                ->withStringBody(json_encode($result));
                        }
                    }

                }else{
                    $result=['message'=>ucwords('Please Check Username or Email & passwords!'),'result'=>ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }

            }else{
                $result=['message'=>ucwords('Please Check Username or Email & passwords!'),'result'=>ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

        }

        $this->set(compact('user'));
    }

    public function logout()
    {
        if ($this->Auth->logout()) {
            $this->request->getSession()->destroy();
            return $this->redirect(['prefix' => false, 'controller' => 'Users', 'action' => 'login']);
        }
        return $this->redirect(['prefix' => false, 'controller' => 'Users', 'action' => 'login']);
    }

}
