<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inspection Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $order_id
 * @property int $fund_cluster_id
 * @property int $supplier_id
 * @property int $office_id
 * @property string $iar_no
 * @property int $is_compete
 * @property int $is_inspected
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\FundCluster $fund_cluster
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\InspectionItem[] $inspection_items
 */
class Inspection extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'request_id' => true,
        'order_id' => true,
        'fund_cluster_id' => true,
        'supplier_id' => true,
        'office_id' => true,
        'iar_no' => true,
        'is_compete' => true,
        'is_inspected' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'request' => true,
        'order' => true,
        'fund_cluster' => true,
        'supplier' => true,
        'office' => true,
        'inspection_items' => true,
    ];

}
