<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InspectionItem Entity
 *
 * @property int $id
 * @property int $inspection_id
 * @property int $item_id
 * @property float $quantity
 * @property float $cost
 * @property float $total
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Inspection $inspection
 * @property \App\Model\Entity\Item $item
 */
class InspectionItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'inspection_id' => true,
        'item_id' => true,
        'quantity' => true,
        'cost' => true,
        'total' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'inspection' => true,
        'item' => true,
    ];
}
