<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $description
 * @property int $category_id
 * @property int $sub_category_id
 * @property int $unit_id
 * @property float $price
 * @property string $item_code
 * @property float $stock
 * @property string $code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\SubCategory $sub_category
 * @property \App\Model\Entity\Unit $unit
 * @property \App\Model\Entity\InspectionItem[] $inspection_items
 * @property \App\Model\Entity\Orderdetail[] $orderdetails
 * @property \App\Model\Entity\Plandetail[] $plandetails
 * @property \App\Model\Entity\Requestdetail[] $requestdetails
 * @property \App\Model\Entity\RequisitionItem[] $requisition_items
 * @property \App\Model\Entity\RequestItemDetail[] $request_item_details
 * @property \App\Model\Entity\Stock[] $stocks
 */
class Item extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'description' => true,
        'category_id' => true,
        'sub_category_id' => true,
        'unit_id' => true,
        'price' => true,
        'item_code' => true,
        'stock' => true,
        'code' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'category' => true,
        'sub_category' => true,
        'unit' => true,
        'inspection_items' => true,
        'orderdetails' => true,
        'plandetails' => true,
        'requestdetails' => true,
        'requisition_items' => true,
        'stocks' => true,
        'request_item_details' => true,
    ];

    protected function _setDescription($value){
        return ucwords($value);
    }

    protected function _setCode($value){
        return strtoupper($value);
    }

}
