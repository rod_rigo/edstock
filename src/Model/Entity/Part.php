<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Part Entity
 *
 * @property int $id
 * @property int $plan_id
 * @property string|null $part
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\PlanTitle[] $plan_titles
 * @property \App\Model\Entity\Plandetail[] $plandetails
 */
class Part extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'plan_id' => true,
        'part' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'plan' => true,
        'plan_titles' => true,
        'plandetails' => true,
    ];

    protected function _setPart($value){
        return strtoupper($value);
    }

}
