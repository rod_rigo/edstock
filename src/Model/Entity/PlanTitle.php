<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlanTitle Entity
 *
 * @property int $id
 * @property int $plan_id
 * @property int $part_id
 * @property string $plan_title
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Plan $plan
 * @property \App\Model\Entity\Part $part
 * @property \App\Model\Entity\Plandetail[] $plandetails
 */
class PlanTitle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'plan_id' => true,
        'part_id' => true,
        'plan_title' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'plan' => true,
        'part' => true,
        'plandetails' => true,
    ];

    protected function _setPlanTitle($value){
        return strtoupper($value);
    }

}
