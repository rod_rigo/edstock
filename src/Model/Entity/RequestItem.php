<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequestItem Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $office_id
 * @property int $department_id
 * @property string $series_number
 * @property string $purpose
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\RequestItemDetail[] $request_item_details
 */
class RequestItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'user_id' => true,
        'office_id' => true,
        'department_id' => true,
        'series_number' => true,
        'purpose' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'office' => true,
        'department' => true,
        'request_item_details' => true,
    ];

    protected function _setPurpose($value){
        return strtoupper($value);
    }

}
