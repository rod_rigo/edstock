<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequestItemDetail Entity
 *
 * @property int $id
 * @property int $request_item_id
 * @property int|null $request_item_series_id
 * @property int $item_id
 * @property float $cost
 * @property int $qty
 * @property int $total
 * @property int $is_approved
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\RequestItem $request_item
 * @property \App\Model\Entity\Item $item
 */
class RequestItemDetail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'request_item_id' => true,
        'request_item_series_id' => true,
        'item_id' => true,
        'cost' => true,
        'qty' => true,
        'total' => true,
        'is_approved' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'request_item' => true,
        'item' => true,
    ];
}
