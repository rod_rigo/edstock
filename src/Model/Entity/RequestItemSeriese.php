<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequestItemSeriese Entity
 *
 * @property int $id
 * @property int $request_item_id
 * @property string $series_number
 * @property int $is_expendable
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\RequestItem $request_item
 * @property \App\Model\Entity\RequestItemDetail[] $request_item_details
 */
class RequestItemSeriese extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'request_item_id' => true,
        'series_number' => true,
        'is_expendable' => true,
        'created' => true,
        'updated' => true,
        'deleted' => true,
        'request_item' => true,
        'request_item_details' => true,
    ];
}
