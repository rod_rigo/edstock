<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Requisition Entity
 *
 * @property int $id
 * @property int $request_id
 * @property int $inspection_id
 * @property int $order_id
 * @property int $fund_cluster_id
 * @property int $office_id
 * @property string|null $ris_no
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Request $request
 * @property \App\Model\Entity\Inspection $inspection
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\FundCluster $fund_cluster
 * @property \App\Model\Entity\Office $office
 * @property \App\Model\Entity\RequisitionItem[] $requisition_items
 */
class Requisition extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'request_id' => true,
        'inspection_id' => true,
        'order_id' => true,
        'fund_cluster_id' => true,
        'office_id' => true,
        'ris_no' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'request' => true,
        'inspection' => true,
        'order' => true,
        'fund_cluster' => true,
        'office' => true,
        'requisition_items' => true,
    ];

    protected function _setResponsibilityCodeCenter($value){
        return strtoupper($value);
    }

    protected function _setRisNo($value){
        return strtoupper($value);
    }

}
