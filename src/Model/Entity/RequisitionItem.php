<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RequisitionItem Entity
 *
 * @property int $id
 * @property int $requisition_id
 * @property int $item_id
 * @property int $is_available
 * @property float $quantity
 * @property float $cost
 * @property float $total
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Requisition $requisition
 * @property \App\Model\Entity\Item $item
 */
class RequisitionItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'requisition_id' => true,
        'item_id' => true,
        'is_available' => true,
        'quantity' => true,
        'cost' => true,
        'total' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'requisition' => true,
        'item' => true,
    ];

    protected function _setRemarks($value){
        return strtoupper($value);
    }

}
