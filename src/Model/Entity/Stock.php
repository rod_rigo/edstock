<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Stock Entity
 *
 * @property int $id
 * @property int $item_id
 * @property string $description
 * @property float $price
 * @property float $last_stocks
 * @property float $qty
 * @property float $total_stocks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Item $item
 */
class Stock extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'item_id' => true,
        'description' => true,
        'price' => true,
        'last_stocks' => true,
        'qty' => true,
        'total_stocks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'item' => true,
    ];

    protected function _setDescription($value){
        return strtoupper($value);
    }


}
