<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $tin_no
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modifed
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Iar[] $iars
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Request[] $requests
 */
class Supplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'tin_no' => true,
        'created' => true,
        'modifed' => true,
        'deleted' => true,
        'iars' => true,
        'orders' => true,
        'requests' => true,
    ];

    protected function _setName($value){
        return ucwords($value);
    }

    protected function _setAddress($value){
        return ucwords($value);
    }

    protected function _setTinNo($value){
        return strtoupper($value);
    }

}
