<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $fullname
 * @property \Cake\I18n\FrozenDate $bdate
 * @property string|null $tin_no
 * @property string|null $plantilla_no
 * @property string $position
 * @property string $role
 * @property int $status
 * @property string|null $token
 * @property string $contact
 * @property int $department_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\RequestItem[] $requestItems
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'fullname' => true,
        'bdate' => true,
        'tin_no' => true,
        'plantilla_no' => true,
        'position' => true,
        'role' => true,
        'status' => true,
        'token' => true,
        'contact' => true,
        'department_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'department' => true,
        'request_items' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array<string>
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword(string $password) : ?string
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher())->hash($password);
        }
    }

}
