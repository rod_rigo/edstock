<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * InspectionItems Model
 *
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\BelongsTo $Inspections
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\BelongsTo $Items
 *
 * @method \App\Model\Entity\InspectionItem newEmptyEntity()
 * @method \App\Model\Entity\InspectionItem newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\InspectionItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InspectionItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\InspectionItem findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\InspectionItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InspectionItem[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\InspectionItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InspectionItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InspectionItem[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionItem[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionItem[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InspectionItem[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InspectionItemsTable extends Table
{

    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */

    public function beforeSave(EventInterface $event)
    {
        $entity = $event->getData('entity');

        $connection = ConnectionManager::get('default');

        if ($entity->isNew()) {
            $connection->begin();
           try{
               $item = $this->getTableLocator()->get('Items')->get(intval($entity->item_id));
               $stocks = $item->stock;

               $total = intval($stocks) + intval($entity->quantity);
               $item->stock = intval($total);

               $this->getTableLocator()->get('Items')->save($item);

               $stock = $this->getTableLocator()->get('Stocks')->newEmptyEntity();
               $stock->item_id = intval($item->id);
               $stock->description = strtoupper('PROCUREMENT (IN)');
               $stock->price = doubleval($entity->cost);
               $stock->last_stocks = intval($stocks);
               $stock->qty = intval($entity->quantity);
               $stock->total_stocks = intval($total);
               $this->getTableLocator()->get('Stocks')->save($stock);

               $connection->commit();
               return true;
           }catch (\Exception $exception){
               $connection->rollback();
               dd($exception->getMessage());
           }



        }
    }

    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('inspection_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Inspections', [
            'foreignKey' => 'inspection_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('inspection_id')
            ->notEmptyString('inspection_id');

        $validator
            ->nonNegativeInteger('item_id')
            ->notEmptyString('item_id');

        $validator
            ->numeric('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmptyString('quantity');

        $validator
            ->numeric('cost')
            ->requirePresence('cost', 'create')
            ->notEmptyString('cost');

        $validator
            ->numeric('total')
            ->requirePresence('total', 'create')
            ->notEmptyString('total');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('inspection_id', 'Inspections'), ['errorField' => 'inspection_id']);
        $rules->add($rules->existsIn('item_id', 'Items'), ['errorField' => 'item_id']);

        return $rules;
    }
}
