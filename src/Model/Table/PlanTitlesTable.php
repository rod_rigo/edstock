<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PlanTitles Model
 *
 * @property \App\Model\Table\PlansTable&\Cake\ORM\Association\BelongsTo $Plans
 * @property \App\Model\Table\PartsTable&\Cake\ORM\Association\BelongsTo $Parts
 * @property \App\Model\Table\PlandetailsTable&\Cake\ORM\Association\HasMany $Plandetails
 *
 * @method \App\Model\Entity\PlanTitle newEmptyEntity()
 * @method \App\Model\Entity\PlanTitle newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\PlanTitle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PlanTitle get($primaryKey, $options = [])
 * @method \App\Model\Entity\PlanTitle findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\PlanTitle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PlanTitle[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\PlanTitle|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanTitle saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlanTitle[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanTitle[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanTitle[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PlanTitle[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlanTitlesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('plan_titles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Parts', [
            'foreignKey' => 'part_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Plandetails', [
            'foreignKey' => 'plan_title_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('plan_id')
            ->notEmptyString('plan_id');

        $validator
            ->nonNegativeInteger('part_id')
            ->notEmptyString('part_id');

        $validator
            ->scalar('plan_title')
            ->maxLength('plan_title', 255)
            ->requirePresence('plan_title', 'create')
            ->notEmptyString('plan_title');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('plan_id', 'Plans'), ['errorField' => 'plan_id']);
        $rules->add($rules->existsIn('part_id', 'Parts'), ['errorField' => 'part_id']);

        return $rules;
    }
}
