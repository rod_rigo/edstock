<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * RequestItemDetails Model
 *
 * @property \App\Model\Table\RequestItemsTable&\Cake\ORM\Association\BelongsTo $RequestItems
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\BelongsTo $Items
 *
 * @method \App\Model\Entity\RequestItemDetail newEmptyEntity()
 * @method \App\Model\Entity\RequestItemDetail newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\RequestItemDetail findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RequestItemDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemDetail[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemDetail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestItemDetail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestItemDetail[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemDetail[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemDetail[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemDetail[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequestItemDetailsTable extends Table
{

    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('request_item_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RequestItems', [
            'foreignKey' => 'request_item_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('RequestItemSerieses', [
            'foreignKey' => 'request_item_series_id',
        ]);
        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER',
        ]);
    }

    public function beforeSave(EventInterface $event)
    {
        $entity = $event->getData('entity');

        $connection = ConnectionManager::get('default');
        if ($entity->isDirty('is_approved') && boolval($entity->is_approved)) {

//            $connection->begin();
//
//            try{
//                $item = $this->getTableLocator()->get('Items')->get(intval($entity->item_id));
//                $stocks = $item->stock;
//
//                $total = intval($stocks) - intval($entity->qty);
//                $item->stock = intval($total);
//
//                $this->getTableLocator()->get('Items')->save($item);
//
//                $stock = $this->getTableLocator()->get('Stocks')->newEmptyEntity();
//                $stock->item_id = intval($item->id);
//                $stock->description = strtoupper('INVENTORY REQUEST (OUT)');
//                $stock->last_stocks = intval($stocks);
//                $stock->price = doubleval($item->price);
//                $stock->qty = intval($entity->qty);
//                $stock->total_stocks = intval($total);
//                $this->getTableLocator()->get('Stocks')->save($stock);
//
//                $connection->commit();
//                return true;
//            }catch (\Exception $exception){
//                $connection->rollback();
//                dd($exception->getMessage());
//            }

        }

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('request_item_id')
            ->notEmptyString('request_item_id');

        $validator
            ->nonNegativeInteger('request_item_series_id')
            ->allowEmptyString('request_item_series_id');

        $validator
            ->nonNegativeInteger('item_id')
            ->notEmptyString('item_id');

        $validator
            ->numeric('cost')
            ->requirePresence('cost', 'create')
            ->notEmptyString('cost');

        $validator
            ->integer('qty')
            ->requirePresence('qty', 'create')
            ->notEmptyString('qty');

        $validator
            ->integer('total')
            ->requirePresence('total', 'create')
            ->notEmptyString('total');

        $validator
            ->numeric('is_approved')
            ->boolean('is_approved')
            ->requirePresence('is_approved', false)
            ->notEmptyString('is_approved');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('request_item_id', 'RequestItems'), ['errorField' => 'request_item_id']);
        $rules->add($rules->existsIn('request_item_series_id', 'RequestItemSerieses'), ['errorField' => 'request_item_series_id']);
        $rules->add($rules->existsIn('item_id', 'Items'), ['errorField' => 'item_id']);

        return $rules;
    }
}
