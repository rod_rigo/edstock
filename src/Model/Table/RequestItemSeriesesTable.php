<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * RequestItemSerieses Model
 *
 * @property \App\Model\Table\RequestItemsTable&\Cake\ORM\Association\BelongsTo $RequestItems
 *
 * @method \App\Model\Entity\RequestItemSeriese newEmptyEntity()
 * @method \App\Model\Entity\RequestItemSeriese newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemSeriese get($primaryKey, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RequestItemSeriese|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RequestItemSeriese[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequestItemSeriesesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('request_item_serieses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RequestItems', [
            'foreignKey' => 'request_item_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('RequestItemDetails', [
            'foreignKey' => 'request_item_series_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('request_item_id')
            ->notEmptyString('request_item_id');

        $validator
            ->scalar('series_number')
            ->maxLength('series_number', 255)
            ->requirePresence('series_number', 'create')
            ->notEmptyString('series_number');

        $validator
            ->integer('is_expendable')
            ->requirePresence('is_expendable', 'create')
            ->notEmptyString('is_expendable');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function findSeriesNumber($is_expendable = null){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $request = $this->find()
            ->where([
                'RequestItemSerieses.created >=' => $startMonth,
                'RequestItemSerieses.created <=' => $endMonth,
                'RequestItemSerieses.is_expendable <=' => intval($is_expendable),
            ])->last();
        $year = date('Y');
        $month = date('m');
        $series = (!empty($request))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $request->series_number)[2]) + 1)), 4, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 4, '0', STR_PAD_LEFT));
        return $series;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('request_item_id', 'RequestItems'), ['errorField' => 'request_item_id']);

        return $rules;
    }
}
