<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * Requests Model
 *
 * @property \App\Model\Table\OfficesTable&\Cake\ORM\Association\BelongsTo $Offices
 * @property \App\Model\Table\FundClustersTable&\Cake\ORM\Association\BelongsTo $FundClusters
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\InspectionsTable&\Cake\ORM\Association\HasMany $Inspections
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\RequestdetailsTable&\Cake\ORM\Association\HasMany $Requestdetails
 *
 * @method \App\Model\Entity\Request newEmptyEntity()
 * @method \App\Model\Entity\Request newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Request[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Request get($primaryKey, $options = [])
 * @method \App\Model\Entity\Request findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Request patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Request[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Request|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Request[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequestsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('requests');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Offices', [
            'foreignKey' => 'office_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('FundClusters', [
            'foreignKey' => 'fund_cluster_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Inspections', [
            'foreignKey' => 'request_id',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'request_id',
        ]);
        $this->hasMany('Requestdetails', [
            'foreignKey' => 'request_id',
        ])->setDependent(true);

        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {

        $validator
            ->integer('user_id')
            ->notEmptyString('user_id');

        $validator
            ->scalar('series_number')
            ->maxLength('series_number',255)
            ->notEmptyString('series_number');

        $validator
            ->integer('office_id')
            ->notEmptyString('office_id');

        $validator
            ->integer('fund_cluster_id')
            ->notEmptyString('fund_cluster_id');

        $validator
            ->integer('department_id')
            ->notEmptyString('department_id');

        $validator
            ->scalar('purpose')
            ->requirePresence('purpose', 'create')
            ->notEmptyString('purpose');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmptyString('status');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function findSeriesNumber(){
        $startMonth = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endMonth = (new Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d');
        $request = $this->find()
            ->where([
                'Requests.created >=' => $startMonth,
                'Requests.created <=' => $endMonth,
                'Requests.status <=' => intval(1),
            ])->last();
        $year = date('Y');
        $month = date('m');
        $series = (!empty($request))? ($year).'-'.($month).'-'.(str_pad(strval(intval(intval(explode('-', $request->series_number)[2]) + 1)), 4, '0', STR_PAD_LEFT)): ($year).'-'.($month).'-'.(str_pad(strval('1'), 4, '0', STR_PAD_LEFT));
        return $series;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('office_id', 'Offices'), ['errorField' => 'office_id']);
        $rules->add($rules->existsIn('fund_cluster_id', 'FundClusters'), ['errorField' => 'fund_cluster_id']);
        $rules->add($rules->existsIn('department_id', 'Departments'), ['errorField' => 'department_id']);

        return $rules;
    }
}
