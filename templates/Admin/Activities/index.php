<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Stock> $stocks
 */
?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Activity Logs</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>User</th>
                                <th>Prefix</th>
                                <th>Controller</th>
                                <th>Action</th>
                                <th>Key</th>
                                <th>Data</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'activities/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            dom:'lBfrtip',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order:[ [7, 'desc'] ],
            ajax:{
                url:baseurl+'getActivities',
                method: 'GET',
                dataType: 'JSON'
            },
            buttons: [
                {
                    text: 'Delete All',
                    action: function () {
                        Swal.fire({
                            title: 'Delete All Logs?',
                            text: 'Are You Sure',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                $.ajax({
                                    url:baseurl+'deleteAll',
                                    type: 'DELETE',
                                    method: 'DELETE',
                                    headers: {
                                        'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                                    },
                                    dataType:'JSON',
                                    beforeSend:function () {
                                        Swal.fire({
                                            icon: 'info',
                                            title: '',
                                            text: 'Please Wait',
                                            allowOutsideClick: false,
                                            showConfirmButton: false,
                                            timerProgressBar: false,
                                            didOpen: function () {
                                                Swal.showLoading();
                                            }
                                        });
                                    }
                                }).done(function (data, status, xhr) {
                                    swal('success', data.result, data.message);
                                    table.ajax.reload(null, false);
                                }).fail(function (xhr, status, error) {
                                    const response = JSON.parse(xhr.responseText);
                                    swal('info', response.result, response.message);
                                });
                            }
                        });

                    },
                    className:'btn btn-warning',
                    footer:true
                },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data,type,row){

//                        var ul = '';
//                        $.map(JSON.parse(row.data),function (data, key) {
//                            console.log(data+ key);
//                            ul+= '<li>'+(key)+' - <strong>'+(data)+'</strong></li>';
//                        });

//                        return '<ul>'+(ul)+'</ul>';
                        return data;
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data,type,row){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'user.username'},
                { data: 'prefix'},
                { data: 'controller'},
                { data: 'action'},
                { data: 'data_key'},
                { data: 'data'},
                { data: 'created'},
            ]
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

