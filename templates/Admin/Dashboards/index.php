<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>
<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>
<!-- <div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-info">
        <div class="inner">
            <h3 id="total-users">0</h3>
            <p>Total Users</p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-primary">
        <div class="inner">
            <h3 id="total-heads">0</h3>
            <p>Total Heads</p>
        </div>
        <div class="icon">
            <i class="fa fa-user-circle"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3 id="total-suppliers">0</h3>
            <p>Total Suppliers</p>
        </div>
        <div class="icon">
            <i class="fa fa-address-card"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-danger">
        <div class="inner">
            <h3 id="total-departments">0</h3>
            <p>Total Departments</p>
        </div>
        <div class="icon">
            <i class="fa fa-building"></i>
        </div>
    </div>
</div> -->

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
                    <?=$this->Form->label('interval', ucwords('Interval (Procurement)'))?>
                    <?=$this->Form->select('interval',[
                        ['value' => strtolower('day'), 'text' => strtoupper('day')],
                        ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                        ['value' => strtolower('month'), 'text' => strtoupper('month')],
                        ['value' => strtolower('week'), 'text' => strtoupper('week')],
                        ['value' => strtolower('year'), 'text' => strtoupper('year')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'interval',
                    ])?>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3 id="request-procurement-pending">0</h3>
                            <p>Request Procurement (Pending)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'pending'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3 id="request-procurement-approved">0</h3>
                            <p>Request Procurement (Approved)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'approved'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3 id="request-procurement-declined">0</h3>
                            <p>Request Procurement (Declined)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'declined'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
                    <?=$this->Form->label('interval_request', ucwords('Interval (Request)'))?>
                    <?=$this->Form->select('interval_request',[
                        ['value' => strtolower('day'), 'text' => strtoupper('day')],
                        ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                        ['value' => strtolower('month'), 'text' => strtoupper('month')],
                        ['value' => strtolower('week'), 'text' => strtoupper('week')],
                        ['value' => strtolower('year'), 'text' => strtoupper('year')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'interval-request',
                    ])?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3 id="inventory-request-pending">0</h3>
                            <p>Inventory Request (Pending)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'RequestItems', 'action' => 'pending'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3 id="inventory-request-approved">0</h3>
                            <p>Inventory Request (Approved)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'RequestItems', 'action' => 'approved'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3 id="inventory-request-declined">0</h3>
                            <p>Inventory Request (Declined)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'RequestItems', 'action' => 'declined'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3 id="inventory-request-released">0</h3>
                            <p>Inventory Request (Released)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'RequestItems', 'action' => 'released'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header ui-sortable-handle" style="cursor: move;">
                            <h3 class="card-title">
                                Forecast
                            </h3>
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#forecast-tab" data-toggle="tab">Chart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#forecast-list-tab" data-toggle="tab">List</a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content p-0">
                                <!-- Morris chart - Sales -->
                                <div class="chart tab-pane active" id="forecast-tab" >
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <canvas id="forecast-chart" height="400 " width="550"></canvas>
                                        </div>
                                    </div>
                                </div>

                                <div class="chart tab-pane" id="forecast-list-tab">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="table-responsive">
                                                <table id="forecast-datatable" class="table table-bordered table-striped" style="width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Item</th>
                                                        <th>Average</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-header border-transparent">
            <h3 class="card-title">Procurements (<?=date('F')?>)</h3>
            <a href="javascript:void(0)" class="btn btn-sm btn-info float-right rounded-0" id="procurements-update">Fetch Update</a>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                    <tr>
                        <th>Process</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="procurements-month">

                    </tbody>
                </table>
            </div>

        </div>

        <div class="card-footer clearfix">

        </div>

    </div>
</div>

<div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-header border-transparent">
            <h3 class="card-title">Procurements (<?=date('Y')?>)</h3>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                    <tr>
                        <th>Process</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="procurements-year">

                    </tbody>
                </table>
            </div>

        </div>

        <div class="card-footer clearfix">

        </div>

    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items List</h3>
            <div class="card-tools">
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Unit</th>
                                <th>Price</th>
                                <th>Stocks</th>
                                <th>Code</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    'use strict';
    $(document).ready(function (e) {

        const autocolors = window['chartjs-plugin-autocolors'];
        Chart.register(autocolors);

        var forecastChart = document.querySelector('#forecast-chart').getContext('2d');
        var forecastContext;

        var baseurl = mainurl+'dashboards/';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:mainurl+'items/getItems',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'description'},
                { data: 'category.name'},
                { data: 'sub_category.name'},
                { data: 'unit.name'},
                { data: 'price'},
                { data: 'stock'},
                { data: 'code'},
            ]
        });

        var forsecastdatatable = $('#forecast-datatable');
        var forsecasttable = forsecastdatatable.DataTable({
            destroy:true,
            dom:'lBfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:mainurl+'dashboards/getDemandForecast',
                method: 'GET',
                dataType: 'JSON'
            },
            buttons: [
                {
                    extend: 'print',
                    title: 'Print',
                    text: 'Print',
                    className:'btn btn-secondary rounded-0',
                    attr:  {
                        id: 'print'
                    },
                    exportOptions: {
                        columns: [0, 1, 2,]
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10px' )
                            .prepend('');
                        $(win.document.body).find( 'table tbody' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' ).css({'background':'transparent'});

                    },
                    footer:true
                },
                {
                    extend: 'pdfHtml5',
                    attr:  {
                        id: 'pdf'
                    },
                    text: 'PDF',
                    title: 'PDF Reports',
                    tag: 'button',
                    className:'btn btn-danger rounded-0',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2,]
                    },
                    action: function(e, dt, node, config) {
                        Swal.fire({
                            title:'PDF',
                            text:'Export To PDF?',
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                setTimeout(function(){
                                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                }, 1000);
                            }
                        });
                    },
                    customize: function(doc) {
                        doc.pageMargins = [2, 2, 2, 2 ];
                        doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.styles.tableHeader.fontSize = 10;
                        doc.styles.tableBodyEven.fontSize = 10;
                        doc.styles.tableBodyOdd.fontSize = 10;
                        doc.styles.tableFooter.fontSize = 10;

                    },
                    download: 'open',
                    footer:true
                },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 2,
                    render: function ( data, type, row, meta ) {
                        return parseFloat(row.average).toFixed(2);
                    }
                },
            ],
            columns: [
                { data: 'item'},
                { data: 'item'},
                { data: 'average'},
            ]
        });

        function users() {
            $.ajax({
                url:baseurl+'users',
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#total-users').text(parseInt(data.users));
                $('#total-heads').text(parseInt(data.heads));
                $('#total-suppliers').text(parseInt(data.suppliers));
                $('#total-departments').text(parseInt(data.departments));
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        }

        function procurements() {
            $.ajax({
                url:baseurl+'procurements',
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('#procurements-month, #procurements-year').empty();
                }
            }).done(function (data, status, xhr) {
                $.map(data.month, function (data, key) {
                    $('#procurements-month').append('<tr> <th>'+(data.label)+'</th> <td>'+(parseInt(data.data))+'</td></tr>');
                });
                $.map(data.year, function (data, key) {
                    $('#procurements-year').append('<tr> <th>'+(data.label)+'</th> <td>'+(parseInt(data.data))+'</td></tr>');
                });
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        }

        function request(interval) {
              $.ajax({
                  url : baseurl+'request/'+(interval),
                  type : 'GET',
                  method : 'GET',
                  dataType:'JSON',
                  beforeSend:function(e){
                      Swal.fire({
                          icon: null,
                          title: null,
                          text: null,
                          allowOutsideClick: false,
                          showConfirmButton: false,
                          timerProgressBar: false,
                          didOpen: function () {
                              Swal.showLoading();
                          }
                      });
                  },
              }).done(function(data, status, xhr){
                  $('#request-procurement-pending').text(parseInt(data.requests.pending));
                  $('#request-procurement-approved').text(parseInt(data.requests.approved));
                  $('#request-procurement-declined').text(parseInt(data.requests.declined));
                  Swal.close();
              }).fail(function(data, status, xhr){
                  Swal.close();
              });
        }

        function inventoryrequest(interval) {
            $.ajax({
                url : baseurl+'inventoryrequest/'+(interval),
                type : 'GET',
                method : 'GET',
                dataType:'JSON',
                beforeSend:function(e){
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                },
            }).done(function(data, status, xhr){
                $('#inventory-request-pending').text(parseInt(data.requests.pending));
                $('#inventory-request-approved').text(parseInt(data.requests.approved));
                $('#inventory-request-declined').text(parseInt(data.requests.declined));
                $('#inventory-request-released').text(parseInt(data.requests.released));
                Swal.close();
            }).fail(function(data, status, xhr){
                Swal.close();
            });
        }

        function demandforecast() {
            var array = {
                'month' : {
                    'data':[],
                    'label':[]
                },
            };
            $.ajax({
                url:baseurl+'demandforecast',
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                async: false,
                global: false,
                beforeSend:function () {
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $.map(data.month, function (data, key) {
                    array.month.data.push(parseFloat(data.average));
                    array.month.label.push((data.item).toUpperCase());
                });
                Swal.close();
            }).fail(function (data, status, xhr) {
                Swal.close();
            }).always(function (data, status, xhr) {
                Swal.close();
            });
            return array;
        }

        function forecastsChart() {
            forecastContext =  new Chart(forecastChart, {
                type: 'line',
                data: {
                    labels: demandforecast().month.label,
                    datasets: [
                        {
                            label:'Average',
                            data: demandforecast().month.data,
                        }
                    ]
                },
                options: {
                    indexAxis:'y',
                    plugins: {
                        autocolors: {
                            mode: 'data',
                            offset: 2
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        subtitle: {
                            display: false,
                            text: ''
                        },
                        legend: {
                            display:true,
                            onClick: function (evt, legendItem, legend) {
                                const index = legend.chart.data.labels.indexOf(legendItem.text);
                                legend.chart.toggleDataVisibility(index);
                                legend.chart.update();
                            },
                            labels:{
                                generateLabels: function (chart) {
                                    return  $.map(chart.data.labels, function( label, index ) {
                                        return {
                                            text:label,
                                            strokeStyle: chart.data.datasets[0].borderColor[index],
                                            fillStyle: chart.data.datasets[0].backgroundColor[index],
                                            hidden: !chart.getDataVisibility(index)
                                        };
                                    });
                                }
                            }
                        }
                    }
                },
                plugins:[
                    {
                        id: '',
                        beforeDraw: function (chart) {
                            const ctx = chart.canvas.getContext('2d');
                            ctx.save();
                            ctx.globalCompositeOperation = 'destination-over';
                            ctx.fillStyle = 'white';
                            ctx.fillRect(0, 0, chart.width, chart.height);
                            ctx.restore();
                        }
                    }
                ]
            });
        }

        $('#procurements-update').click(function (e) {
            procurements();
        });

        $('#interval').change(function (e) {
           var value = $(this).val();
            request(value);
        });

        $('#interval-request').change(function (e) {
            var value = $(this).val();
            inventoryrequest(value);
        });

        users();
        request($('#interval').val());
        inventoryrequest($('#interval-request').val());

        demandforecast();
        forecastsChart();

        setInterval(function () {
            users();
            request($('#interval').val());
            inventoryrequest($('#interval-request').val());
            demandforecast();
            forecastContext.destroy();
            forecastsChart();
        },30000);

    });
</script>

