<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inspection $inspection
 * @var \Cake\Collection\CollectionInterface|string[] $requests
 * @var \Cake\Collection\CollectionInterface|string[] $orders
 * @var \Cake\Collection\CollectionInterface|string[] $fundClusters
 * @var \Cake\Collection\CollectionInterface|string[] $suppliers
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 */
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'edit', intval($request->id)])?>" class="btn btn-info rounded-0">
                    View Requests
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'edit', intval($order->id)])?>" class="btn btn-info rounded-0">
                    View Orders
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($inspection,['id'=>'form', 'type' => 'file'])?>
            <div class="row mb-3">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <?=$this->Form->control('created_by',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->user->fullname,
                        'readonly' => true,
                    ])?>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <?=$this->Form->control('purpose',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->purpose,
                        'readonly' => true,
                    ])?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('supplier_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $suppliers,
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                            <?=$this->Form->hidden('is_compete',[
                                'class' => 'form-control rounded-0',
                                'value' => 0,
                                'required' => true,
                                'id' => 'is-compete'
                            ])?>

                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="compete">
                                <label for="compete">
                                    Completed
                                </label>
                            </div>
                        </div>


                        <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                            <?=$this->Form->hidden('is_inspected',[
                                'class' => 'form-control rounded-0',
                                'value' => 0,
                                'required' => true,
                                'id' => 'is-inspected'
                            ])?>

                            <div class="icheck-primary d-inline">
                                <input type="checkbox" id="inspected">
                                <label for="inspected">
                                    Inspected
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                    <thead>
                                    <tr>
                                        <th width="15%">Property No</th>
                                        <th width="10%">Unit</th>
                                        <th width="20%">Description</th>
                                        <th width="25%">Quantity</th>
                                        <th width="15%">Unit Cost</th>
                                        <th width="15%">Total Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody id="items">
                                    <?php $i = 1;?>
                                    <?php foreach ($order->orderdetails as $key => $orderdetail):?>
                                        <tr>
                                            <td><?=intval($i++)?></td>
                                            <td>
                                                <?=strtoupper($orderdetail->item->unit->name)?>
                                                <?=$this->Form->hidden('inspection_items.'.($key).'.item_id',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($orderdetail->item->id),
                                                    'id' => 'inspection-items-'.($key).'-item-id'
                                                ])?>
                                            </td>
                                            <td><?=strtoupper($orderdetail->item->description)?></td>
                                            <td>
                                                <?=strtoupper($orderdetail->qty)?>
                                                <?=$this->Form->hidden('inspection_items.'.($key).'.quantity',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($orderdetail->qty),
                                                    'id' => 'inspection-items-'.($key).'-quantity'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=strtoupper(number_format($orderdetail->cost))?>
                                                <?=$this->Form->hidden('inspection_items.'.($key).'.cost',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($orderdetail->cost),
                                                    'id' => 'inspection-items-'.($key).'-cost'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=strtoupper(number_format($orderdetail->total))?>
                                                <?=$this->Form->hidden('inspection_items.'.($key).'.total',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($orderdetail->total),
                                                    'id' => 'inspection-items-'.($key).'-total'
                                                ])?>
                                            </td>
                                        </tr>
                                    <?php endforeach;;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->control('request_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $request->id])?>
            <?=$this->Form->control('order_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $order->id])?>
            <button type="reset" class="btn btn-danger rounded-0">Reset</button>
            <button type="submit" class="btn btn-primary rounded-0">Save</button>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.replace(data.redirect);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('#compete').change(function (e) {
           var prop = $(this).prop('checked');
           $('#is-compete').val(Number(prop));
        });

        $('#inspected').change(function (e) {
            var prop = $(this).prop('checked');
            $('#is-inspected').val(Number(prop));
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
