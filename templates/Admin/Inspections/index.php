<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Inspection> $inspections
 */
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Series No.</th>
                                <th>Created By</th>
                                <th>Supplier</th>
                                <th>Place Of Delivery</th>
                                <th>Status</th>
                                <th>Inspection</th>
                                <th>Created</th>
                                <th>
                                    Options
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'inspections/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order:[ [7, 'desc'] ],
            ajax:{
                url:baseurl+'getInspections',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data,type,row){
                        return (row.is_compete)? 'Completed': 'Incomplete';
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data,type,row){
                        return (row.is_inspected)? 'Inspected': 'Incomplete';
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data,type,row){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 8,
                    data: null,
                    render: function(data, type, row, meta){
                        return '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a> | ' +
                        '<a data-id="'+row.id+'" class="btn btn-info btn-sm rounded-0 pdf">PDF</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'iar_no'},
                { data: 'request.user.fullname'},
                // { data: 'request.purpose'},
                // { data: 'office.name'},
                // { data: 'fund_cluster.name'},
                { data: 'supplier.name'},
                { data: 'order.place_of_delivery'},
                { data: 'is_compete'},
                { data: 'is_inspected'},
                { data: 'created'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.inspection',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var requestId = $(this).attr('request-id');
            var href = mainurl+'inspections/add/'+(dataId)+'/'+(requestId);
            Swal.fire({
                title: 'Process To Inspection?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.location.replace(href);
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'print-inspection/'+dataId;
            window.open(baseurl+href);
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

