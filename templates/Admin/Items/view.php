<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Items'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Item'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="items view content">
            <h3><?= h($item->description) ?></h3>
            <table>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($item->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Category') ?></th>
                    <td><?= $item->has('category') ? $this->Html->link($item->category->name, ['controller' => 'Categories', 'action' => 'view', $item->category->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Unit') ?></th>
                    <td><?= $item->has('unit') ? $this->Html->link($item->unit->name, ['controller' => 'Units', 'action' => 'view', $item->unit->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($item->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Price') ?></th>
                    <td><?= $this->Number->format($item->price) ?></td>
                </tr>
                <tr>
                    <th><?= __('Stocks') ?></th>
                    <td><?= $this->Number->format($item->stocks) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($item->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($item->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Deleted') ?></th>
                    <td><?= h($item->deleted) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Orderdetails') ?></h4>
                <?php if (!empty($item->orderdetails)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Order Id') ?></th>
                            <th><?= __('Item Id') ?></th>
                            <th><?= __('Cost') ?></th>
                            <th><?= __('Qty') ?></th>
                            <th><?= __('Total') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($item->orderdetails as $orderdetails) : ?>
                        <tr>
                            <td><?= h($orderdetails->id) ?></td>
                            <td><?= h($orderdetails->order_id) ?></td>
                            <td><?= h($orderdetails->item_id) ?></td>
                            <td><?= h($orderdetails->cost) ?></td>
                            <td><?= h($orderdetails->qty) ?></td>
                            <td><?= h($orderdetails->total) ?></td>
                            <td><?= h($orderdetails->created) ?></td>
                            <td><?= h($orderdetails->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Orderdetails', 'action' => 'view', $orderdetails->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Orderdetails', 'action' => 'edit', $orderdetails->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orderdetails', 'action' => 'delete', $orderdetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderdetails->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Plandetails') ?></h4>
                <?php if (!empty($item->plandetails)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Plan Id') ?></th>
                            <th><?= __('Code') ?></th>
                            <th><?= __('Item Id') ?></th>
                            <th><?= __('Qty') ?></th>
                            <th><?= __('Cost') ?></th>
                            <th><?= __('Total') ?></th>
                            <th><?= __('Jan') ?></th>
                            <th><?= __('Feb') ?></th>
                            <th><?= __('Mar') ?></th>
                            <th><?= __('Apr') ?></th>
                            <th><?= __('May') ?></th>
                            <th><?= __('Jun') ?></th>
                            <th><?= __('Jul') ?></th>
                            <th><?= __('Aug') ?></th>
                            <th><?= __('Sep') ?></th>
                            <th><?= __('Oct') ?></th>
                            <th><?= __('Nov') ?></th>
                            <th><?= __('Decm') ?></th>
                            <th><?= __('Created') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($item->plandetails as $plandetails) : ?>
                        <tr>
                            <td><?= h($plandetails->id) ?></td>
                            <td><?= h($plandetails->plan_id) ?></td>
                            <td><?= h($plandetails->code) ?></td>
                            <td><?= h($plandetails->item_id) ?></td>
                            <td><?= h($plandetails->qty) ?></td>
                            <td><?= h($plandetails->cost) ?></td>
                            <td><?= h($plandetails->total) ?></td>
                            <td><?= h($plandetails->jan) ?></td>
                            <td><?= h($plandetails->feb) ?></td>
                            <td><?= h($plandetails->mar) ?></td>
                            <td><?= h($plandetails->apr) ?></td>
                            <td><?= h($plandetails->may) ?></td>
                            <td><?= h($plandetails->jun) ?></td>
                            <td><?= h($plandetails->jul) ?></td>
                            <td><?= h($plandetails->aug) ?></td>
                            <td><?= h($plandetails->sep) ?></td>
                            <td><?= h($plandetails->oct) ?></td>
                            <td><?= h($plandetails->nov) ?></td>
                            <td><?= h($plandetails->decm) ?></td>
                            <td><?= h($plandetails->created) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Plandetails', 'action' => 'view', $plandetails->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Plandetails', 'action' => 'edit', $plandetails->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Plandetails', 'action' => 'delete', $plandetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $plandetails->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Requestdetails') ?></h4>
                <?php if (!empty($item->requestdetails)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Request Id') ?></th>
                            <th><?= __('Item Id') ?></th>
                            <th><?= __('Cost') ?></th>
                            <th><?= __('Qty') ?></th>
                            <th><?= __('Total') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Deleted') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($item->requestdetails as $requestdetails) : ?>
                        <tr>
                            <td><?= h($requestdetails->id) ?></td>
                            <td><?= h($requestdetails->request_id) ?></td>
                            <td><?= h($requestdetails->item_id) ?></td>
                            <td><?= h($requestdetails->cost) ?></td>
                            <td><?= h($requestdetails->qty) ?></td>
                            <td><?= h($requestdetails->total) ?></td>
                            <td><?= h($requestdetails->created) ?></td>
                            <td><?= h($requestdetails->modified) ?></td>
                            <td><?= h($requestdetails->deleted) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Requestdetails', 'action' => 'view', $requestdetails->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Requestdetails', 'action' => 'edit', $requestdetails->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Requestdetails', 'action' => 'delete', $requestdetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $requestdetails->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Stocks') ?></h4>
                <?php if (!empty($item->stocks)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Item Id') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Last Stocks') ?></th>
                            <th><?= __('Qty') ?></th>
                            <th><?= __('Total Stocks') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('Deleted') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($item->stocks as $stocks) : ?>
                        <tr>
                            <td><?= h($stocks->id) ?></td>
                            <td><?= h($stocks->item_id) ?></td>
                            <td><?= h($stocks->description) ?></td>
                            <td><?= h($stocks->last_stocks) ?></td>
                            <td><?= h($stocks->qty) ?></td>
                            <td><?= h($stocks->total_stocks) ?></td>
                            <td><?= h($stocks->created) ?></td>
                            <td><?= h($stocks->modified) ?></td>
                            <td><?= h($stocks->deleted) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Stocks', 'action' => 'view', $stocks->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Stocks', 'action' => 'edit', $stocks->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stocks', 'action' => 'delete', $stocks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stocks->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
