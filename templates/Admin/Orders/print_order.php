<?php
class xtcpdf extends TCPDF {

    private $customHeaderText;  // Additional parameter for Header()

    // Constructor to set additional parameters
    public function __construct($customHeaderText = null) {
        parent::__construct();
        $this->customHeaderText = $customHeaderText;
    }

    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 8);
        $this->Cell(0, 0, 'Appendix 61', 0, 0, 'R');
        $this->Ln(6);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'PURCHASE ORDER', 0, 0, 'C');
        $this->Ln(6);
        $this->SetFont('times', 'U', 12);
        $this->Cell(0, 0, ' ' .($this->customHeaderText). ' ', 0, 0, 'C');
        $this->Ln(5);
        $this->SetFont('times', 'P', 12);
        $this->Cell(0, 0, 'Entity Name', 0, 0, 'C');
    }
}

function convertNumberToWords($number, $currency = 'Pesos') {
    $formatter = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    $amountInWords = ucwords($formatter->format($number));
    return $amountInWords . ' ' . $currency . ' ' . "Only";
}


$pdf = new xtcpdf($order->office->name,'P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->Header();
$pdf->SetMargins(10, 40, 20, true);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('times','',12);

$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$data = '';
$ctr = 0;
$total = 0;
foreach ($order->orderdetails as $details){
    $data .='<tr>
                <td align="center">'.($ctr+1).'</td>
                <td align="center">'.$details->item->unit->name.'</td>
                <td align="left"> '.$details->item->description.'</td>
                <td align="right">'.number_format($details->qty).'</td>
                <td align="right">'.number_format($details->cost,2).'</td>
                <td align="right">'.number_format($details->total,2).'</td>
                </tr>
    ';
    $ctr++;
    $total +=$details->total;
}

$totalAmountWords = convertNumberToWords($total);

$html = '<table width="100%" cellpadding="2" style="border-collapse: collapse; border: 1px solid black;">
    <tr>

        <td width="60%">Supplier: <b><u>'.$order->supplier->name.'</u></b></td>
        <td width="40%" style="border-left: 1px solid black;">PO No: <b><u>'.$order->po_no.'</u></b></td>

    </tr>
    <tr>
        <td>Address: <b><u>'.$order->supplier->address.'</u></b></td>
        <td style="border-left: 1px solid black;">Date: ______________</td>
    </tr>
    <tr>
        <td>TIN: <b><u>'.$order->supplier->tin_no.'</u></b></td>
        <td style="border-left: 1px solid black;">Mode of Procurement: <b><u>'.$order->method->name.'</u></b></td>
    </tr>
</table>
<table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
    <tr>
    <td colspan="2">Gentlemen:
            <p align="center">Please furnish this office the following articles subject to the terms and conditions contained herein:</p>
    </td>
    </tr>
    <tr>
        <td width="58%" style="border-bottom: 1px solid black;"></td>
        <td width="42%" style="border-bottom: 1px solid black;"></td>
    </tr>
    <tr>
        <td style="border-left: 1px solid black;" width="60%">Place of Delivery: <b><u>'.$order->place_of_delivery.'</u></b></td>
        <td style="border-left: 1px solid black;">Delivery Term: <b>__________________</b></td>
    </tr>
     <tr>
        <td style="border-right: 1px solid black;" width="60%">Date of Delivery: <b><u>'.$order->date_of_delivery.'</u></b></td>
        <td style="border-left: 1px solid black;">Payment Term: <b>__________________</b></td>
    </tr>

</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
        <th width="10%" align="center">Stock/ Property No.</th>
        <th width="10%" align="center">Unit</th>
        <th width="40%" align="center">Item Description</th>
        <th width="10%" align="center">Quantity</th>
        <th width="10%" align="center">Unit Cost</th>
        <th width="20%" align="center">Total Cost</th>
    </tr>
    '.$data.'
    <tr>
        <td colspan="6" align="center">Nothing Follows</td>
    </tr>

    <tr>
        <td width="80%" align="left"><b>(Total Amount in Words)</b><span>    '. $totalAmountWords.'</span></td>
        <td width="20%" align="right"><b>'.number_format($total,2).'</b></td>
    </tr>
</table>
<table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
<tr>
    <td></td>
</tr>
<tr>
    <td colspan="2" style="text-indent: 20px; padding-left: 0;">
        In case of failure to make the full delivery within the time specified above, a penalty of one-tenth (1/10) percent for every day of delay shall be imposed on the undelivered item/s.
    </td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <td width="49%" style="border-collapse: collapse;">Conforme:</td>
    <td width="51%" style="border-collapse: collapse;">Very truly yours:</td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <td width="55%" align="center" style="border-collapse: collapse;"><b><u>'.$order->supplier->name.'</u></b></td>
    <td width="40%" align="center" style="border-collapse: collapse;"><b><u>FLORDELIZA C. GECOBE PHD, CESO V</u></b></td>
</tr>
<tr>
    <td width="55%" align="center" style="border-collapse: collapse;">Signature over Printed Name</td>
    <td width="39%" align="center" style="border-collapse: collapse;"> Schools Division Superintendent</td>
</tr>
<tr>
    <td width="55%" align="center" style="border-collapse: collapse;">___________________</td>
    <td width="39%" align="center" style="border-collapse: collapse;">___________________</td>
</tr>
<tr>
    <td width="54%" align="center" style="border-collapse: collapse;"><p>Date</p></td>
    <td width="41%" align="center" style="border-collapse: collapse;"><p>Date</p></td>
</tr>
</table>
<table cellpadding="5"  width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;" >
<tr>
    <td width="58%" style="border-left: 1px solid black;">Fund Cluster: <b><u>'.$order->fund_cluster->name.'</u></b></td>
    <td width="42%" style="border-left: 1px solid black;">ORS/BURS No.: <b>___________________</b></td>
 </tr>
 <tr>
    <td width="58%" style="border-left: 1px solid black;">Funds Available: <b>_______________________________</b></td>
    <td width="42%" style="border-left: 1px solid black;">Date of the ORS/BURS: <b>_____________</b></td>
 </tr>
 <tr>
    <td width="58%" style="border-left: 1px solid black;"></td>
    <td width="42%" style="border-left: 1px solid black;">Amount: <b>'.number_format($total,2).'</b></td>
 </tr>
 <tr>
    <td width="58%" align="center" style="border-left: 1px solid black;"><b><u>CHERRY ANN R. SEGUNDO, CPA</u></b></td>
    <td width="42%" style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td align="center" style="border-left: 1px solid black;">Accountant III</td>
    <td style="border-left: 1px solid black;"></td>
 </tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('PO.pdf', 'I');