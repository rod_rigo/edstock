<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plandetail $plandetail
 * @var \Cake\Collection\CollectionInterface|string[] $plans
 * @var \Cake\Collection\CollectionInterface|string[] $items
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Plandetails'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="plandetails form content">
            <?= $this->Form->create($plandetail) ?>
            <fieldset>
                <legend><?= __('Add Plandetail') ?></legend>
                <?php
                    echo $this->Form->control('plan_id', ['options' => $plans]);
                    echo $this->Form->control('code');
                    echo $this->Form->control('item_id', ['options' => $items]);
                    echo $this->Form->control('qty');
                    echo $this->Form->control('cost');
                    echo $this->Form->control('total');
                    echo $this->Form->control('jan');
                    echo $this->Form->control('feb');
                    echo $this->Form->control('mar');
                    echo $this->Form->control('apr');
                    echo $this->Form->control('may');
                    echo $this->Form->control('jun');
                    echo $this->Form->control('jul');
                    echo $this->Form->control('aug');
                    echo $this->Form->control('sep');
                    echo $this->Form->control('oct');
                    echo $this->Form->control('nov');
                    echo $this->Form->control('decm');
                    echo $this->Form->control('deleted', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
