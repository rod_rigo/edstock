<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Plan $plan
 */

?>

<script>
    const planId = parseInt(<?=intval($plan->id)?>);
</script>

<style>
    ul.ui-autocomplete {
        z-index: 1100 !important;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <?=$this->Form->create($entity,['id' => 'plan-details-form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-header">Plan Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->control('part_id',[
                            'class' => 'form-control rounded-0',
                            'empty' => ucwords('Select Part'),
                            'options' => $parts,
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('plan_title_id',ucwords('Plan title'))?>
                        <?=$this->Form->select('plan_title_id', [],[
                            'class' => 'form-control rounded-0',
                            'id' => 'plan-title-id',
                            'required' => true,
                            'empty' => ucwords('Select Plan Title')
                        ])?>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
                        <?=$this->Form->control('autocomplete',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('Type Item Name'),
                            'id' => 'autocomplete',
                            'label' => ucwords('Item Name')
                        ])?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('code',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('code'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('cost',[
                            'class' => 'form-control rounded-0',
                            'label' => ucwords('Unit Cost'),
                            'placeholder' => ucwords('unit cost'),
                            'value' => intval(0),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('jan',[
                            'class' => 'form-control rounded-0 q-1',
                            'data-id' => intval(1),
                            'placeholder' => ucwords('jan'),
                            'label' => ucwords('jan (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('feb',[
                            'class' => 'form-control rounded-0 q-1',
                            'data-id' => intval(1),
                            'placeholder' => ucwords('feb'),
                            'label' => ucwords('feb (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('mar',[
                            'class' => 'form-control rounded-0 q-1',
                            'data-id' => intval(1),
                            'placeholder' => ucwords('mar'),
                            'label' => ucwords('mar (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('q_1',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('q1'),
                            'label' => ucwords('q1 (Cost)'),
                            'value' => intval(0),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('apr',[
                            'class' => 'form-control rounded-0 q-2',
                            'data-id' => intval(2),
                            'placeholder' => ucwords('apr'),
                            'label' => ucwords('apr (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('may',[
                            'class' => 'form-control rounded-0 q-2',
                            'placeholder' => ucwords('may'),
                            'label' => ucwords('may (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('jun',[
                            'class' => 'form-control rounded-0 q-2',
                            'data-id' => intval(2),
                            'placeholder' => ucwords('jun'),
                            'label' => ucwords('jun (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('q_2',[
                            'class' => 'form-control rounded-0 q-2',
                            'data-id' => intval(2),
                            'placeholder' => ucwords('q2'),
                            'label' => ucwords('q2 (Cost)'),
                            'value' => intval(0),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('jul',[
                            'class' => 'form-control rounded-0 q-3',
                            'data-id' => intval(3),
                            'placeholder' => ucwords('jul'),
                            'label' => ucwords('jul (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('aug',[
                            'class' => 'form-control rounded-0 q-3',
                            'data-id' => intval(3),
                            'placeholder' => ucwords('aug'),
                            'label' => ucwords('aug (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('sep',[
                            'class' => 'form-control rounded-0 q-3',
                            'data-id' => intval(3),
                            'placeholder' => ucwords('sep'),
                            'label' => ucwords('sep (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('q_3',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('q3'),
                            'label' => ucwords('q3 (Cost)'),
                            'value' => intval(0),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('oct',[
                            'class' => 'form-control rounded-0 q-4',
                            'data-id' => intval(4),
                            'placeholder' => ucwords('oct'),
                            'label' => ucwords('oct (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('nov',[
                            'class' => 'form-control rounded-0 q-4',
                            'data-id' => intval(4),
                            'placeholder' => ucwords('nov'),
                            'label' => ucwords('nov (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('decm',[
                            'class' => 'form-control rounded-0 q-4',
                            'data-id' => intval(4),
                            'placeholder' => ucwords('decm'),
                            'label' => ucwords('decm (Quantity)'),
                            'value' => intval(0),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('q_4',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('q4'),
                            'label' => ucwords('q4 (Cost)'),
                            'value' => intval(0),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('qty',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('qty'),
                            'label' => ucwords('total quantity'),
                        ]);?>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('total',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('total'),
                            'label' => ucwords('total cost'),
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <?=$this->Form->control('item_id',[
                    'type' => 'hidden',
                ]);?>
                <?=$this->Form->control('plan_id',[
                    'type' => 'hidden',
                    'value' => intval($plan->id)
                ]);?>
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="modal fade" id="part-modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <?=$this->Form->create($part,['id' => 'part-form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="part-modal-header"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('part', ucwords('part'))?>
                        <?=$this->Form->text('part',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('part'),
                            'required' => true,
                            'id' => 'part',
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$this->Form->control('plan_id',[
                    'type' => 'hidden',
                    'value' => intval($plan->id)
                ]);?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="modal fade" id="plan-title-modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <?=$this->Form->create($planTitle,['id' => 'plan-title-form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="part-modal-header"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->control('part_id',[
                            'class' => 'form-control rounded-0',
                            'empty' => ucwords('Select Part'),
                            'options' => $parts,
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('plan_title', ucwords('plan title'))?>
                        <?=$this->Form->text('plan_title',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('Select plan title'),
                            'required' => true,
                            'id' => 'plan-title',
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$this->Form->control('plan_id',[
                    'type' => 'hidden',
                    'value' => intval($plan->id)
                ]);?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-edit"></i>
                Configuration
            </h3>
        </div>
        <div class="card-body">

            <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="plans-tab" data-toggle="pill" href="#plan-panel" role="tab" aria-controls="plan-panel" aria-selected="true">Plans</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="parth-tab" data-toggle="pill" href="#parts-panel" role="tab" aria-controls="parts-panel" aria-selected="true">Parts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="plan-title-tab" data-toggle="pill" href="#plan-title-panels" role="tab" aria-controls="plan-title-panels" aria-selected="true">Plan Titles</a>
                </li>
            </ul>
            <div class="tab-content" id="custom-content-below-tabContent">
                <div class="tab-pane fade show active" id="plan-panel" role="tabpanel" aria-labelledby="plans-tab">
                    <?=$this->Form->create($plan,['id' => 'form', 'type' => 'file'])?>
                        <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->control('title',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('title'),
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->control('subtitle',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('subtitle'),
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->control('prepared_by',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('prepared by'),
                            ]);?>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->control('position',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('position'),
                            ]);?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mt-4">
                            <?=$this->Form->hidden('status',['required' => true, 'id' => 'status', 'value' => intval($plan->status)])?>
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Plans', 'action' => 'index'])?>" class="btn btn-danger rounded-0 mr-2">
                                Cancel
                            </a>
                            <?=$this->Form->button('Submit',[
                                'class' => 'btn btn-success rounded-0',
                                'type' => 'submit'
                            ])?>
                        </div>

                    </div>
                    <?=$this->Form->end()?>
                </div>
                <div class="tab-pane fade" id="parts-panel" role="tabpanel" aria-labelledby="parth-tab">
                    <div class="row">

                       <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                           <button id="part-modal-toggle" class="btn btn-primary rounded-0 d-flex justify-content-end align-items-center" data-toggle="tooltip" data-placement="bottom" title="Add Department">
                               <i class=""></i> New Part
                           </button>
                       </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                            <div class="table-responsive">
                                <table id="parts-datatable" class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Parts</th>
                                        <th>Created</th>
                                        <th>
                                            Options
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="plan-title-panels" role="tabpanel" aria-labelledby="plan-title-tab">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                            <button id="plan-title-modal-toggle" class="btn btn-primary rounded-0 d-flex justify-content-end align-items-center" data-toggle="tooltip" data-placement="bottom" title="Add Department">
                                <i class=""></i> New Plan Title
                            </button>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
                            <div class="table-responsive">
                                <table id="plan-titles-datatable" class="table table-bordered table-striped" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Parts</th>
                                        <th>Title</th>
                                        <th>Created</th>
                                        <th>
                                            Options
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card -->
    </div>
</div>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <div class="card-tools">
                <button id="modal-toggle" class="btn btn-primary rounded-0" data-toggle="tooltip" data-placement="bottom" title="Add Department">
                    <i class=""></i> New Plan Details
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th rowspan="2" colspan="6" style="text-align: center; !important;">
                                        <small>Items & Specifications</small>
                                    </th>
                                    <th rowspan="2" colspan="1"><small>Unit of Measure</small></th>
                                    <th colspan="20" style="text-align: center; !important;"><small>Monthly Quantity Requirement</small></th>
                                    <th rowspan="2" colspan="1"><small>Total Quantity for the year</small></th>
                                    <th rowspan="2" colspan="1"><small>Price</small></th>
                                    <th rowspan="2" colspan="1"><small>Total Amount For The Year</small></th>
                                    <th rowspan="2" colspan="1">
                                        <small>Options</small>
                                    </th>
                                </tr>
                                <tr>
                                    <th><small>Jan</small></th>
                                    <th><small>Feb</small></th>
                                    <th><small>Mar</small></th>
                                    <th><small>Q1</small></th>
                                    <th><small>Q1 Amount</small></th>
                                    <th><small>April</small></th>
                                    <th><small>May</small></th>
                                    <th><small>June</small></th>
                                    <th><small>Q2</small></th>
                                    <th><small>Q2 Amount</small></th>
                                    <th><small>Jul</small></th>
                                    <th><small>Aug</small></th>
                                    <th><small>Sept</small></th>
                                    <th><small>Q3</small></th>
                                    <th><small>Q3 Amount</small></th>
                                    <th><small>Oct</small></th>
                                    <th><small>Nov</small></th>
                                    <th><small>Dec</small></th>
                                    <th><small>Q4</small></th>
                                    <th><small>Q4 Amount</small></th>
                                </tr>
                            </thead>
                            <tbody id="plans-tbody">
                                <?php foreach ($plan->parts as $kpart => $part):?>
                                <tr>
                                    <th colspan="34"><?=strtoupper($part->part)?></th>
                                    <?php foreach ($part->plan_titles as $kplan_title => $plan_title):?>
                                        <tr>
                                            <th colspan="34"><?=strtoupper($plan_title->plan_title)?></th>
                                        </tr>
                                        <tr>
                                            <?php foreach ($plan_title->plandetails as $kplan_detail => $plandetail):?>
                                            <tr>
                                                <th><?=intval($kplan_detail + 1)?></th>
                                                <th><?=strtoupper($plandetail->code)?></th>
                                                <th colspan="4"><?=strtoupper($plandetail->item->description)?></th>
                                                <th><?=strtoupper($plandetail->item->unit->name)?></th>
                                                <th><?=intval($plandetail->jan)?></th>
                                                <th><?=intval($plandetail->feb)?></th>
                                                <th><?=intval($plandetail->mar)?></th>
                                                <th>
                                                    <?=number_format(round((intval($plandetail->jan) + intval($plandetail->feb) + intval($plandetail->mar)),2))?>
                                                </th>
                                                <th>
                                                    <?=number_format(round(doubleval((intval($plandetail->jan) + intval($plandetail->feb) + intval($plandetail->mar)) * doubleval($plandetail->cost)),2),2)?>
                                                </th>
                                                <th><?=intval($plandetail->apr)?></th>
                                                <th><?=intval($plandetail->may)?></th>
                                                <th><?=intval($plandetail->jun)?></th>
                                                <th>
                                                    <?=number_format(round((intval($plandetail->apr) + intval($plandetail->may) + intval($plandetail->jun)),2))?>
                                                </th>
                                                <th>
                                                    <?=number_format(round(doubleval((intval($plandetail->apr) + intval($plandetail->may) + intval($plandetail->jun)) * doubleval($plandetail->cost)),2),2)?>
                                                </th>
                                                <th><?=intval($plandetail->jul)?></th>
                                                <th><?=intval($plandetail->aug)?></th>
                                                <th><?=intval($plandetail->sep)?></th>
                                                <th>
                                                    <?=number_format(round((intval($plandetail->jul) + intval($plandetail->aug) + intval($plandetail->sep)),2))?>
                                                </th>
                                                <th>
                                                    <?=number_format(round(doubleval((intval($plandetail->jul) + intval($plandetail->aug) + intval($plandetail->sep)) * doubleval($plandetail->cost)),2),2)?>
                                                </th>
                                                <th><?=intval($plandetail->oct)?></th>
                                                <th><?=intval($plandetail->nov)?></th>
                                                <th><?=intval($plandetail->decm)?></th>
                                                <th>
                                                    <?=number_format(round((intval($plandetail->oct) + intval($plandetail->nov) + intval($plandetail->decm)),2))?>
                                                </th>
                                                <th>
                                                    <?=number_format(round(doubleval((intval($plandetail->oct) + intval($plandetail->nov) + intval($plandetail->decm)) * doubleval($plandetail->cost)),2),2)?>
                                                </th>
                                                <th>
                                                    <?=number_format($plandetail->qty)?>
                                                </th>
                                                <th><?=number_format(round(doubleval($plandetail->cost),2),2)?></th>
                                                <th><?=number_format(round(($plandetail->cost * intval($plandetail->qty)),2),2)?></th>
                                                <th>
                                                    <button data-id="<?=intval($plandetail->id)?>" class="btn btn-primary btn-sm rounded-0 edit">Edit</button> |
                                                    <button data-id="<?=intval($plandetail->id)?>" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></button>
                                                </th>
                                            </tr>
                                            <?php endforeach;?>
                                        </tr>
                                    <?php endforeach;?>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    $('input').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('input').prop('disabled', false);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('input[type="number"]').keypress(function (e) {
            const regex = /^([0-9\.]){1,}$/;
            if(!(e.key).match(regex)){
                e.preventDefault();
            }
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
    $(function () {
        'use strict';

        var baseurl = mainurl+'parts/';
        var url = '';

        var datatable = $('#parts-datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:baseurl+'getParts/'+(parseInt(planId)),
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 2,
                    data: null,
                    render: function(data, type, row, meta){
                        return moment(row.created).format('Y-M-d h:m A');
                    }
                },
                {
                    targets: 3,
                    data: null,
                    render: function(data, type, row, meta){
                        return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</a> | '+
                            '<a data-id="'+row.id+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'part'},
                { data: 'created'},
                { data: 'id'},
            ]
        });

        $('#part-modal-toggle').click(function (e) {
            url = 'add';
            $('#part-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#part-modal').modal('toggle');
        });

        $('#part-modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#part-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            $('button[type="reset"]').fadeIn(100);
        });

        $('#part-form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = baseurl+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#part-modal').modal('toggle');
                $('#part-form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                table.ajax.reload(null, false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            $.ajax({
                url:baseurl+href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    url = href;
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    $('button[type="reset"]').fadeOut(100);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#part').val(data.part);
                $('#part-modal').modal('toggle');
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
    $(function () {
        'use strict';

        var baseurl = mainurl+'plan-titles/';
        var url = '';

        var datatable = $('#plan-titles-datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:baseurl+'getPlanTitles/'+(parseInt(planId)),
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 3,
                    data: null,
                    render: function(data, type, row, meta){
                        return moment(row.created).format('Y-M-d h:m A');
                    }
                },
                {
                    targets: 4,
                    data: null,
                    render: function(data, type, row, meta){
                        return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</a> | '+
                            '<a data-id="'+row.id+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'part.part'},
                { data: 'plan_title'},
                { data: 'created'},
                { data: 'id'},
            ]
        });

        $('#plan-title-modal-toggle').click(function (e) {
            url = 'add';
            $('#plan-title-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#plan-title-modal').modal('toggle');
        });

        $('#plan-title-modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#plan-title-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            $('button[type="reset"]').fadeIn(100);
        });

        $('#plan-title-form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = baseurl+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#plan-title-modal').modal('toggle');
                $('#plan-title-form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                table.ajax.reload(null, false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            $.ajax({
                url:baseurl+href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    url = href;
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    $('button[type="reset"]').fadeOut(100);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#plan-title-form #part-id').val(data.part_id);
                $('#plan-title').val(data.plan_title);
                $('#plan-title-modal').modal('toggle');
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
    $(function () {
        'use strict';

        var baseurl = mainurl+'plandetails/';
        var url = '';

        $('#modal-toggle').click(function (e) {
            url = 'add';
            $('#plan-details-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#modal').modal('toggle');
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#plan-details-form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            $('button[type="reset"]').fadeIn(100);
        });

        $('#plan-details-form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = baseurl+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                getPlanTable();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $(document).on('click','button.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            $.ajax({
                url:baseurl+href,
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    url = href;
                    $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                    $('button[type="reset"]').fadeOut(100);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                planTitles(planId, data.part_id);
                $('#code').val(data.code);
                $('#cost').val(data.cost);
                $('#jan').val(data.jan);
                $('#feb').val(data.feb);
                $('#mar').val(data.mar);
                $('#apr').val(data.apr);
                $('#may').val(data.may);
                $('#jun').val(data.jun);
                $('#jul').val(data.jul);
                $('#aug').val(data.aug);
                $('#sep').val(data.sep);
                $('#oct').val(data.oct);
                $('#nov').val(data.nov);
                $('#decm').val(data.decm);
                $('#total').val(data.total);
                $('#item-id').val(data.item_id);
                $('#plan-details-form  #part-id').val(data.part_id);
                $('#modal').modal('toggle');
                $('#autocomplete').val((data.item.description).toUpperCase());
                setTimeout(function () {
                    $('#plan-title-id').val(data.plan_title_id);
                    Swal.close();
                }, 1500);
                $('#qty').val(data.qty);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        });

        $(document).on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        getPlanTable();
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });


        $('#jan, #feb, #mar').on('input', function (e) {

            var cost = (parseInt($('#cost').val()));

            var jan = $('#jan').val();
            var feb = $('#feb').val();
            var mar = $('#mar').val();

            var q1 = parseFloat(jan) + parseFloat(feb) + parseFloat(mar);

            var totalq1 = parseInt(q1) * parseFloat(cost);

            if(!isNaN(q1)){
                $('#q-1').val(parseFloat(totalq1).toFixed(2));
            }

            costs();
        });

        $('#apr, #may, #jun').on('input', function (e) {

            var cost = (parseInt($('#cost').val()));

            var apr = $('#apr').val();
            var may = $('#may').val();
            var jun = $('#jun').val();

            var q2 = parseFloat(apr) + parseFloat(may) + parseFloat(jun);

            var totalq2 = parseInt(q2) * parseFloat(cost);

            if(!isNaN(q2)){
                $('#q-2').val(parseFloat(totalq2).toFixed(2));
            }

            costs();
        });

        $('#jul, #aug, #sep').on('input', function (e) {

            var cost = (parseInt($('#cost').val()));

            var jul = $('#jul').val();
            var aug = $('#aug').val();
            var sep = $('#sep').val();

            var q3 = parseFloat(jul) + parseFloat(aug) + parseFloat(sep);

            var totalq3 = parseInt(q3) * parseFloat(cost);

            if(!isNaN(q3)){
                $('#q-3').val(parseFloat(totalq3).toFixed(2));
            }

            costs();
        });

        $('#oct, #nov, #decm').on('input', function (e) {

            var cost = (parseInt($('#cost').val()));

            var oct = $('#oct').val();
            var nov = $('#nov').val();
            var decm = $('#decm').val();

            var q4 = parseFloat(oct) + parseFloat(nov) + parseFloat(decm);

            var totalq4 = parseInt(q4) * parseFloat(cost);

            if(!isNaN(q4)){
                $('#q-4').val(parseFloat(totalq4).toFixed(2));
            }

            costs();
        });

        $('#cost').on('input', function (e) {
           var value = $(this).val();
           if((value).length){
               costs();
           }
        });

        $('#plan-details-form  #part-id').change(function (e) {
            var value = $(this).val();
            planTitles(planId, value);
        });

        function costs() {

            var cost = (parseInt($('#cost').val()));

            var quantity = 0;
            var grandtotal = 0;

            var jan = $('#jan').val();
            var feb = $('#feb').val();
            var mar = $('#mar').val();

            var q1 = parseFloat(jan) + parseFloat(feb) + parseFloat(mar);

            var totalq1 = parseInt(q1) * parseFloat(cost);

            if(!isNaN(q1)){
                $('#q-1').val(parseFloat(totalq1).toFixed(2));
            }

            var apr = $('#apr').val();
            var may = $('#may').val();
            var jun = $('#jun').val();

            var q2 = parseFloat(apr) + parseFloat(may) + parseFloat(jun);

            var totalq2 = parseInt(q2) * parseFloat(cost);

            if(!isNaN(q2)){
                $('#q-2').val(parseFloat(totalq2).toFixed(2));
            }

            var jul = $('#jul').val();
            var aug = $('#aug').val();
            var sep = $('#sep').val();

            var q3 = parseFloat(jul) + parseFloat(aug) + parseFloat(sep);

            var totalq3 = parseInt(q3) * parseFloat(cost);

            if(!isNaN(q3)){
                $('#q-3').val(parseFloat(totalq3).toFixed(2));
            }

            var oct = $('#oct').val();
            var nov = $('#nov').val();
            var decm = $('#decm').val();

            var q4 = parseFloat(oct) + parseFloat(nov) + parseFloat(decm);

            var totalq4 = parseInt(q4) * parseFloat(cost);

            if(!isNaN(q4)){
                $('#q-4').val(parseFloat(totalq4).toFixed(2));
            }

            quantity = parseInt(q1) + parseInt(q2) + parseInt(q3) + parseInt(q4);
            grandtotal = parseFloat(totalq1) + parseFloat(totalq2) + parseFloat(totalq3) + parseFloat(totalq4);

            if(!isNaN(parseInt(quantity)) && !isNaN(parseInt(grandtotal))){
                $('#qty').val(quantity);
                $('#total').val(parseFloat(grandtotal).toFixed(2));
            }

        }

        function getItemLists() {
            $.ajax({
                url:mainurl+'items/getItemsPansLists',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#autocomplete').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#autocomplete').autocomplete({
                    delay: 100,
                    minLength: 2,
                    autoFocus: true,
                    source: function (request, response) {
                        response($.map(data, function (value, key) {
                            var name = value.description.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    key: value.id,
                                    label: value.description,
                                    unit: value.unit.name,
                                    price: value.price,
                                    item_code: value.item_code,
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    create: function (event, ui) {
                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                            return $('<li></li>')
                                .data('item.autocomplete', item)
                                .append('<li>'+(item.label)+'</li>')
                                .appendTo(ul);
                        };
                    },
                    focus: function(e, ui) {
                        e.preventDefault();
                    },
                    select: function(e, ui) {
                        $('#autocomplete').blur();
                        $('#autocomplete').val(ui.item.label);
                        $('#item-id').val(ui.item.key);
                        $('#cost').val(ui.item.price);
                        $('#code').val(ui.item.item_code);
                    },
                    change: function(e, ui ) {
                        e.preventDefault();
                    },
                });
                $('#autocomplete').prop('disabled', false);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function planTitles(planId, partId) {
            $.ajax({
                url: mainurl+'plan-titles/getPlanTitlesLists/'+(parseInt(planId))+'/'+(parseInt(partId)),
                method:'GET',
                type:'GET',
                dataType:'JSON',
                beforeSend:function () {
                    $('#plan-title-id').prop('disabled', true).empty();
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                $('#plan-title-id').append('<option value="">Select Plan Title</option>');
                $.map(data, function (data, key) {
                    $('#plan-title-id').append('<option value="'+(key)+'">'+((data).toUpperCase())+'</option>');
                });
                $('#plan-title-id').prop('disabled', false);
                Swal.close();
            }).fail(function (xhr, status, error) {
                $('#plan-title-id').prop('disabled', false);
            });
        }

        function getPlanTable() {
            $.ajax({
                url:mainurl+'plans/getPlanTable/'+(planId),
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {
                    $('#plans-tbody').empty();
                },
            }).done(function (data, status, xhr) {
                console.log(data);
                $('#plans-tbody').html(data.html);
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

        getItemLists();
    });
</script>
