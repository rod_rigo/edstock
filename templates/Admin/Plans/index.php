<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Plan> $plans
 */
?>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-header">Plan Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('title',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('title'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('subtitle',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('subtitle'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('prepared_by',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('prepared by'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('position',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('position'),
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$this->Form->hidden('status',['required' => true, 'id' => 'status', 'value' => intval(0)])?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <button id="modal-toggle" class="btn btn-primary rounded-0" data-toggle="tooltip" data-placement="bottom" title="Add Department">
                    <i class=""></i> New Plan
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Subtitle</th>
                                <th>Prepared By</th>
                                <th>Position</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>
                                    Options
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'plans/';
        var url = '';
        var statuses = ['INACTIVE', 'ACTIVE'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:baseurl+'getPlans',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data, type, row, meta){
                        var status = (row.status)? '<span class="badge badge-success">'+(statuses[parseInt(row.status)])+'</span>': '<span class="badge badge-danger">'+(statuses[parseInt(row.status)])+'</span>';
                        return status;
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data, type, row, meta){
                        return moment(row.created).format('Y-M-d h:m A');
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data, type, row, meta){
                        var status = (row.status)? '<a data-id="'+(row.id)+'" status="0" class="btn btn-warning btn-sm rounded-0 status">Inactive</i></a>':
                                        '<a data-id="'+(row.id)+'" status="1" class="btn btn-success btn-sm rounded-0 status">Active</i></a>';
                        return  '<a data-id="'+(row.id)+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</a> | '+
                            '<a data-id="'+(row.id)+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a> | '+
                            (status);
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'title'},
                { data: 'subtitle'},
                { data: 'prepared_by'},
                { data: 'position'},
                { data: 'status'},
                { data: 'created'},
                { data: 'id'}
            ]
        });

        $('#modal-toggle').click(function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#modal').modal('toggle');
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            $('button[type="reset"]').fadeIn(100);
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = baseurl+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.status',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var status = $(this).attr('status');
            var href = baseurl+'status/'+(dataId)+'/'+(status);
            Swal.fire({
                title: 'Mark As '+(statuses[parseInt(status)])+'?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
