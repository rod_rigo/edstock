<?php

$checked = WWW_ROOT. 'img'. DS. 'checked.png';

$file_info = new finfo(FILEINFO_MIME_TYPE);
$mime_type = $file_info->buffer(file_get_contents($checked));
$image = 'data:'.(strtolower($mime_type)).';base64,'.(base64_encode(file_get_contents($checked)));

if(boolval($custodian->is_expendable)){

    class xtcpdf extends TCPDF {
        public function Header($name = null) {
            $this->setY(10);
            $this->Ln(3);
            $this->SetFont('times', 'I', 9);
            $this->Cell(0, 0, 'Appendix 63', 0, 01, 'R');
            $this->Ln(5);
            $this->SetFont('timesB', 'B', 15);
            $this->Cell(0, 0, 'REQUISITION AND ISSUE SLIP', 0, 0, 'C');
            $this->Ln(6);
        }
    }

    $pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
    $pdf->SetMargins(10, 35, 5, true);
    $pdf->SetAutoPageBreak(true, 10);
    $pdf->SetFont('times','',12);
    $style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
    $pdf->AddPage();

    $data = '';
    $ctr = 0;
    $total = 0;
    foreach ($custodian->request_item_details as $details){
        $data .='<tr>
        <td align="center">'.($ctr+1).'</td>
        <td align="center">'.$details->item->unit->name.'</td>
        <td align="center">'.$details->item->description.'</td>
        <td align="center">'.number_format($details->qty).'</td>
        <td align="center"><img src="'.($image).'" alt="" height="10" width="10"></td>
        <td align="center">'.$details->is_available.'</td>
        <td align="center">'.number_format($details->qty).'</td>
        <td align="center">'.$details->remarks.'<b>COMPLETE</b></td>
        </tr>
        ';
        $ctr++;
        $total +=$details->total;
    }

    $html = '<table width="100%">
    <tr>
    <td width="60%"><p>Entity Name: <b>'.$custodian->request_item->office->name.'</b></p></td>
    <td width="1%"></td>
    <td width="35%"><p>Fund Cluster: <b>_____________</b></p></td>
    </tr>
    </table>
    <table width="100%" cellpadding="2" style="border-collapse: collapse; border: 1px solid black;">
    <tr>
    <td width="58%" style="font-size: small;">Division: <b><u>'.$custodian->request_item->office->name.'</u></b></td>
    <td width="41%" align="left" style="font-size: small; border-left: 1px solid black;">Responsibility Center Code:</td>
    </tr>
    <tr>
    <td style="font-size: small;">Office:<b><u>'.$custodian->request_item->department->name.'</u></b></td>
    <td style="font-size: small; border-left: 1px solid black;" >RIS No: <b><u>'.$custodian->series_number.'</u></b></td>
    </tr>
    </table>
    <table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
    <td style="font-size: small;" width="49%" align="center"><b><i>Requisition</i></b></td>
    <td style="font-size: small;" width="18%" align="center"><b><i>Stock Available?</i></b></td>
    <td style="font-size: small;" width="32%" align="center"><b><i>Issue</i></b></td>
    </tr>
    </table>
    <table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
    <th width="7%" align="center">Stock No.</th>
    <th width="5%" align="center">Unit</th>
    <th width="25%" align="center">Item Description</th>
    <th width="12%" align="center">Quantity</th>
    <th width="9%" align="center">Yes</th>
    <th width="9%" align="center">No</th>
    <th width="13%" align="center">Quantity</th>
    <th width="19%" align="center">Remarks</th>
    </tr>
    '.$data.'
    <tr>
    <td colspan="8" align="center">Nothing Follows</td>
    </tr>
    <tr>
    <td colspan="8">Purpose:
    <p align="center"><u>'.$custodian->request_item->purpose.'</u></p>
    </td>
    </tr>
    
    </table>
    <table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
    <tr>
    <td width="12%"></td>
    <td width="25%" style="font-size: small; border-left: 1px solid black;"><p>Requested By: <b></b></p></td>
    <td width="21%" style="font-size: small; border-left: 1px solid black;"><p>Approved By: <b></b></p></td>
    <td width="22%" style="font-size: small; border-left: 1px solid black;"><p>Issued By: <b></b></p></td>
    <td width="19%" style="font-size: small; border-left: 1px solid black;"><p>Received By: <b></b></p></td>
    </tr>
    <tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Signature:</td>
    <td width="25%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="21%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="22%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="19%" style="border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td></td>
    </tr>
    <tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Printed Name:</td>
    <td width="25%" align="center" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>'.(strtoupper($custodian->request_item->user->fullname)).'</b></td>
    <td width="21%" align="center" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>'.$coordinator->name.'</b></td>
    <td width="22%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>'.(strtoupper($user->fullname)).'</b></td>
    <td width="19%" align="center" style="font-size: 6px; border-left: 1px solid black; border-bottom: 1px solid black;"><b>'.(strtoupper($custodian->request_item->user->fullname)).'</b></td>
    </tr>
    <tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Designation:</td>
    <td width="25%" align="center" style="font-size: 7px; border-left: 1px solid black; border-bottom: 1px solid black;">'.(strtoupper($custodian->request_item->user->position)).'</td>
    <td width="21%" align="center" style="font-size: 7px; border-left: 1px solid black; border-bottom: 1px solid black;">'.$coordinator->position.'</td>
    <td width="22%" align="center" style="font-size: 7px; border-left: 1px solid black; border-bottom: 1px solid black;">'.(strtoupper($user->position)).'</td>
    <td width="19%" align="center" style="font-size: 7px; border-left: 1px solid black; border-bottom: 1px solid black;">'.(strtoupper($custodian->request_item->user->position)).'</td>
    </tr>
    <tr>
    <td width="12%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;">Date:</td>
    <td width="25%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="21%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="22%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    <td width="19%" style="font-size: 8px; border-left: 1px solid black; border-bottom: 1px solid black;"></td>
    </tr>
    </table>';

    $pdf->writeHTML($html, true, false, true, false, 'L');
    $pdf->LastPage();
    $pdf->Output('PR.pdf', 'I');
    
}else{

    class xtcpdf extends TCPDF {
        public function Header($name = null) {
            $this->setY(10);
            $this->Ln(3);
            $this->SetFont('times', 'I', 9);
            $this->Cell(0, 0, 'Appendix 59', 0, 01, 'R');
            $this->Ln(5);
            $this->SetFont('timesB', 'B', 15);
            $this->Cell(0, 0, 'INVENTORY CUSTODIAN SLIP', 0, 0, 'C');
            $this->Ln(6);
        }
    }

    $html = '';

    $pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
    $pdf->SetMargins(10, 35, 10, true);
    $pdf->SetFont('times','',12);
    $style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
    $pdf->AddPage();

    $data = '';
    $ctr = 0;
    $total = 0;

    $sphv = '';
    $sphvcounter = 0;
    $sphvtotal = 0;

    foreach ($custodian->request_item_details as $details){

        if(doubleval($details->item->price) < doubleval(5000)){
            $data .='<tr>
            <td align="center">'.number_format($details->qty).'</td>
            <td align="center">'.$details->item->unit->name.'</td>
            <td align="center">'.number_format($details->item->price).'</td>
            <td align="center">'.number_format($details->total).'</td>
            <td align="center">'.$details->item->description.'</td>
            <td align="center"></td>
            <td align="center"></td>
            </tr>
            ';
            $ctr++;
            $total +=$details->total;
        }else{
            $sphv .='<tr>
            <td align="center">'.number_format($details->qty).'</td>
            <td align="center">'.$details->item->unit->name.'</td>
            <td align="center">'.number_format($details->item->price).'</td>
            <td align="center">'.number_format($details->total).'</td>
            <td align="center">'.$details->item->description.'</td>
            <td align="center"></td>
            <td align="center"></td>
            </tr>
            ';
            $sphvcounter++;
            $sphvtotal +=$details->total;
        }

    }

    if($ctr > 0){

        $html .= '<table width="100%">
        <tr>
        <td width="60%"><p>Entity Name: <b>'.$custodian->request_item->office->name.'</b></p></td>
        <td width="1%"></td>
        </tr>
        <tr>
        <td width="75%">Fund Cluster: <b>_______________________</b></td>
        <td width="40%">ICS No.: <b><u>SPLV-'.$custodian->series_number.'</u></b></td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
        <table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
        <tr>
        <th width="8%" align="center"><b>Quantity</b></th>
        <th width="5%" align="center"><b>Unit</b></th>
        <th width="20%" style="text-align: center;"><b>Amount</b></th>
        <th width="40%" align="center"><b>Item Description</b></th>
        <th width="13%" align="center"><b>Inventory Item No.</b></th>
        <th width="14.58%" align="center"><b>Estimated Useful Life</b></th>
        </tr>
        <tr>
        <th></th>
        <th></th>
        <th width="10%" align="center">Unit Cost</th>
        <th width="10%" align="center">Total Cost</th>
        <th width="40%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="13%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="14.58%"></th>
        </tr>
        '.$data.'
        <tr>
        <td colspan="8" align="center">Nothing Follows</td>
        </tr>
        </table>
        <table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
        <tr>
        <td width="53%">Received from:</td>
        <td width="47.58%" align="left" style="border-collapse: collapse; border-left: 1px solid black;">Received by:</td>
        </tr>
        <tr>
        <td width="53%"></td>   
        <td width="53%" style="border-collapse: collapse; border-left: 1px solid black;"></td>
        </tr>
        <tr>
        <td width="53%" align="center"><b><u>JAMES PAUL R. SANTIAGO</u></b></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u>'.(strtoupper($custodian->request_item->user->fullname)).'</u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('signature over printed name')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">'.(ucwords('signature over printed name')).'</td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('administrative officer IV')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u>'.(strtoupper($custodian->request_item->user->position)).'</u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">Position/Office</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Position/Office</td>
        </tr>
        <tr>
        <td width="53%" align="center"><u>'.((new DateTime($custodian->created))->format('m/d/Y')).'</u></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><u>'.((new DateTime($custodian->created))->format('m/d/Y')).'</u></td>
        </tr>
        <tr>
        <td width="53%" align="center">Date</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Date</td>
        </tr>
        </table>';

    }

    if($sphvcounter > 0){

        if($ctr > 0){
            $html .= '<div style="page-break-before:always"></div>';
        }

        $html .= '<table width="100%">
        <tr>
        <td width="60%"><p>Entity Name: <b>'.$custodian->request_item->office->name.'</b></p></td>
        <td width="1%"></td>
        </tr>
        <tr>
        <td width="75%">Fund Cluster: <b>_______________________</b></td>
        <td width="40%">ICS No.: <b><u>SPHV-'.$custodian->series_number.'</u></b></td>
        </tr>
        <tr>
        <td></td>
        </tr>
        </table>
        <table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
        <tr>
        <th width="8%" align="center"><b>Quantity</b></th>
        <th width="5%" align="center"><b>Unit</b></th>
        <th width="20%" style="text-align: center;"><b>Amount</b></th>
        <th width="40%" align="center"><b>Item Description</b></th>
        <th width="13%" align="center"><b>Inventory Item No.</b></th>
        <th width="14.58%" align="center"><b>Estimated Useful Life</b></th>
        </tr>
        <tr>
        <th></th>
        <th></th>
        <th width="10%" align="center">Unit Cost</th>
        <th width="10%" align="center">Total Cost</th>
        <th width="40%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="13%" style="border-collapse: collapse; border: 1px solid black;"></th>
        <th width="14.58%"></th>
        </tr>
        '.$sphv.'
        <tr>
        <td colspan="8" align="center">Nothing Follows</td>
        </tr>
        </table>
        <table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
        <tr>
        <td width="53%">Received from:</td>
        <td width="47.58%" align="left" style="border-collapse: collapse; border-left: 1px solid black;">Received by:</td>
        </tr>
        <tr>
        <td width="53%"></td>   
        <td width="53%" style="border-collapse: collapse; border-left: 1px solid black;"></td>
        </tr>
        <tr>
        <td width="53%" align="center"><b><u>JAMES PAUL R. SANTIAGO</u></b></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u>'.(strtoupper($custodian->request_item->user->fullname)).'</u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('signature over printed name')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">'.(ucwords('signature over printed name')).'</td>
        </tr>
        <tr>
        <td width="53%" align="center">'.(ucwords('administrative officer IV')).'</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><b><u>'.(strtoupper($custodian->request_item->user->position)).'</u></b></td>
        </tr>
        <tr>
        <td width="53%" align="center">Position/Office</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Position/Office</td>
        </tr>
        <tr>
        <td width="53%" align="center"><u>'.((new DateTime($custodian->created))->format('m/d/Y')).'</u></td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;"><u>'.((new DateTime($custodian->created))->format('m/d/Y')).'</u></td>
        </tr>
        <tr>
        <td width="53%" align="center">Date</td>
        <td width="53%" align="center" style="border-collapse: collapse; border-left: 1px solid black;">Date</td>
        </tr>
        </table>';

    }

    $pdf->writeHTML($html, true, false, true, false, 'L');
    $pdf->LastPage();
    $pdf->Output('PR.pdf', 'I');

}
