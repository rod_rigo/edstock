<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<script>
    const id = parseInt(<?=intval($requestItem->id)?>);
</script>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Request-Items', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($requestItem,['id'=>'form', 'type'=> 'file'])?>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('requested_by', ucwords('Requested By'))?>
                            <?=$this->Form->text('requested_by',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('Requested By'),
                                'value' => $requestItem->user->fullname,
                                'readonly' => true,
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'empty' => ucwords('select office')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'empty' => ucwords('select department')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose')
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Unit</th>
                                        <th>Item Quantity</th>
                                        <th>Cost</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($requestItem->request_item_details as $key => $request_item_detail):?>
                                    <?=$this->Form->hidden('request_item_details.'.(intval($key)).'.id',[
                                        'id' => 'request-item-details-'.(intval($key)).'-id',
                                        'required' => true,
                                        'value' => intval($request_item_detail->id)
                                    ])?>
                                    <?=$this->Form->hidden('request_item_details.'.(intval($key)).'.is_approved',[
                                        'id' => 'request-item-details-'.(intval($key)).'-is-approved',
                                        'required' => true,
                                        'value' => intval($request_item_detail->is_approved),
                                        'class' => 'is-approved'
                                    ])?>
                                    <tr>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->description)?>
                                        </td>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->unit->name)?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->item->stock))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->cost))?>
                                        </td>
                                        <td>
                                            <?=number_format(intval($request_item_detail->qty))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->total))?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->hidden('series_number',[
                'id' => 'series-number',
                'required' => true,
                'value' => intval(0)
            ])?>
            <?=$this->Form->hidden('status',[
                'id' => 'status',
                'required' => true,
            ])?>
            <?php if(!in_array(intval($requestItem->status),[intval(1),intval(2),intval(3)])):?>
                <button type="button" class="btn btn-danger rounded-0" id="decline">Decline</button>
                <button type="submit" class="btn btn-primary rounded-0">Save</button>
                <button type="button" class="btn btn-success rounded-0" id="approved">Approve</button>
            <?php endif;?>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'request-items/';

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+'edit/'+(id),
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.reload();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('#approved').click(function (e) {
            Swal.fire({
                title: 'Approved Request?',
                text: 'Once You Approved This Request You Can No Longer Update Or Decline This Request',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#status').val(parseInt(1));
                    $('.is-approved').val(1);
                    setTimeout(function () {
                        $('#form').submit();
                    },1000);
                }else{
                    $('.is-approved').val(0);
                    $('#status').val(parseInt(0));
                }
            });
        });

        $('#decline').click(function (e) {
            Swal.fire({
                title: 'Decline Request?',
                text: 'Once You Decline This Request You Can No Longer Update Or Approve This Request',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $('#status').val(parseInt(2));
                    $('.is-approved').val(0);
                    setTimeout(function () {
                        $('#form').submit();
                    },1000);
                }else{
                    $('.is-approved').val(0);
                    $('#status').val(parseInt(0));
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
