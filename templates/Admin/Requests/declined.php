<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Requests List Approved</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-7 col-lg-7 mb-2">
                    <?=$this->Form->label('interval', ucwords('Interval'))?>
                    <?=$this->Form->select('interval',[
                        ['value' => strtolower('day'), 'text' => strtoupper('day')],
                        ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                        ['value' => strtolower('month'), 'text' => strtoupper('month')],
                        ['value' => strtolower('week'), 'text' => strtoupper('week')],
                        ['value' => strtolower('year'), 'text' => strtoupper('year')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'interval',
                    ])?>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <table id="datatable" class="table table-bordered table-striped" style="font-size: small;">
                        <thead>
                        <tr>
                            <th>Series No.</th>
                            <th>Process By</th>
                            <th>Office.</th>
                            <th>Purpose</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'requests/';
        var url = '';
        var status = ['PENDING', 'APPROVED', 'DECLINED'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order: [ [5, 'desc'] ],
            ajax:{
                url:baseurl+'getRequestsDeclined/day',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 4,
                    data: null,
                    render: function(data,type,row,meta){
                        return status[parseInt(row.status)];
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data, type, row, meta){
                        var del = (row.orders.length)? '': ' | <a data-id="'+(row.id)+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a> | ';
                        return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a>'+
                            (del)+
                            '<a data-id="'+row.id+'" class="btn btn-info btn-sm rounded-0 pdf">PDF</a>';
                    }
                }
            ],
            columns: [
                { data: 'series_number'},
                { data: 'user.fullname'},
                { data: 'office.name'},
                { data: 'purpose'},
                { data: 'status'},
                { data: 'created'},
                { data: 'id'}
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'printRequest/'+dataId;
            Swal.fire({
                title: 'Export File?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        $('#interval').change(function (e) {
            var value = $(this).val();
            table.ajax.url( baseurl+'getRequestsDeclined/'+(value) ).load(null, true);
        });

        setInterval(function () {
            var value = $('#interval').val();
            table.ajax.url( baseurl+'getRequestsDeclined/'+(value) ).load(null, true);
        }, 150000);

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>