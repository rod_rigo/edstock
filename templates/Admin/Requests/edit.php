<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<?php $ctr = 0; ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($request,['id'=>'requests-form'])?>
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5 mb-3">
                    <?=$this->Form->control('created_by',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->user->fullname,
                        'readonly' => true
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'empty' => ucwords('select office')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                                'empty' => ucwords('select fund cluster')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'empty' => ucwords('select departments')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose')
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            Items
                            <div class="card-tools">
                                <a href="" id="edit-search" data-toggle="tooltip" data-placement="bottom" title="Search Items"><i class="fas fa-search"></i></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                <thead>
                                <tr>
                                    <th width="15%">Property No</th>
                                    <th width="10%">Unit</th>
                                    <th width="20%">Description</th>
                                    <th width="25%">Quantity</th>
                                    <th width="15%">Unit Cost</th>
                                    <th width="15%">Total Cost</th>
                                </tr>
                                </thead>
                                <tbody id="items">
                                <?php foreach ($request->requestdetails as $details): ?>
                                    <tr>
                                        <td><?= $ctr + 1 ?></td>
                                        <td><?= $details->item->unit->name ?></td>
                                        <td><?= $details->item->description ?></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <button type="button" class="btn btn-outline-secondary edit-minus" data-field="requestdetails[<?= $ctr ?>][qty]">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="number" name="requestdetails[<?= $ctr ?>][qty]" class="form-control input-number" value="<?= $details->qty ?>" step="any" min="0">
                                                <input type="hidden" name="requestdetails[<?= $ctr ?>][item_id]" value="<?= $details->item_id ?>">
                                                <input type="hidden" name="requestdetails[<?= $ctr ?>][id]" value="<?= $details->id ?>">
                                                <span class="input-group-append">
                                                    <button type="button" class="btn btn-outline-secondary edit-plus"  data-field="requestdetails[<?= $ctr ?>][qty]">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="number" name="requestdetails[<?= $ctr ?>][cost]" class="form-control cost" value="<?= $details->cost ?>" step="any" readonly="readonly"></td>
                                        <td><input type="number" name="requestdetails[<?= $ctr ?>][total]" class="form-control total" value="<?= $details->total ?>" step="any" readonly="readonly"></td>

                                    </tr>
                                    <?php
                                    $ctr++;
                                endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <input type="hidden" name="id" id="id" value="<?= $request->id ?>">
            <?php if(!boolval($request->status)):?>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'add', $request->id]);?>" class="btn btn-success rounded-0">Approve</a>
                <button class="btn btn-danger rounded-0" id="decline" data-id="<?=$request->id?>" status="0">Decline</button>
            <?php endif;?>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>



<script>
    'use strict';
    $(document).ready(function () {

        $('#decline').click(function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var status = $(this).attr('status');
            var href = mainurl+'requests/decline/'+dataId+'/'+status;
            Swal.fire({
                title: 'Decline Request?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend: function (e) {
                            Swal.fire({
                                icon: 'info',
                                title: null,
                                text: 'Please Wait!...',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        },
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (data, status, xhr) {
                        swal('error', 'Error', data.responseJSON.message);
                    });
                }
            });
        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    })
</script>