<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Requests List</h3>
            <div class="card-tools">
                <?= $this->Html->link('New Request','/admin/requests/add',
                ['id'=>'add', 'class'=>'btn btn-primary rounded-0','data-toggle'=>'tooltip','data-placement'=>'bottom', 'title'=>'Add Request','escape'=>false]) ?>
            </div>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped" style="font-size: small;">
                <thead>
                    <tr>
                        <th>Series No.</th>
                        <th>Process By</th>
                        <th>Office.</th>
                        <th>Purpose</th>
                        <th>Status</th>
                        <th>Created</th>
                        <th>Option</th>
                    </tr>   
                </thead>
            </table>
        </div>
    </div>
</div>
<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'requests/';
        var url = '';
        var status = ['PENDING', 'APPROVED', 'DECLINED'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
             processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            order:[[5, 'desc']],
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:baseurl+'getRequests',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
//                {
//                    targets: 0,
//                    render: function ( data, type, full, meta ) {
//                        const row = meta.row;
//                        return  row+1;
//                    }
//                },
                {
                    targets: 4,
                    data: null,
                    render: function(data,type,row,meta){
                        return status[parseInt(row.status)];
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data, type, row, meta){
                        var status = (row.status == 0)? ' | <a data-id="'+(row.id)+'" class="btn btn-success btn-sm rounded-0 status" status="1">Approve</i></a> | ' +
                                                        '<a data-id="'+(row.id)+'" class="btn btn-warning btn-sm rounded-0 status" status="2">Decline</i></a>': '';
                        var approved = (row.status == 1 && !(row.orders).length)? '| <a data-id="'+(row.id)+'" class="btn btn-success btn-sm rounded-0 orders">Orders</i></a>': '';
                        var del = (row.orders.length)? '': '<a data-id="'+(row.id)+'" class="btn btn-danger btn-sm rounded-0 approved">Delete</i></a> | ';
                        return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a> | '+
                            (del)+
                            '<a data-id="'+row.id+'" class="btn btn-info btn-sm rounded-0 pdf">PDF</a>'+
                            (status) + (approved);
                    }
                }
            ],
            columns: [
                { data: 'series_number'},
                { data: 'user.fullname'},
                { data: 'office.name'},
                { data: 'purpose'},
                { data: 'status'},
                { data: 'created'},
                { data: 'id'}
            ]
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = mainurl+'requests/'+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.orders',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            window.location.replace(mainurl+'orders/add/'+(parseInt(dataId)));
        });

        datatable.on('click','.stocks',function (e) {
            var dataId = $(this).attr('data-id');
            url = 'add/'+(dataId);
            $('#form')[0].reset();
            $('#item-id').val(parseInt(dataId));
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#modal').modal('toggle');
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.status',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var stat = $(this).attr('status');
            var href = baseurl+'status/'+(parseInt(dataId))+'/'+(parseInt(stat));
            Swal.fire({
                title: 'Mark As '+(status[parseInt(stat)])+'?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'print-request/'+dataId;
            window.open(baseurl+href);
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

        setInterval(function () {
            table.ajax.reload(null, false);
        }, 10000);

    });
</script>