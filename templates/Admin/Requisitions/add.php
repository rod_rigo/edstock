<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Requisition $requisition
 * @var \Cake\Collection\CollectionInterface|string[] $requests
 * @var \Cake\Collection\CollectionInterface|string[] $inspections
 * @var \Cake\Collection\CollectionInterface|string[] $orders
 * @var \Cake\Collection\CollectionInterface|string[] $fundClusters
 * @var \Cake\Collection\CollectionInterface|string[] $offices
 */
?>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requisitions', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'edit', intval($request->id)])?>" class="btn btn-info rounded-0">
                    View Requests
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Orders', 'action' => 'edit', intval($order->id)])?>" class="btn btn-info rounded-0">
                    View Orders
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Inspections', 'action' => 'edit', intval($inspection->id)])?>" class="btn btn-info rounded-0">
                    View Inspections
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($requisition,['id'=>'form', 'type' => 'file'])?>
            <div class="row mb-3">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <?=$this->Form->control('created_by',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->user->fullname,
                        'readonly' => true,
                    ])?>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <?=$this->Form->control('purpose',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->purpose,
                        'readonly' => true,
                    ])?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                            ])?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                    <thead>
                                    <tr>
                                        <th style="width: 1em;">Property No</th>
                                        <th>Unit</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Total Cost</th>
                                        <th style="width: 1em;">Stock Available</th>
                                        <th>Remarks</th>
                                    </tr>
                                    </thead>
                                    <tbody id="items">
                                    <?php $i = 1;?>
                                    <?php foreach ($request->requestdetails as $key => $requestdetail):?>
                                        <tr>
                                            <td style="text-align: center;"><?=intval($i++)?></td>
                                            <td style="text-align: center;">
                                                <?=strtoupper($requestdetail->item->unit->name)?>
                                                <?=$this->Form->hidden('requisition_items.'.($key).'.item_id',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($requestdetail->item->id),
                                                    'id' => 'requisition-items-'.($key).'-item-id'
                                                ])?>
                                            </td>
                                            <td style="text-align: center;"><?=strtoupper($requestdetail->item->description)?></td>
                                            <td style="text-align: center;">
                                                <?=strtoupper($requestdetail->qty)?>
                                                <?=$this->Form->hidden('requisition_items.'.($key).'.quantity',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($requestdetail->qty),
                                                    'id' => 'requisition-items-'.($key).'-quantity'
                                                ])?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?=strtoupper(number_format($requestdetail->cost))?>
                                                <?=$this->Form->hidden('requisition_items.'.($key).'.cost',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($requestdetail->cost),
                                                    'id' => 'requisition-items-'.($key).'-cost'
                                                ])?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?=strtoupper(number_format($requestdetail->total))?>
                                                <?=$this->Form->hidden('requisition_items.'.($key).'.total',[
                                                    'required' => true,
                                                    'readonly' => true,
                                                    'value' => intval($requestdetail->total),
                                                    'id' => 'requisition-items-'.($key).'-total'
                                                ])?>
                                            </td>
                                            <td style="text-align: center;">
                                                <div class="icheck-primary d-inline">
                                                    <input type="checkbox" class="requisition-items-is-available" id="stock=<?=intval($key)?>" data-id="<?=intval($key)?>">
                                                    <label for="stock=<?=intval($key)?>">
                                                    </label>
                                                </div>
                                                <?=$this->Form->control('requisition_items.'.($key).'.is_available',[
                                                    'type' => 'hidden',
                                                    'value' => 0
                                                ])?>
                                            </td>
                                            <td style="text-align: center;"><?=$this->Form->control('requisition_items.'.($key).'.remarks',[
                                                    'label' => false,
                                                    'class' => 'form-control rounded-0',
                                                    'options' => [
                                                        ['value' => strtoupper('Complete'), 'text' => strtoupper('Complete')],
                                                        ['value' => strtoupper('Incomplete'), 'text' => strtoupper('Incomplete')]
                                                    ]
                                                ])?></td>
                                        </tr>
                                    <?php endforeach;;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->control('request_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $request->id])?>
            <?=$this->Form->control('order_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $order->id])?>
            <?=$this->Form->control('inspection_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $inspection->id])?>
            <button type="reset" class="btn btn-danger rounded-0">Reset</button>
            <button type="submit" class="btn btn-primary rounded-0">Save</button>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.replace(mainurl+'requisitions/index');
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('.requisition-items-is-available').change(function (e) {
            var dataId = $(this).attr('data-id');
            var prop = $(this).prop('checked');
            $('#requisition-items-'+(dataId)+'-is-available').val(Number(prop));
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

