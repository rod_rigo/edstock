<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="col-md-12">
    <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
    <div class="card">
        <div class="card-header">
            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" class="btn btn-primary rounded-0 float-right">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('username',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('username'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('fullname',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('fullname'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?=$this->Form->control('bdate',[
                        'class' => 'form-control rounded-0',
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?=$this->Form->control('tin_no',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('TIN No'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?=$this->Form->control('plantilla_no',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('plantilla No'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3">
                    <?=$this->Form->control('position',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('position'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-5 col-lg-5">
                    <?=$this->Form->control('role',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('Select Role'),
                        'options' => [
                            ['text' => ucwords('Super Administrator'), 'value' => ucwords('Super Administrator')],
                            ['text' => ucwords('Administrator'), 'value' => ucwords('Administrator')],
                            ['text' => ucwords('User'), 'value' => ucwords('User')]
                        ]
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?=$this->Form->control('department_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('select Select department'),
                        'options' => $department
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('contact',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('contact'),
                    ]);?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-end">
                    <div class="icheck-primary d-inline">
                        <input type="checkbox" id="active" <?=($user->status)? 'checked': null;?>>
                        <label for="active">
                            Active
                        </label>
                    </div>
                    <?=$this->Form->control('status',[
                        'type' => 'hidden',
                        'required' => true,
                    ]);?>
                </div>

            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->button('Reset',[
                'class' => 'btn btn-danger rounded-0 m-2',
                'type' => 'reset'
            ])?>
            <?=$this->Form->button('Submit',[
                'class' => 'btn btn-success rounded-0 m-2',
                'type' => 'submit'
            ])?>
        </div>
    </div>
    <?=$this->Form->end()?>
</div>

<div class="col-md-12">
    <?=$this->Form->create($user,['id' => 'password-form', 'type' => 'file'])?>
    <div class="card">
        <div class="card-header">

        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?=$this->Form->control('password',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('password'),
                        'value' => '',
                        'required' => true
                    ]);?>
                </div>

            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->button('Submit',[
                'class' => 'btn btn-success rounded-0 m-2',
                'type' => 'submit'
            ])?>
        </div>
    </div>
    <?=$this->Form->end()?>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('#password-form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                window.location.reload();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('#active').change(function (e) {
            var prop = $(this).prop('checked');
            $('#status').val(Number(prop));
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

