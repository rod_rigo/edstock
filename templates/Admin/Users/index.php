
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">User List</h3>
                <div class="card-tools">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'add'])?>" class="btn btn-primary rounded-0" data-toggle="tooltip" data-placement="bottom" title="Add Unit">
                        <i class=""></i> New User
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Full Name</th>
                                        <th>Birthdate</th>
                                        <th>TIN No.</th>
                                        <th>Plantilla No.</th>
                                        <th>Position</th>
                                        <th>Role</th>
                                        <th>Contact No.</th>
                                        <th>Department</th>
                                        <th>Created</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            'use strict';

            var baseurl = mainurl+'users/';
            var url = '';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                pagingType: 'full_numbers',
                lengthMenu:[ 100, 200, 300, 400, 500],
                ajax:{
                    url:baseurl+'getUsers',
                    method: 'GET',
                    dataType: 'JSON'
                },
                columnDefs: [
                    {
                        targets: 0,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: 10,
                        data: null,
                        render: function(data, type, row, meta){
                            return moment(row.created).format('Y-M-D h:m A');
                        }
                    },
                    {
                        targets: 11,
                        data: null,
                        render: function(data, type, row, meta){
                            return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</a> | '+
                                '<a data-id="'+row.id+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a>';
                        }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'username'},
                    { data: 'fullname'},
                    { data: 'bdate'},
                    { data: 'tin_no'},
                    { data: 'plantilla_no'},
                    { data: 'position'},
                    { data: 'role'},
                    { data: 'contact'},
                    { data: 'department.name'},
                    { data: 'created'},
                    { data: 'id'},
                ]
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = 'edit/'+dataId;
               window.location.replace(baseurl+href);
            });

            datatable.on('click','.delete',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = baseurl+'delete/'+dataId;
                Swal.fire({
                    title: 'Delete File?',
                    text: 'Are You Sure',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $.ajax({
                            url:href,
                            type: 'DELETE',
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                            },
                            dataType:'JSON',
                            beforeSend:function () {
                                Swal.fire({
                                    icon: 'info',
                                    title: '',
                                    text: 'Please Wait',
                                    allowOutsideClick: false,
                                    showConfirmButton: false,
                                    timerProgressBar: false,
                                    didOpen: function () {
                                        Swal.showLoading();
                                    }
                                });
                            }
                        }).done(function (data, status, xhr) {
                            swal('success', data.result, data.message);
                            table.ajax.reload(null, false);
                        }).fail(function (xhr, status, error) {
                            const response = JSON.parse(xhr.responseText);
                            swal('info', response.result, response.message);
                        });
                    }
                });
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }

        });
    </script>