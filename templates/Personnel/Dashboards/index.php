<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $units
 */
?>

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
            <?=$this->Form->label('interval_request', ucwords('Interval (Request)'))?>
            <?=$this->Form->select('interval_request',[
                ['value' => strtolower('day'), 'text' => strtoupper('day')],
                ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                ['value' => strtolower('month'), 'text' => strtoupper('month')],
                ['value' => strtolower('week'), 'text' => strtoupper('week')],
                ['value' => strtolower('year'), 'text' => strtoupper('year')],
            ],[
                'class' => 'form-control rounded-0',
                'id' => 'interval-request',
            ])?>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3 id="inventory-request-pending">0</h3>
                    <p>Inventory Request (Pending)</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file"></i>
                </div>
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'RequestItems', 'action' => 'pending'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3 id="inventory-request-approved">0</h3>
                    <p>Inventory Request (Approved)</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file"></i>
                </div>
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'RequestItems', 'action' => 'approved'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3 id="inventory-request-declined">0</h3>
                    <p>Inventory Request (Declined)</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file"></i>
                </div>
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'RequestItems', 'action' => 'declined'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3 id="inventory-request-released">0</h3>
                    <p>Inventory Request (Released)</p>
                </div>
                <div class="icon">
                    <i class="fa fa-file"></i>
                </div>
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'RequestItems', 'action' => 'released'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items List</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Unit</th>
                                <th>Stocks</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'dashboards/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:mainurl+'items/getItems',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'description'},
                { data: 'category.name'},
                { data: 'sub_category.name'},
                { data: 'unit.name'},
                { data: 'stock'},
            ]
        });

        $('#interval-request').change(function (e) {
            var value = $(this).val();
            inventoryrequest(value);
        });

        function inventoryrequest(interval) {
            $.ajax({
                url : baseurl+'inventoryrequest/'+(interval),
                type : 'GET',
                method : 'GET',
                dataType:'JSON',
                beforeSend:function(e){
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                },
            }).done(function(data, status, xhr){
                $('#inventory-request-pending').text(parseInt(data.requests.pending));
                $('#inventory-request-approved').text(parseInt(data.requests.approved));
                $('#inventory-request-declined').text(parseInt(data.requests.declined));
                $('#inventory-request-released').text(parseInt(data.requests.released));
                Swal.close();
            }).fail(function(data, status, xhr){
                Swal.close();
            });
        }

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

        inventoryrequest('day');

    });
</script>





