<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<script>
    const id = parseInt(<?=intval($requestItemSeries->id)?>);
</script>

<?php if(!boolval($requestItemSeries->is_expendable)):?>
    <?php
        $collection = (new \Cake\Collection\Collection($requestItemSeries->request_item_details));

    ?>
<?php endif;?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'Request-Items', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($requestItemSeries,['id'=>'form', 'type'=> 'file'])?>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('requested_by', ucwords('Requested By'))?>
                            <?=$this->Form->text('requested_by',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('Requested By'),
                                'value' => $requestItemSeries->request_item->user->fullname,
                                'readonly' => true,
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'readonly' => true,
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'readonly' => true,
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('request_item.purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose'),
                                'readonly' => true,
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Unit</th>
                                    <th>Item Quantity</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($requestItemSeries->request_item_details as $key => $request_item_detail):?>
                                    <?=$this->Form->hidden('request_item_details.'.(intval($key)).'.id',[
                                        'id' => 'request-item-details-'.(intval($key)).'-id',
                                        'required' => true,
                                        'value' => intval($request_item_detail->id)
                                    ])?>
                                    <?=$this->Form->hidden('request_item_details.'.(intval($key)).'.is_approved',[
                                        'id' => 'request-item-details-'.(intval($key)).'-is-approved',
                                        'required' => true,
                                        'value' => intval($request_item_detail->is_approved),
                                        'class' => 'is-approved'
                                    ])?>
                                    <tr>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->description)?>
                                        </td>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->unit->name)?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->item->stock))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->cost))?>
                                        </td>
                                        <td>
                                            <?=number_format(intval($request_item_detail->qty))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->total))?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">

        </div>
        <?= $this->Form->end()?>
    </div>
</div>


