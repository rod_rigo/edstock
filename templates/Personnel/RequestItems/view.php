<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<script>
    const id = parseInt(<?=intval($requestItem->id)?>);
</script>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Personnel', 'controller' => 'Request-Items', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($requestItem,['id'=>'form', 'type'=> 'file'])?>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('requested_by', ucwords('Requested By'))?>
                            <?=$this->Form->text('requested_by',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('Requested By'),
                                'value' => $requestItem->user->fullname,
                                'readonly' => true,
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'empty' => ucwords('select office')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'empty' => ucwords('select department')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose')
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Unit</th>
                                    <th>Item Quantity</th>
                                    <th>Cost</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($requestItem->request_item_details as $key => $request_item_detail):?>
                                    <tr>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->description)?>
                                        </td>
                                        <td>
                                            <?=strtoupper($request_item_detail->item->unit->name)?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->item->stock))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->cost))?>
                                        </td>
                                        <td>
                                            <?=number_format(intval($request_item_detail->qty))?>
                                        </td>
                                        <td>
                                            <?=number_format(doubleval($request_item_detail->total))?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?php if(in_array(intval($requestItem->status),[intval(0)])):?>
                <button type="submit" class="btn btn-primary rounded-0">Save</button>
            <?php endif;?>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'request-items/';

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+'edit/'+(id),
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.reload();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
