<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<!-- <div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-info">
        <div class="inner">
            <h3 id="total-users">0</h3>
            <p>Total Users</p>
        </div>
        <div class="icon">
            <i class="fa fa-users"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-primary">
        <div class="inner">
            <h3 id="total-heads">0</h3>
            <p>Total Heads</p>
        </div>
        <div class="icon">
            <i class="fa fa-user-circle"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3 id="total-suppliers">0</h3>
            <p>Total Suppliers</p>
        </div>
        <div class="icon">
            <i class="fa fa-address-card"></i>
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-3 col-lg-3">
    <div class="small-box bg-danger">
        <div class="inner">
            <h3 id="total-departments">0</h3>
            <p>Total Departments</p>
        </div>
        <div class="icon">
            <i class="fa fa-building"></i>
        </div>
    </div>
</div> -->

<div class="col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-7">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
                    <?=$this->Form->label('interval', ucwords('Interval (Procurement)'))?>
                    <?=$this->Form->select('interval',[
                        ['value' => strtolower('day'), 'text' => strtoupper('day')],
                        ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                        ['value' => strtolower('month'), 'text' => strtoupper('month')],
                        ['value' => strtolower('week'), 'text' => strtoupper('week')],
                        ['value' => strtolower('year'), 'text' => strtoupper('year')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'interval',
                    ])?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3 id="request-procurement-pending">0</h3>
                            <p>Request Procurement (Pending)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Requests', 'action' => 'pending'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3 id="request-procurement-approved">0</h3>
                            <p>Request Procurement (Approved)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Requests', 'action' => 'approved'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3 id="request-procurement-declined">0</h3>
                            <p>Request Procurement (Declined)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Requests', 'action' => 'declined'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5 col-md-5 col-lg-5">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 mb-2">
                    <?=$this->Form->label('interval_request', ucwords('Interval (Request)'))?>
                    <?=$this->Form->select('interval_request',[
                        ['value' => strtolower('day'), 'text' => strtoupper('day')],
                        ['value' => strtolower('quarter'), 'text' => strtoupper('quarter')],
                        ['value' => strtolower('month'), 'text' => strtoupper('month')],
                        ['value' => strtolower('week'), 'text' => strtoupper('week')],
                        ['value' => strtolower('year'), 'text' => strtoupper('year')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'interval-request',
                    ])?>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3 id="inventory-request-pending">0</h3>
                            <p>Inventory Request (Pending)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'RequestItems', 'action' => 'pending'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3 id="inventory-request-approved">0</h3>
                            <p>Inventory Request (Approved)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'RequestItems', 'action' => 'approved'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3 id="inventory-request-declined">0</h3>
                            <p>Inventory Request (Declined)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'RequestItems', 'action' => 'declined'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3 id="inventory-request-released">0</h3>
                            <p>Inventory Request (For Release)</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-file"></i>
                        </div>
                        <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'RequestItems', 'action' => 'released'])?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-header border-transparent">
            <h3 class="card-title">Procurements (<?=date('F')?>)</h3>
            <a href="javascript:void(0)" class="btn btn-sm btn-info float-right rounded-0" id="procurements-update">Fetch Update</a>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                    <tr>
                        <th>Process</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="procurements-month">

                    </tbody>
                </table>
            </div>

        </div>

        <div class="card-footer clearfix">

        </div>

    </div>
</div>

<div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card">
        <div class="card-header border-transparent">
            <h3 class="card-title">Procurements (<?=date('Y')?>)</h3>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                    <tr>
                        <th>Process</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="procurements-year">

                    </tbody>
                </table>
            </div>

        </div>

        <div class="card-footer clearfix">

        </div>

    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items List</h3>
            <div class="card-tools">
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Unit</th>
                                <th>Price</th>
                                <th>Stocks</th>
                                <th>Code</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    'use strict';
    $(document).ready(function (e) {

        const autocolors = window['chartjs-plugin-autocolors'];
        Chart.register(autocolors);

        var baseurl = mainurl+'dashboards/';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:mainurl+'items/getItems',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'description'},
                { data: 'category.name'},
                { data: 'sub_category.name'},
                { data: 'unit.name'},
                { data: 'price'},
                { data: 'stock'},
                { data: 'code'},
            ]
        });

        function users() {
            $.ajax({
                url:baseurl+'users',
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data, status, xhr) {
                $('#total-users').text(parseInt(data.users));
                $('#total-heads').text(parseInt(data.heads));
                $('#total-suppliers').text(parseInt(data.suppliers));
                $('#total-departments').text(parseInt(data.departments));
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        }

        function procurements() {
            $.ajax({
                url:baseurl+'procurements',
                type: 'GET',
                method: 'GET',
                dataType:'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('#procurements-month, #procurements-year').empty();
                }
            }).done(function (data, status, xhr) {
                $.map(data.month, function (data, key) {
                    $('#procurements-month').append('<tr> <th>'+(data.label)+'</th> <td>'+(parseInt(data.data))+'</td></tr>');
                });
                $.map(data.year, function (data, key) {
                    $('#procurements-year').append('<tr> <th>'+(data.label)+'</th> <td>'+(parseInt(data.data))+'</td></tr>');
                });
                Swal.close();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
            });
        }

        function request(interval) {
              $.ajax({
                  url : baseurl+'request/'+(interval),
                  type : 'GET',
                  method : 'GET',
                  dataType:'JSON',
                  beforeSend:function(e){
                      Swal.fire({
                          icon: null,
                          title: null,
                          text: null,
                          allowOutsideClick: false,
                          showConfirmButton: false,
                          timerProgressBar: false,
                          didOpen: function () {
                              Swal.showLoading();
                          }
                      });
                  },
              }).done(function(data, status, xhr){
                  $('#request-procurement-pending').text(parseInt(data.requests.pending));
                  $('#request-procurement-approved').text(parseInt(data.requests.approved));
                  $('#request-procurement-declined').text(parseInt(data.requests.declined));
                  Swal.close();
              }).fail(function(data, status, xhr){
                  Swal.close();
              });
        }

        function inventoryrequest(interval) {
            $.ajax({
                url : baseurl+'inventoryrequest/'+(interval),
                type : 'GET',
                method : 'GET',
                dataType:'JSON',
                beforeSend:function(e){
                    Swal.fire({
                        icon: null,
                        title: null,
                        text: null,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                },
            }).done(function(data, status, xhr){
                $('#inventory-request-pending').text(parseInt(data.requests.pending));
                $('#inventory-request-approved').text(parseInt(data.requests.approved));
                $('#inventory-request-declined').text(parseInt(data.requests.declined));
                $('#inventory-request-released').text(parseInt(data.requests.released));
                Swal.close();
            }).fail(function(data, status, xhr){
                Swal.close();
            });
        }

        $('#procurements-update').click(function (e) {
            procurements();
        });

        $('#interval').change(function (e) {
           var value = $(this).val();
            request(value);
        });

        $('#interval-request').change(function (e) {
            var value = $(this).val();
            inventoryrequest(value);
        });

        users();
        request($('#interval').val());
        inventoryrequest($('#interval-request').val());

        setInterval(function () {
            users();
            request($('#interval').val());
            inventoryrequest($('#interval-request').val());
        },30000);

    });
</script>

