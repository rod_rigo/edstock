<?php
class xtcpdf extends TCPDF {
    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 10);
        $this->Cell(0, 0, 'Appendix 62', 0, 0, 'R');
        $this->Ln(6);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'INSPECTION AND ACCEPTANCE REPORT', 0, 0, 'C');
        $this->Ln(6);
    }
}

$check = WWW_ROOT. 'img'. DS. 'check.png';
$file_info = new finfo(FILEINFO_MIME_TYPE);
$mime_type = $file_info->buffer(file_get_contents($check));
$image = 'data:'.(strtolower($mime_type)).';base64,'.(base64_encode(file_get_contents($check)));

$uncheck = WWW_ROOT. 'img'. DS. 'uncheck.png';
$uncheckinfo = new finfo(FILEINFO_MIME_TYPE);
$uncheckmime_type = $uncheckinfo->buffer(file_get_contents($uncheck));
$uncheckimage = 'data:'.(strtolower($uncheckmime_type)).';base64,'.(base64_encode(file_get_contents($uncheck)));

$pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->SetMargins(10, 35, 30, true);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('times','',12);
$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$data = '';
$ctr = 0;
$total = 0;
foreach ($inspection->inspection_items as $details){
    $data .='<tr>
                <td align="center">'.($ctr+1).'</td>
                <td align="center">'.$details->item->description.'</td>
                <td align="center">'.($details->item->unit->name).'</td>
                <td align="center">'.number_format($details->quantity).'</td>
                </tr>';
    $ctr++;
    $total +=$details->total;
}

$html = '<table width="100%">
    <tr>
        <td width="50%"><p>Entity Name: <b>'.$inspection->office->name.'</b></p></td>
        <td width="1%"></td>
        <td width="45%"><p>Fund Cluster: <b>'.$inspection->fund_cluster->name.'</b></p></td>
    </tr>
</table>
<table width="100%" cellpadding="2" style="border-collapse: collapse; border: 1px solid black;">
    <tr>
        <td width="70%" style="border-left: 1px solid black;">Supplier: <b><u>'.$inspection->order->supplier->name.'</u></b></td>
        <td width="30%" style="border-left: 1px solid black;">IAR No.: <b><u>'.$inspection->iar_no.'</u></b></td>
    </tr>
    <tr>
        <td style="border-left: 1px solid black;">PO No./Date: <b><u>'.$inspection->order->po_no.' /______________</u></b></td>
        <td width="40%" style="border-left: 1px solid black;">Date: ___________</td>
    </tr>
    <tr>
        <td width="70%" style="border-left: 1px solid black;">Requisitioning Office/Dept.: _______________________________</td>
        <td width="40%" style="border-left: 1px solid black;">Invoice No.: ___________</td>
    </tr>
    <tr>
        <td width="70%" style="border-left: 1px solid black;">Responsibility Center Code: _______________________________</td>
        <td width="40%" style="border-left: 1px solid black;">Date: ___________</td>
    </tr>
</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
        <th width="15%" align="center">Stock/ Property No.</th>
        <th width="55%" align="center">Item Description</th>
        <th width="15%" align="center">Unit</th>
        <th width="15%" align="center">Quantity</th>
    </tr>
    '.$data.'
    <tr>
        <td colspan="6" align="center">Nothing Follows</td>
    </tr>

</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
<tr>
    <td width="60%" align="center"><b><i>INSPECTION</i></b></td>
    <td width="40%" align="center"><b><i>ACCEPTANCE</i></b></td>
</tr>
</table>
<table cellpadding="5"  width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;" >
<tr>
    <td width="60%" style="border-left: 1px solid black;">Date Inspected:</td>
    <td width="40%" style="border-left: 1px solid black;">Date Received:</td>
 </tr>
 <tr>
    <td width="5%"></td>
    <td width="5%"></td>
        <td width="5%"></td>
    <td width="5%"><img src="'.( (boolval($inspection->is_inspected))? $check: $uncheck ).'" height="40" width="40" alt=""></td>
    <td width="40%" style="border-right: 1px solid black;" align="right">
    Inspected, verified and found in order as to quantity and specifications</td>
        <td width="5%"></td>
      <td width="5%"><img src="'.( (boolval($inspection->is_compete))? $check: $uncheck ).'" height="40" width="40" alt=""></td>
    <td width="21%" align="right">Completed</td>
 </tr>
 <tr>
    <td width="60%" style="border-right: 1px solid black;"></td>
     <td width="5%"></td>
     <td width="5%"><img src="'.( $uncheck ).'" height="40" width="40" alt=""></td>
    <td width="25%" align="right">Partial (pls. specify quantity)</td>
 </tr>
  <tr>
    <td width="60%" style="border-left: 1px solid black;"></td>
    <td width="35%" style="border-left: 1px solid black;" align="center">______________</td>
 </tr>
 <tr>
    <td width="60%" align="center" style="border-left: 1px solid black;"><b><u>JULIE ANN T. GALLIEN</u></b></td>
    <td width="40%" style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td align="center" style="border-left: 1px solid black;">Inspection Officer / Inspection Committee</td>
    <td style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td></td>
    <td width="40%" style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td width="60%" align="center" style="border-left: 1px solid black;"><b><u>IMEE MARIE M. PILLIEN</u></b></td>
    <td width="40%" style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td align="center" style="border-left: 1px solid black;">Inspection Officer / Inspection Committee</td>
    <td style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td></td>
    <td width="40%" style="border-left: 1px solid black;"></td>
 </tr>
 <tr>
    <td width="60%" align="center" style="border-left: 1px solid black;"><b><u>MINERMA P. RIVERA</u></b></td>
    <td width="40%" align="center" style="border-left: 1px solid black;"><b><u>JAMES PAUL R. SANTIAGO</u></b></td>
 </tr>
 <tr>
    <td align="center" style="border-left: 1px solid black;">Inspection Officer / Inspection Committee</td>
    <td align="center" style="border-left: 1px solid black;">Administrative Officer IV</td>
 </tr>

</table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('IAR.pdf', 'I');