<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $units
 */
?>

<div class="col-md-12">
    <?=$this->Form->create($item,['id' => 'form', 'type' => 'file'])?>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Update Item</h3>
            <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Items', 'action' => 'index'])?>" class="btn btn-primary rounded-0 float-right">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?=$this->Form->control('item_code',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('item code'),
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?=$this->Form->control('description',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('description'),
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('category_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('select categories'),
                        'options' => $categories
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('unit_id',[
                        'class' => 'form-control rounded-0',
                        'empty' => ucwords('select units'),
                        'options' => $units
                    ]);?>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <?=$this->Form->control('price',[
                        'class' => 'form-control rounded-0',
                        'placeholder' => ucwords('price'),
                    ]);?>
                </div>

            </div>
        </div>
        <div class="card-footer d-flex justify-content-end align-items-center">
            <?=$this->Form->button('Reset',[
                'class' => 'btn btn-danger rounded-0 m-2',
                'type' => 'reset'
            ])?>
            <?=$this->Form->button('Submit',[
                'class' => 'btn btn-success rounded-0 m-2',
                'type' => 'submit'
            ])?>
        </div>
    </div>
    <?=$this->Form->end()?>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('input[type="number"]').keypress(function (e) {
            const regex = /^([0-9./]){1,}$/;
            if(!(e.key).match(regex)){
                e.preventDefault();
            }
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
