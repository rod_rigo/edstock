<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $units
 */
?>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-header">Item Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('qty',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('quantity'),
                            'label' => ucwords('quantity'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3">
                        <?=$this->Form->control('price',[
                            'class' => 'form-control rounded-0',
                            'placeholder' => ucwords('price'),
                            'label' => ucwords('price'),
                        ]);?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->control('description',[
                            'class' => 'form-control rounded-0',
                            'empty' => ucwords('description'),
                            'options' => [
                                ['value' => strtoupper('in'), 'text' => strtoupper('in')],
                                ['value' => strtoupper('out'), 'text' => strtoupper('out')],
                            ]
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$this->Form->control('item_id',[
                    'class' => 'form-control rounded-0',
                    'type' => 'hidden',
                ]);?>
                <?=$this->Form->button('Reset',[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset'
                ])?>
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <?=$this->Form->button('Submit',[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit'
                ])?>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Items List</h3>
            <div class="card-tools">
                <a class="btn btn-primary rounded-0" href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Items', 'action' => 'add'])?>">
                    <i class=""></i> New Item
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item Code</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Unit</th>
                                <th>Price</th>
                                <th>Stocks</th>
                                <th>Code</th>
                                <th>Created</th>
                                <th>
                                    Options
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'items/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order:[ [8, 'desc'] ],
            ajax:{
                url:baseurl+'getItems',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 9,
                    data: null,
                    render: function(data,type,row){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 10,
                    data: null,
                    render: function(data, type, row, meta){
                        return '<a data-id="'+(row.id)+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</i></a> | '+
                            '<a data-id="'+(row.id)+'" class="btn btn-danger btn-sm rounded-0 delete">Delete</i></a> | '+
                            '<a data-id="'+(row.id)+'" class="btn btn-info btn-sm rounded-0 stocks">Manage Stocks</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'item_code'},
                { data: 'description'},
                { data: 'category.name'},
                { data: 'sub_category.name'},
                { data: 'unit.name'},
                { data: 'price'},
                { data: 'stock'},
                { data: 'code'},
                { data: 'created'},
                { data: 'id'}
            ]
        });

        $('#modal-toggle').click(function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#modal').modal('toggle');
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            url = 'add';
            $('#form')[0].reset();
            $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            $('button[type="reset"]').fadeIn(100);
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = mainurl+'stocks/'+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#modal').modal('toggle');
                $('#form')[0].reset();
                table.ajax.reload(null, false);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.stocks',function (e) {
            var dataId = $(this).attr('data-id');
            url = 'add/'+(dataId);
            $('#form')[0].reset();
            $('#item-id').val(parseInt(dataId));
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            $('button[type="reset"]').fadeIn(100);
            $('#modal').modal('toggle');
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        $('input[type="number"]').keypress(function (e) {
            const regex = /^([0-9./]){1,}$/;
            if(!(e.key).match(regex)){
                e.preventDefault();
            }
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

