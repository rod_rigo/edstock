<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<script>
    const requestId = '<?=intval($request->id)?>';
</script>

<?php $ctr = 0; ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Orders', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
                <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Requests', 'action' => 'edit', intval($request->id)])?>" class="btn btn-info rounded-0">
                     View Requests
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($order,['id'=>'form', 'type' => 'file'])?>
            <div class="row mb-3">
                <div class="col-sm-12 col-md-5 col-lg-5">
                    <?=$this->Form->control('created_by',[
                        'class' => 'form-control rounded-0',
                        'readonly' => true,
                        'value' => $request->user->fullname
                    ])?>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7">
                    <?=$this->Form->control('purpose',[
                        'class' => 'form-control rounded-0',
                        'readonly' => true,
                        'value' => $request->purpose
                    ])?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('supplier_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $suppliers,
                                'empty' => ucwords('select supplier')
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('method_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $methods,
                                'empty' => ucwords('select method')
                            ])?>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('place_of_delivery',[
                                'class' => 'form-control rounded-0',
                                'options' => $placeOfDeliveries,
                                'label' => ucwords('place of delivery'),
                                'empty' => ucwords('Select Place Of delivery'),
                                'required' => true,
                            ])?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                    <thead>
                                    <tr>
                                        <th width="15%">Property No</th>
                                        <th width="10%">Unit</th>
                                        <th width="20%">Description</th>
                                        <th width="25%">Quantity</th>
                                        <th width="15%">Unit Cost</th>
                                        <th width="15%">Total Cost</th>
                                    </tr>
                                    </thead>
                                    <tbody id="items">
                                    <?php $i = 1;?>
                                        <?php foreach ($request->requestdetails as $key => $requestdetail):?>
                                            <tr>
                                                <td><?=intval($i++)?></td>
                                                <td><?=strtoupper($requestdetail->item->unit->name)?></td>
                                                <td><?=strtoupper($requestdetail->item->description)?></td>
                                                <td><?=strtoupper($requestdetail->qty)?></td>
                                                <td><?=strtoupper(number_format($requestdetail->cost))?></td>
                                                <td><?=strtoupper(number_format($requestdetail->total))?></td>
                                            </tr>
                                        <?php endforeach;;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->control('request_id',['type' => 'hidden', 'readonly' => true, 'required' => true, 'value' => $request->id])?>
            <button type="reset" class="btn btn-danger rounded-0">Reset</button>
            <button type="submit" class="btn btn-primary rounded-0">Save</button>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'orders/';
        var url = 'add/'+requestId;

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            const action = baseurl+url;
            $.ajax({
                url: action,
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.replace(data.redirect);
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>