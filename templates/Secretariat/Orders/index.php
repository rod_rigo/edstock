<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Purchase Orders List</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Series No.</th>
                                <th>Created By</th>
                                <th>Method</th>
                                <th>Supplier</th>
                                <th>Place Of Delivery</th>
                                <th>Created</th>
                                <th>
                                    Options
                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'orders/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order:[ [6, 'desc'] ],
            ajax:{
                url:baseurl+'getOrders',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data,type,row){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data, type, row, meta){
                        return '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a> | ' + 
                        '<a data-id="'+row.id+'" class="btn btn-info btn-sm rounded-0 pdf">PDF</a>';
                    }
                }
            ],
            columns: [
                { data: 'id'},
                { data: 'request.series_number'},
                { data: 'request.user.fullname'},
                { data: 'method.name'},
                { data: 'supplier.name'},
                { data: 'place_of_delivery'},
                { data: 'created'},
                { data: 'id'},
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'print-order/'+dataId;
            window.open(baseurl+href);
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>