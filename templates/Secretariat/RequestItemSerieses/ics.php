<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\RequestItemSeriese> $requestItemSerieses
 */
?>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Requests Series (ICS)</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped" style="font-size: small;">
                <thead>
                <tr>
                    <th>Series No.</th>
                    <th>Type</th>
                    <th>Request By</th>
                    <th>Office</th>
                    <th>Purpose</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Option</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'request-item-serieses/';
        var url = '';
        var status = ['PENDING', 'APPROVED', 'DECLINED', 'RELEASED'];
        var types = ['ICS', 'RIS'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order:[ [6, 'desc'] ],
            ajax:{
                url:mainurl+'request-item-details/getRequestItemDetailsIcs',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 1,
                    data: null,
                    render: function(data,type,row,meta){
                        return (types[parseInt(row.request_item_seriese.is_expendable)])+' - '+(row.ics_type);
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data,type,row,meta){
                        return status[parseInt(row.request_item.status)];
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data, type, row, meta){
                        return  '<a data-id="'+row.request_item_seriese.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a> | '+
                            '<a data-id="'+row.request_item_seriese.id+'" class="btn btn-info btn-sm rounded-0 pdf">PDF</a>';
                    }
                }
            ],
            columns: [
                { data: 'request_item_seriese.series_number'},
                { data: 'request_item_seriese.is_expendable'},
                { data: 'request_item.user.fullname'},
                { data: 'request_item.office.name'},
                { data: 'request_item.purpose'},
                { data: 'request_item.status'},
                { data: 'request_item_seriese.created'},
                { data: 'request_item_seriese.id'}
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'view/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'print-custodian/'+dataId;
            Swal.fire({
                title: 'Export File?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    window.open(href);
                }
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>
