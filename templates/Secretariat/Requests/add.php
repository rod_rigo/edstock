<?php
/**
 * @var \App\View\AppView $this
 */
?>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-header"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12" style="height: 30em !important; overflow-y:auto !important;">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>Property No.</th>
                                    <th>Unit</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Unit Cost</th>
                                    <th>
                                        <div class="icheck-primary d-inline">
                                            <input type="checkbox" id="checkbox">
                                            <label for="checkbox">
                                                Check All
                                            </label>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary rounded-0" data-dismiss="modal">Close</button>
                <button type="button" id="select" class="btn btn-success rounded-0" data-dismiss="modal">Select</button>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($request,['id'=>'form'])?>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'empty' => ucwords('select office')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                                'empty' => ucwords('select fund cluster')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'empty' => ucwords('select department')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose')
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 mb-2 d-flex justify-content-end align-items-center">
                            <button type="button" class="btn btn-primary rounded-0" id="modal-toggle">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 mb-2 d-none">
                            <?=$this->Form->control('autocomplete',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('Type Item Name'),
                                'id' => 'autocomplete',
                                'label' => ucwords('Item Name')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                    <thead>
                                    <tr>
                                        <th width="15%">Property No</th>
                                        <th width="10%">Unit</th>
                                        <th width="20%">Description</th>
                                        <th width="25%">Quantity</th>
                                        <th width="15%">Unit Cost</th>
                                        <th width="15%">Total Cost</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody id="items">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->hidden('user_id',[
                'id' => 'user-id',
                'required' => true,
                'value' => $auth['id']
            ])?>
            <?=$this->Form->hidden('series_number',[
                'id' => 'series-number',
                'required' => true,
                'value' => $series
            ])?>
            <button type="submit" class="btn btn-primary rounded-0">Save</button>
        </div>
        <?= $this->Form->end()?>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'requests/';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'frti',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:mainurl+'items/getItemsProcurementPlan',
                method: 'GET',
                dataType: 'JSON'
            },
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 5,
                    orderable: false,
                    data: null,
                    render: function(data, type, row, meta){
                        return '<span class="d-none" id="data-'+(row.item.id)+'">'+(JSON.stringify(row.item))+'</span><div class="icheck-primary d-inline"> ' +
                            '<input type="checkbox" data-id="'+(row.item.id)+'" value="'+(row.stock)+'" id="item-'+(row.item.id)+'"> ' +
                            '<label for="item-'+(row.item.id)+'"> </label> ' +
                            '</div>';
                    }
                }
            ],
            columns: [
                { data: 'item.id'},
                { data: 'item.unit.name'},
                { data: 'item.description'},
                { data: 'stock'},
                { data: 'item.price'},
                { data: 'item.id'}
            ]
        });

        $('#form').submit(function (e) {
            e.preventDefault();
            const data = new FormData(this);
            $.ajax({
                url: baseurl+'add',
                method:'POST',
                type:'POST',
                data: data,
                cache:false,
                contentType: false,
                processData: false,
                dataType:'JSON',
                beforeSend:function () {
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                }
            }).done(function (data,response, status) {
                swal('success', data.result, data.message);
                $('#form')[0].reset();
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
                window.location.reload();
            }).fail(function (xhr, status, error) {
                const response = JSON.parse(xhr.responseText);
                swal('info', response.result, response.message);
                $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            });
        });

        $('#checkbox').change(function (e) {
            var prop = $(this).prop('checked');
            $('input[type="checkbox"]').prop('checked', prop);
        });

        $(document).on('keypress', 'input[type="number"]', function (e) {
            const regex = /^([0-9]){1,}$/;
            if(!(e.key).match(regex)){
                e.preventDefault();
            }
        });

        $(document).on('click', '.remove', function (e) {
            var dataTarget = $(this).attr('data-target');
            $(dataTarget).remove();
            inputs();
        });

        $(document).on('input', '.requestdetails-qty', function () {
            var dataId = $(this).attr('data-id');
            var value = $(this).val();
            var cost = $('#requiestdetails-cost-'+(parseInt(dataId))+'').val();
            var total = parseFloat(value) * parseFloat(cost);
            $('#requiestdetails-total-'+(parseInt(dataId))+'').val(parseFloat(total).toFixed(2));
        });

        $('#modal-toggle').click(function (e) {
            $('#modal').modal('toggle');
        });

        $('#select').click(function (e) {
            $('input[type="checkbox"]:checked').not('#checkbox').each(function () {
                var dataId = $(this).attr('data-id');
                var value = $(this).val();
                var text = JSON.parse($('#data-'+(parseInt(dataId))+'').text());
                trdata(text, parseInt(value));
            });
        });

        $('#modal').on('hidden.bs.modal', function (e) {
            $('input[type="checkbox"]:checked').prop('checked', false);
        });

        function getItemLists() {
            $.ajax({
                url:mainurl+'items/getItemsLists',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function (e) {

                },
            }).done(function (data, status, xhr) {
                $('#autocomplete').autocomplete({
                    delay: 100,
                    minLength: 2,
                    autoFocus: true,
                    source: function (request, response) {
                        response($.map(data, function (value, key) {
                            var name = value.description.toUpperCase();
                            if (name.indexOf(request.term.toUpperCase()) !== -1) {
                                return {
                                    key: value.id,
                                    label: value.description,
                                    unit: value.unit.name,
                                    price: value.price,
                                }
                            } else {
                                return null;
                            }
                        }));
                    },
                    create: function (event, ui) {
                        $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                            var label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
                            return $('<li></li>')
                                .data('item.autocomplete', item)
                                .append('<li>'+(item.label)+'</li>')
                                .appendTo(ul);
                        };
                    },
                    focus: function(e, ui) {
                        e.preventDefault();
                    },
                    select: function(e, ui) {
                        e.preventDefault();
                        console.log(ui.item);
                        $('#row-'+(parseInt(ui.item.key))+'').remove();
                        trdata(ui.item);
                        $('#autocomplete').val(null);
                    },
                    change: function(e, ui ) {
                        e.preventDefault();
                    },
                });
            }).fail(function (data, status, xhr) {
                swal('info', 'Info', 'Error Fetching Items');
            }).always(function (data, status, xhr) {
                Swal.close();
            });
        }

        function trdata(data, stock) {
            if(!$('#row-'+(data.id)+'').length){
                var html = '<tr id="row-'+(data.id)+'"> ' +
                    '<td class="property-no"></td> ' +
                    '<td>'+((data.unit.name).toUpperCase())+'</td> ' +
                    '<td>'+((data.description).toUpperCase())+'<input type="hidden" class="requestdetails-item-id" value="'+(data.id)+'" id="requiestdetails-item-id-'+(data.id)+'" readonly required></td> ' +
                    '<td><input type="number" class="form-control requestdetails-qty" value="'+(parseInt(stock))+'" min="1" id="requiestdetails-qty-'+(data.id)+'" step="any" data-id="'+(data.id)+'" required></td> ' +
                    '<td><input type="number" class="requestdetails-cost form-control" value="'+(data.price)+'" id="requiestdetails-cost-'+(data.id)+'" step="any" data-id="'+(data.id)+'" required></td> ' +
                    '<td><input type="number" class="form-control requestdetails-total" value="'+(data.price)+'" id="requiestdetails-total-'+(data.id)+'" step="any" data-id="'+(data.id)+'" readonly required></td> ' +
                    '<td><button class="btn btn-danger remove" data-target="#row-'+(data.id)+'"><i class="fa fa-times"></i></button></td>' +
                    '</tr>';
                $('#items').append(html);
            }
            inputs();
        }

        function inputs() {
            var propertyNo = document.querySelectorAll('.property-no');
            var itemId = document.querySelectorAll('.requestdetails-item-id');
            var qty = document.querySelectorAll('.requestdetails-qty');
            var cost = document.querySelectorAll('.requestdetails-cost');
            var total = document.querySelectorAll('.requestdetails-total');
            $.map(propertyNo, function (value, key) {
                propertyNo[(parseInt(key))].innerText = parseInt(key+1);
                itemId[(parseInt(key))].name = 'requestdetails['+(parseInt(key))+'][item_id]';
                qty[(parseInt(key))].name = 'requestdetails['+(parseInt(key))+'][qty]';
                cost[(parseInt(key))].name = 'requestdetails['+(parseInt(key))+'][cost]';
                total[(parseInt(key))].name = 'requestdetails['+(parseInt(key))+'][total]';
            });
        }

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

//        getItemLists();

    });
</script>
