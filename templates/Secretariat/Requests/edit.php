<?php
/**
 * @var \App\View\AppView $this
 *
 *
 */
?>

<?php $ctr = 0; ?>
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
                <a href="<?=$this->Url->build(['prefix' => 'Secretariat', 'controller' => 'Requests', 'action' => 'index'])?>" class="btn btn-primary rounded-0">
                    <i class="fas fa-arrow-left"></i> Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <?= $this->Form->create($request,['id'=>'requests-form'])?>
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5 mb-3">
                    <?=$this->Form->control('created_by',[
                        'class' => 'form-control rounded-0',
                        'value' => $request->user->fullname,
                        'readonly' => true
                    ])?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('office_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $offices,
                                'empty' => ucwords('select office')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('fund_cluster_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $fundClusters,
                                'empty' => ucwords('select fund cluster')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('department_id',[
                                'class' => 'form-control rounded-0',
                                'options' => $departments,
                                'empty' => ucwords('select departments')
                            ])?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->control('purpose',[
                                'class' => 'form-control rounded-0',
                                'placeholder' => ucwords('purpose')
                            ])?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            Items
                            <div class="card-tools">
                                <a href="" id="edit-search" data-toggle="tooltip" data-placement="bottom" title="Search Items"><i class="fas fa-search"></i></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-bordered" width="100%" id="item-count" style="font-size: small;">
                                <thead>
                                <tr>
                                    <th width="15%">Property No</th>
                                    <th width="10%">Unit</th>
                                    <th width="20%">Description</th>
                                    <th width="25%">Quantity</th>
                                    <th width="15%">Unit Cost</th>
                                    <th width="15%">Total Cost</th>
                                </tr>
                                </thead>
                                <tbody id="items">
                                <?php foreach ($request->requestdetails as $details): ?>
                                    <tr>
                                        <td><?= $ctr + 1 ?></td>
                                        <td><?= $details->item->unit->name ?></td>
                                        <td><?= $details->item->description ?></td>
                                        <td>
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <button type="button" class="btn btn-outline-secondary edit-minus" data-field="requestdetails[<?= $ctr ?>][qty]">
                                                        <span class="fa fa-minus"></span>
                                                    </button>
                                                </span>
                                                <input type="number" name="requestdetails[<?= $ctr ?>][qty]" class="form-control input-number" value="<?= $details->qty ?>" min="0">
                                                <input type="hidden" name="requestdetails[<?= $ctr ?>][item_id]" value="<?= $details->item_id ?>">
                                                <input type="hidden" name="requestdetails[<?= $ctr ?>][id]" value="<?= $details->id ?>">
                                                <span class="input-group-append">
                                                    <button type="button" class="btn btn-outline-secondary edit-plus"  data-field="requestdetails[<?= $ctr ?>][qty]">
                                                        <span class="fa fa-plus"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="number" name="requestdetails[<?= $ctr ?>][cost]" class="form-control cost" value="<?= $details->cost ?>" readonly="readonly"></td>
                                        <td><input type="number" name="requestdetails[<?= $ctr ?>][total]" class="form-control total" value="<?= $details->total ?>" readonly="readonly"></td>

                                    </tr>
                                    <?php
                                    $ctr++;
                                endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <input type="hidden" name="id" id="id" value="<?= $request->id ?>">
        </div>
        <?= $this->Form->end()?>
    </div>
</div>



<script>
    'use strict';
    $(document).ready(function () {


        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    })
</script>