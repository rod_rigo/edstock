<?php
class xtcpdf extends TCPDF {
    public function Header($name = null) {
        $this->setY(10);
        $this->Ln(3);
        $this->SetFont('times', 'I', 8);
        $this->Cell(0, 0, 'Appendix 60', 0, 0, 'R');
        $this->Ln(10);
        $this->SetFont('timesB', 'B', 15);
        $this->Cell(0, 0, 'PURCHASE REQUEST', 0, 0, 'C');
        $this->Ln(6);
    }
}

$pdf = new xtcpdf('P', 'mm', [215.9, 330.2], true, 'UTF-8', false);
$pdf->SetMargins(10, 35, 10, true);
$pdf->SetAutoPageBreak(true, 10);
$pdf->SetFont('times','',12);
$style =['width' => 0.5, 'cap' => 'round', 'join' => 'round', 'dash' => 0];
$pdf->AddPage();

$data = '';
$ctr = 0;
$total = 0;
foreach ($request->requestdetails as $details){
    $data .='<tr>
                <td align="center">'.($ctr+1).'</td>
                <td align="center">'.$details->item->unit->name.'</td>
                <td align="center">'.$details->item->description.'</td>
                <td align="right">'.number_format($details->qty).'</td>
                <td align="right">'.number_format($details->cost,2).'</td>
                <td align="right">'.number_format($details->total,2).'</td>
                </tr>
    ';
    $ctr++;
    $total +=$details->total;
}

$html = '<table width="100%">
    <tr>
        <td width="60%"><p>Entity Name: <b>'.$request->office->name.'</b></p></td>
        <td width="1%"></td>
        <td width="35%"><p>Fund Cluster: <b>'.$request->fund_cluster->name.'</b></p></td>
    </tr>
</table>
<table border="1" width="100%" cellpadding="2">
    <tr>
        <td width="21%">Office/Section: <b>'.$request->department->name.'</b></td>
        <td width="49%">PR No: <b><u>'.$request->series_number.'</u></b></td>
        <td width="30%" align="right">Date: <b><u>'.((new DateTime($request->created))->format('Y-M-d')).'</u></b></td>
    </tr>
    <tr>
        <td></td>
        <td style="font-size: small;">Responsibility Center Code : </td>
        <td></td>
    </tr>
</table>
<table border="1" cellpadding="2" width="100%" style="font-size: 10px;">
    <tr>
        <th width="13%" align="center">Stock/ Property No.</th>
        <th width="8%" align="center">Unit</th>
        <th width="40%" align="center">Item Description</th>
        <th width="9%" align="center">Quantity</th>
        <th width="15%" align="center">Unit Cost</th>
        <th width="15%" align="center">Total Cost</th>
    </tr>
    '.$data.'
    <tr>
        <td align="center"></td>
        <td align="center"></td>
        <td align="center"></td>
        <td align="right"></td>
        <td align="center"><p>TOTAL</p></td>
        <td align="right"><b>'.number_format($total,2).'</b></td>
    </tr>
    <tr>
         <td colspan="6" align="center">Nothing Follows</td>
    </tr>
     <tr>
         <td colspan="6">Purpose:
            <p align="center">'.$request->purpose.'</p>
    </td>
</tr>
    
</table>
<table cellpadding="2" width="100%" style="font-size: 10px; border-collapse: collapse; border: 1px solid black;">
<tr>
        <td width="13%"></td>
        <td width="48%"><p>Requested By: <b></b></p></td>
        <td width="39%"><p>Approved: <b></b></p></td>
    </tr>
    <tr>
        <td width="20%"></td>
    </tr>
<tr>
    <td width="13%">Signature:</td>
    <td width="29%">_______________________________</td>
    <td width="20%"></td>
    <td width="43%">____________________________________</td>
    <td></td>
</tr>
<tr>
    <td width="13%">Printed Name:</td>
    <td width="49%" align="left"><b><u>'.$coordinator->name.'</u></b></td>
    <td width="38%" align="left"><b><u>FLORDELIZA C. GECOBE PHD, CESO V</u></b></td>
</tr>
<tr>
    <td width="13%">Designation:</td>
    <td width="49%" align="left">'.$coordinator->position.'</td>
    <td width="38%" align="left"><p>SCHOOLS DIVISION SUPERINTENDENT</p></td>
</tr>
<tr>
    <td width="13%">Date:</td>
    <td width="49%" align="left">'.((new DateTime($request->created))->format('Y-M-d')).'</td>
    <td width="38%" align="left">'.((new DateTime($request->created))->format('Y-M-d')).'</td>
</tr>
</table>';

$pdf->writeHTML($html, true, false, true, false, 'L');
$pdf->LastPage();
$pdf->Output('PR.pdf', 'I');