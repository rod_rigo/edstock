<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Requests List</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3 mb-2">
                    <?=$this->Form->label('start_date', ucwords('Start date'))?>
                    <?=$this->Form->date('start_date',[
                        'class' => 'form-control rounded-0',
                        'id' => 'start-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 mb-2">
                    <?=$this->Form->label('end_date', ucwords('End date'))?>
                    <?=$this->Form->date('end_date',[
                        'class' => 'form-control rounded-0',
                        'id' => 'end-date',
                        'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end mb-2">
                    <?=$this->Form->button('Search',[
                        'class' => 'btn btn-info rounded-0',
                        'id' => 'search'
                    ])?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 mb-2">
                    <?=$this->Form->label('status', ucwords('status'))?>
                    <?=$this->Form->select('status',[
                        ['text' => strtoupper('pending'), 'value' => strtoupper('pending')],
                        ['text' => strtoupper('approved'), 'value' => strtoupper('approved')],
                        ['text' => strtoupper('declined'), 'value' => strtoupper('declined')],
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'status',
                        'empty' => strtoupper('all')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <table id="datatable" class="table table-bordered table-striped" style="font-size: small;">
                        <thead>
                        <tr>
                            <th>Series No.</th>
                            <th>Process By</th>
                            <th>Office.</th>
                            <th>Purpose</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Option</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'requests/';
        var url = '';
        var status = ['PENDING', 'APPROVED', 'DECLINED'];

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lBfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            order: [ [5, 'desc'] ],
            ajax:{
                url:baseurl+'getReports',
                method: 'GET',
                dataType: 'JSON'
            },
            buttons: [
                {
                    extend: 'print',
                    title: 'Print',
                    text: 'Print',
                    className:'btn btn-secondary rounded-0',
                    attr:  {
                        id: 'print'
                    },
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5,]
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10px' )
                            .prepend('');
                        $(win.document.body).find( 'table tbody' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' ).css({'background':'transparent'});

                    },
                    footer:true,
                    messageBottom:function () {
                        return 'Exported By '+ (username);
                    }
                },
                {
                    extend: 'pdfHtml5',
                    attr:  {
                        id: 'pdf'
                    },
                    text: 'PDF',
                    title: 'PDF Reports',
                    tag: 'button',
                    className:'btn btn-danger rounded-0',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5,]
                    },
                    action: function(e, dt, node, config) {
                        Swal.fire({
                            title:'PDF',
                            text:'Export To PDF?',
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                setTimeout(function(){
                                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                }, 1000);
                            }
                        });
                    },
                    customize: function(doc) {
                        doc.pageMargins = [2, 2, 2, 2 ];
                        doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.styles.tableHeader.fontSize = 10;
                        doc.styles.tableBodyEven.fontSize = 10;
                        doc.styles.tableBodyOdd.fontSize = 10;
                        doc.styles.tableFooter.fontSize = 10;

                    },
                    download: 'open',
                    footer:true,
                    messageBottom:function () {
                        return 'Exported By '+ (username);
                    }
                },
            ],
            columnDefs: [
                {
                    targets: 4,
                    data: null,
                    render: function(data,type,row,meta){
                        return status[parseInt(row.status)];
                    }
                },
                {
                    targets: 5,
                    data: null,
                    render: function(data,type,row,meta){
                        return moment(row.created).format('MM-DD-YYYY hh:mm A');
                    }
                },
                {
                    targets: 6,
                    data: null,
                    render: function(data, type, row, meta){
                        var status = (row.status == 0)? ' | <a data-id="'+(row.id)+'" class="btn btn-success btn-sm rounded-0 status" status="1">Approve</i></a> | ' +
                            '<a data-id="'+(row.id)+'" class="btn btn-warning btn-sm rounded-0 status" status="2">Decline</i></a>': '';
                        var approved = (row.status == 1 && !(row.orders).length)? '| <a data-id="'+(row.id)+'" class="btn btn-success btn-sm rounded-0 orders">Orders</i></a>': '';
                        var del = (row.orders.length)? '': '<a data-id="'+(row.id)+'" class="btn btn-danger btn-sm rounded-0 approved">Delete</i></a> | ';
                        return  '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">View</a> ';
                    }
                }
            ],
            columns: [
                { data: 'series_number'},
                { data: 'user.fullname'},
                { data: 'office.name'},
                { data: 'purpose'},
                { data: 'status'},
                { data: 'created'},
                { data: 'id'}
            ]
        });

        datatable.on('click','.edit',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'edit/'+dataId;
            window.location.replace(baseurl+href);
        });

        datatable.on('click','.orders',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            window.location.replace(mainurl+'orders/add/'+(parseInt(dataId)));
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.status',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var stat = $(this).attr('status');
            var href = baseurl+'status/'+(parseInt(dataId))+'/'+(parseInt(stat));
            Swal.fire({
                title: 'Mark As '+(status[parseInt(stat)])+'?',
                text: 'Are You Sure',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'POST',
                        method: 'POST',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        datatable.on('click','.pdf',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = 'print-request/'+dataId;
            window.open(baseurl+href);
        });

        $('#search').click(function () {
            var startDate = moment($('#start-date').val()).format('Y-M-DD');
            var endDate = moment($('#end-date').val()).format('Y-M-DD');

            Swal.fire({
                icon: 'info',
                title: '',
                text: 'Please Wait',
                allowOutsideClick: false,
                showConfirmButton: false,
                timerProgressBar: false,
                didOpen: function () {
                    Swal.showLoading();
                    $('#search').prop('disabled', true);
                }
            });

            table.ajax.url(baseurl+'getReports?start_date='+(startDate)+'&&end_date='+(endDate)+'').load(function () {
                Swal.close();
                $('#search').prop('disabled', false);
            }, false);
        });

        $('#status').change(function (e) {
            var value = $(this).val();
            table.columns([4]).search((value) ? '^' + (value) + '$' : '', true, false).draw();
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>