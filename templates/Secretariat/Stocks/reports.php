<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Stock> $stocks
 */
?>
<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Stock Reports</h3>
            <div class="card-tools">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 col-md-5 col-lg-5 mb-2">
                    <?=$this->Form->label('description', ucwords('description'))?>
                    <?=$this->Form->select('description',[
                        ['text' => strtoupper('PROCUREMENT'), 'value' => strtoupper('PROCUREMENT')],
                        ['text' => strtoupper('INVENTORY'), 'value' => strtoupper('INVENTORY')]
                    ],[
                        'class' => 'form-control rounded-0',
                        'id' => 'description',
                        'empty' => strtoupper('all')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 mb-2">
                    <?=$this->Form->label('start_date', ucwords('Start date'))?>
                    <?=$this->Form->text('start_date',[
                        'class' => 'form-control rounded-0',
                        'id' => 'start-date',
                        'placeholder' => ucwords('Start date')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-3 col-lg-3 mb-2">
                    <?=$this->Form->label('end_date', ucwords('End date'))?>
                    <?=$this->Form->text('end_date',[
                        'class' => 'form-control rounded-0',
                        'id' => 'end-date',
                        'placeholder' => ucwords('End date')
                    ])?>
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item</th>
                                <th>Unit</th>
                                <th>Description</th>
                                <th>Last Stocks</th>
                                <th>Quantity</th>
                                <th>Total Stocks</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        'use strict';

        var baseurl = mainurl+'stocks/';
        var url = '';

        var datatable = $('#datatable');
        var table = datatable.DataTable({
            destroy:true,
            dom:'lBfrtip',
            processing:true,
            responsive: true,
            serchDelay:3500,
            deferRender: true,
            pagingType: 'full_numbers',
            lengthMenu:[ 100, 200, 300, 400, 500],
            ajax:{
                url:baseurl+'getReports',
                method: 'GET',
                dataType: 'JSON'
            },
            buttons: [
                {
                    extend: 'print',
                    title: 'Print',
                    text: 'Print',
                    className:'btn btn-secondary rounded-0',
                    attr:  {
                        id: 'print'
                    },
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7,]
                    },
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10px' )
                            .prepend('');
                        $(win.document.body).find( 'table tbody' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' ).css({'background':'transparent'});

                    },
                    footer:true,
                    messageBottom:function () {
                        return 'Exported By '+ (username);
                    }
                },
                {
                    extend: 'pdfHtml5',
                    attr:  {
                        id: 'pdf'
                    },
                    text: 'PDF',
                    title: 'PDF Reports',
                    tag: 'button',
                    className:'btn btn-danger rounded-0',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7,]
                    },
                    action: function(e, dt, node, config) {
                        Swal.fire({
                            title:'PDF',
                            text:'Export To PDF?',
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes'
                        }).then(function (result) {
                            if (result.isConfirmed) {
                                setTimeout(function(){
                                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                                }, 1000);
                            }
                        });
                    },
                    customize: function(doc) {
                        doc.pageMargins = [2, 2, 2, 2 ];
                        doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.styles.tableHeader.fontSize = 10;
                        doc.styles.tableBodyEven.fontSize = 10;
                        doc.styles.tableBodyOdd.fontSize = 10;
                        doc.styles.tableFooter.fontSize = 10;

                    },
                    download: 'open',
                    footer:true,
                    messageBottom:function () {
                        return 'Exported By '+ (username);
                    }
                },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function ( data, type, full, meta ) {
                        const row = meta.row;
                        return  row+1;
                    }
                },
                {
                    targets: 7,
                    data: null,
                    render: function(data,type,row){
                        return moment(row.created).format('Y/MM/DD H:m');
                    }
                },
            ],
            columns: [
                { data: 'id'},
                { data: 'item.description'},
                { data: 'item.unit.name'},
                { data: 'description'},
                { data: 'last_stocks'},
                { data: 'qty'},
                { data: 'total_stocks'},
                { data: 'created'},
            ]
        });

        datatable.on('click','.delete',function (e) {
            e.preventDefault();
            var dataId = $(this).attr('data-id');
            var href = baseurl+'delete/'+dataId;
            Swal.fire({
                title: 'Delete File?',
                text: 'Are You Sure',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function (result) {
                if (result.isConfirmed) {
                    $.ajax({
                        url:href,
                        type: 'DELETE',
                        method: 'DELETE',
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                        },
                        dataType:'JSON',
                        beforeSend:function () {
                            Swal.fire({
                                icon: 'info',
                                title: '',
                                text: 'Please Wait',
                                allowOutsideClick: false,
                                showConfirmButton: false,
                                timerProgressBar: false,
                                didOpen: function () {
                                    Swal.showLoading();
                                }
                            });
                        }
                    }).done(function (data, status, xhr) {
                        swal('success', data.result, data.message);
                        table.ajax.reload(null, false);
                    }).fail(function (xhr, status, error) {
                        const response = JSON.parse(xhr.responseText);
                        swal('info', response.result, response.message);
                    });
                }
            });
        });

        $('#description').change(function (e) {
           var value = $(this).val();
           table.ajax.url(baseurl+'getReports?description='+(value)).load(null, false);
        });

        var minDate, maxDate;

        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[7] );

                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );

        minDate = new DateTime($('#start-date'), {
            format: 'Y/MM/DD'
        });
        maxDate = new DateTime($('#end-date'), {
            format: 'Y/MM/DD'
        });

        // Refilter the table
        $('#start-date, #end-date').on('change', function () {
            table.draw();
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

    });
</script>

