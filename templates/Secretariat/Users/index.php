
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">User List</h3>
                <div class="card-tools">

                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table id="datatable" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Full Name</th>
                                        <th>Birthdate</th>
                                        <th>TIN No.</th>
                                        <th>Plantilla No.</th>
                                        <th>Position</th>
                                        <th>Role</th>
                                        <th>Contact No.</th>
                                        <th>Department</th>
                                        <th>Position</th>
                                        <th>Created</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            'use strict';

            var baseurl = mainurl+'users/';
            var url = '';

            var datatable = $('#datatable');
            var table = datatable.DataTable({
                destroy:true,
                processing:true,
                responsive: true,
                serchDelay:3500,
                deferRender: true,
                pagingType: 'full_numbers',
                lengthMenu:[ 100, 200, 300, 400, 500],
                ajax:{
                    url:baseurl+'getUsers',
                    method: 'GET',
                    dataType: 'JSON'
                },
                columnDefs: [
                    {
                        targets: 0,
                        render: function ( data, type, full, meta ) {
                            const row = meta.row;
                            return  row+1;
                        }
                    },
                    {
                        targets: 11,
                        data: null,
                        render: function(data, type, row, meta){
                            return moment(row.created).format('Y-M-d h:m A');
                        }
                    },
                    {
                        targets: 12,
                        data: null,
                        render: function(data, type, row, meta){
                            return '<a data-id="'+row.id+'" class="btn btn-primary btn-sm rounded-0 edit">Edit</a>';
                        }
                    }
                ],
                columns: [
                    { data: 'id'},
                    { data: 'username'},
                    { data: 'fullname'},
                    { data: 'bdate'},
                    { data: 'tin_no'},
                    { data: 'plantilla_no'},
                    { data: 'position'},
                    { data: 'role'},
                    { data: 'status'},
                    { data: 'contact'},
                    { data: 'department.name'},
                    { data: 'created'},
                    { data: 'id'},
                ]
            });

            datatable.on('click','.edit',function (e) {
                e.preventDefault();
                var dataId = $(this).attr('data-id');
                var href = 'edit/'+dataId;
                window.location.replace(baseurl+href);
            });

            function swal(icon, result, message) {
                Swal.fire({
                    icon:icon,
                    title:result,
                    text:message,
                    timer:5000
                });
            }

        });
    </script>
