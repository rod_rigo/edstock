<?php
/**
 * @var \App\View\AppView $this
 *
 * */
?>

<div class="login-box">
    <div class="login-logo">
        <?= $this->Html->image('/icon.png',['alt'=>'',
            'class'=>'brand-image img-circle elevation-3','style'=>'opacity:0.8', 'width'=>'100', 'height'=>'100'])
            ?>
            <br>
            <b>EdStock</b>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to continue</p>
                <div class="text-center"><?= $this->Flash->render() ?></div>
                <?= $this->Form->create($user,['id' => 'form', 'type' => 'file']) ?>
                <div class="input-group mb-3">
                    <?= $this->Form->text('username',[
                        'type'=>'text',
                        'class'=>'form-control',
                        'placeholder'=>'Username',
                        'label'=>false,
                        'required' => true,
                        'id' => 'username'
                    ]) ?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?= $this->Form->password('password',[
                        'class'=>'form-control',
                        'placeholder'=>'Password',
                        'label'=>false,
                        'required' => true,
                        'id' => 'password'
                    ]) ?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                <?= $this->Form->end() ?>
            <!-- <p class="mb-1">
                <a href="forgot-password.html">I forgot my password</a>
            </p> -->
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<script>
    'use strict';
    $(document).ready(function (e) {

        const baseurl = mainurl+'users/';
        
        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl+'login',
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend:function () {
                    Swal.fire({
                        icon: 'info',
                        title: '',
                        text: 'Please Wait.',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('button[type="submit"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('button[type="submit"]').prop('disabled', true);
                swal('success', data.result, data.message);
                window.location.replace(data.redirect);
            }).fail(function (data, status, xhr) {
                $('button[type="submit"]').prop('disabled', false);
                swal('info', data.responseJSON.result, data.responseJSON.message);
            });
        });

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }
        
    })
</script>