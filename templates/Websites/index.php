<?php
/**
 * @var \App\View\AppView $this
 */
?>

<!-- slider Area Start-->
<div class="slider-area ">
    <div class="slider-active">
        <div class="single-slider  hero-overly slider-height d-flex align-items-center" data-background="<?=$this->Url->assetUrl('/img/bg-hero.png')?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-11 col-lg-11">
                        <div class="hero__caption">
                            <div class="hero-text1">
                                <div class="col-sm-12 col-md-11 col-lg-11 d-flex justify-content-start align-items-center">
                                    <img src="<?=$this->Url->assetUrl('/img/a1.png')?>" loading="lazy" alt="" height="100">
                                    <img src="<?=$this->Url->assetUrl('/img/a2.png')?>" loading="lazy" alt="" height="100">
                                    <img src="<?=$this->Url->assetUrl('/img/a3.png')?>" loading="lazy" alt="" height="100">
                                </div>
                            </div>
                            <h1 data-animation="fadeInUp" data-delay=".5s">School</h1>
                            <div class="stock-text" data-animation="fadeInUp" data-delay=".8s">
                                <h2>Division Office</h2>
                                <h2>Division Office</h2>
                            </div>
                            <div class="hero-text2 mt-110" data-animation="fadeInUp" data-delay=".9s">
                                <h1><a href="javascript:void(0);" style="font-size: 1em !important;" align="center">Santiago City</a></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider Area End-->

<!-- CountDown Area Start -->
<div class="count-area">
    <div class="container">
        <div class="count-wrapper count-bg" data-background="assets/img/gallery/section-bg3.jpg">
            <div class="row justify-content-center" >
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="count-clients">
                        <div class="single-counter">
                            <div class="count-number">
                                <span class="counter" id="total-entries">0</span>
                            </div>
                            <div class="count-text">
                                <p>Total</p>
                                <h5>Entries</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="count-clients">
                        <div class="single-counter">
                            <div class="count-number">
                                <span class="counter" id="total-quantity">0</span>
                            </div>
                            <div class="count-text">
                                <p>Total</p>
                                <h5>Quantity</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="count-clients">
                        <div class="single-counter">
                            <div class="count-number">
                                <span class="counter" id="total-cost">0</span>
                            </div>
                            <div class="count-text">
                                <p>Total</p>
                                <h5>Cost</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    'use strict';
    $(document).ready(function (e) {

        const baseurl = mainurl+'websites/';

        var cost = 0;
        var quantity = 0;
        var entry = 0;

        function entries() {
            $.ajax({
                url: baseurl+'entries',
                type: 'GET',
                method: 'GET',
                dataType: 'JSON',
                beforeSend:function () {
//                    Swal.fire({
//                        icon: 'info',
//                        title: '',
//                        text: 'Please Wait.',
//                        allowOutsideClick: false,
//                        showConfirmButton: false,
//                        timerProgressBar: false,
//                        didOpen: function () {
//                            Swal.showLoading();
//                        }
//                    });
                    $('#total-entries').text(parseFloat(entry));
                    $('#total-quantity').text(parseFloat(quantity));
                    $('#total-cost').text(parseFloat(cost));
                },
            }).done(function (data, status, xhr) {

                cost = parseFloat(data.cost);
                quantity = parseFloat(data.quantity);
                entry = parseFloat(data.entries);

                $('#total-entries').text(parseFloat(data.entries));
                $('#total-quantity').text(parseFloat(data.quantity));
                $('#total-cost').text(parseFloat(data.cost));
            }).fail(function (data, status, xhr) {
                $('button[type="submit"]').prop('disabled', false);
                swal('info', data.responseJSON.result, data.responseJSON.message);
            });
        }

        function swal(icon, result, message) {
            Swal.fire({
                icon:icon,
                title:result,
                text:message,
                timer:5000
            });
        }

        entries();

        setInterval(function () {
            entries();
        }, 900000);

    })
</script>
<!-- CountDown Area End -->


