<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    $(function () {
        Swal.fire({
            icon:'error',
            title:'Error',
            text:'<?= $message ?>',
            timer:5000,
            timerProgressBar:true,
            toast:true,
            position:'top-right',
            showConfirmButton:false,
        });
    });
</script>
