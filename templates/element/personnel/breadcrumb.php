<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <?php
                $pageTitle = $this->fetch('title');
                if ($pageTitle === 'Personnel/Fclusters') {
                    $pageTitle = 'Fund Clusters';
                } elseif ($pageTitle === 'Inventories') {
                    $pageTitle = 'Item Inventory';
                } elseif ($pageTitle === 'Personnel/Requests') {
                    $pageTitle = 'Purchase Requests';
                } elseif ($pageTitle === 'Personnel/Dashboards') {
                    $pageTitle = 'Dashboard';
                } elseif ($pageTitle === 'Personnel/Suppliers') {
                    $pageTitle = 'Suppliers';
                } elseif ($pageTitle === 'Personnel/Units') {
                    $pageTitle = 'Units';
                } elseif ($pageTitle === 'Personnel/Plans') {
                    $pageTitle = 'Annual Procurement Plan';
                } elseif ($pageTitle === 'Personnel/Orders') {
                    $pageTitle = 'Purchase Orders';
                } elseif ($pageTitle === 'Personnel/Heads') {
                    $pageTitle = 'Heads';
                } elseif ($pageTitle === 'Personnel/Methods') {
                    $pageTitle = 'Procurement Methods';
                } elseif ($pageTitle === 'Personnel/Users') {
                    $pageTitle = 'Users';
                } elseif ($pageTitle === 'Personnel/RequestItems') {
                    $pageTitle = 'Request Item';
                } elseif ($pageTitle === 'Personnel/RequestItemSerieses') {
                    $pageTitle = 'Request Series';
                } 

                ?>
                <h1 class="m-0"><?= !empty(substr($pageTitle, -15)) ? substr($pageTitle, -50) : 'Dashboard' ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><?= $this->Html->link('EdStock', '/') ?></li>
                    <li class="breadcrumb-item active"><?= substr($pageTitle, -50) ?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
