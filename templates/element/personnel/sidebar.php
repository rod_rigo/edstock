<?php
/**
 * @var  \App\View\AppView $this
 */
$title = substr($this->fetch('title'),6);
?>
<!-- Main Sidebar Container -->

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center p-2">
                <img src="<?=$this->Url->assetUrl('/icon.png')?>" height="100" width="100" alt="">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                <h4 class="brand-text font-weight-light">
                    <strong>EdStock</strong>
                </h4>
            </div>
        </div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <?php if ($this->request->getSession()->check('Auth.User')) : ?>
                <?php
                $fullname = h($this->request->getSession()->read('Auth.User.fullname'));
$paddingLeft = min(max(strlen($fullname) * 5, 10), 0); // Adjust the values as needed
?>
<a class="d-block" style="padding-left: <?= $paddingLeft ?>px;">
  <?= $fullname ?>
</a>
<?php endif; ?>
</div>
</div>
<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <?= $this->Html->link('<i class="nav-icon fas fa-solid fa-user"></i><p> Account</p>', '/personnel/users/account/'.(intval($auth['id'])), ['class' => 'nav-link ' , 'escape' => false]) ?>
        </li>
        <li class="nav-item">
            <?php $active = $title == 'Dashboard' ? 'active' : ''; ?>
            <?= $this->Html->link('<i class="nav-icon fas fa-solid fa-chart-line"></i>
                <p>
                Dashboard
                </p>', '/personnel/dashboards', ['class' => 'nav-link ' . $active, 'escape' => false]) ?>
            </li>
            <li class="nav-item">
                <?php $active = $title == 'Request Item' ? 'active' : ''; ?>
                <?= $this->Html->link('<i class="fas fa-tv nav-icon"></i>
                <p>Requests</p>', '/personnel/request-items', ['class' => 'nav-link ' . $active, 'escape' => false]) ?>
            </li>

            <li class="nav-item">
                <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p> Reports</p><i class="right fas fa-angle-right"></i>','',['class'=>'nav-link ','escape'=>false]) ?>
                <ul class="nav nav-treeview pl-3">
                    <li class="nav-item">
                        <?= $this->Html->link('<i class="nav-icon fas fa-file"></i><p>RIS</p>', '/personnel/request-item-serieses/index/'.(1), ['class' => 'nav-link ', 'escape' => false]) ?>
                    </li>
                    <li class="nav-item">
                        <?= $this->Html->link('<i class="nav-icon fas fa-file"></i><p>ICS</p>', '/personnel/request-item-serieses/index/'.(0), ['class' => 'nav-link ', 'escape' => false]) ?>
                    </li>
                </ul>
            </li>

        </ul> <!-- Close the <ul> for the first-level menu -->
            <!-- /.nav -->
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>