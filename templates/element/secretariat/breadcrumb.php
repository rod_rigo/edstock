<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <?php
                $pageTitle = $this->fetch('title');
                if ($pageTitle === 'Secretariat/FundClusters') {
                    $pageTitle = 'Fund Clusters';
                } elseif ($pageTitle === 'Inventories') {
                    $pageTitle = 'Item Inventory';
                } elseif ($pageTitle === 'Secretariat/Requests') {
                    $pageTitle = 'Purchase Requests';
                } elseif ($pageTitle === 'Secretariat/Dashboards') {
                    $pageTitle = 'Dashboard';
                } elseif ($pageTitle === 'Secretariat/Suppliers') {
                    $pageTitle = 'Suppliers';
                } elseif ($pageTitle === 'Secretariat/Units') {
                    $pageTitle = 'Units';
                } elseif ($pageTitle === 'Secretariat/Plans') {
                    $pageTitle = 'Annual Procurement Plan';
                } elseif ($pageTitle === 'Secretariat/Orders') {
                    $pageTitle = 'Purchase Orders';
                } elseif ($pageTitle === 'Secretariat/Heads') {
                    $pageTitle = 'Heads';
                } elseif ($pageTitle === 'Secretariat/Methods') {
                    $pageTitle = 'Procurement Methods';
                } elseif ($pageTitle === 'Secretariat/Users') {
                    $pageTitle = 'Users';
                } elseif ($pageTitle === 'Secretariat/Items') {
                    $pageTitle = 'Items';
                } elseif ($pageTitle === 'Secretariat/Departments') {
                    $pageTitle = 'Departments';
                } elseif ($pageTitle === 'Secretariat/Categories') {
                    $pageTitle = 'Categories';
                } elseif ($pageTitle === 'Secretariat/Inspections') {
                    $pageTitle = 'Inspection & Acceptance Reports';
                } elseif ($pageTitle === 'Secretariat/Requisitions') {
                    $pageTitle = 'Requisitions & Issue Slips';
                } elseif ($pageTitle === 'Secretariat/Offices') {
                    $pageTitle = 'Offices';
                } elseif ($pageTitle === 'Secretariat/RequestItems') {
                    $pageTitle = 'Request Items';
                } elseif ($pageTitle === 'Secretariat/RequestItemSerieses') {
                    $pageTitle = 'Request Series';
                }  elseif ($pageTitle === 'Secretariat/Stocks') {
                    $pageTitle = 'Stocks';
                } 

                ?>
                <h1 class="m-0"><?= !empty(substr($pageTitle, -15)) ? substr($pageTitle, -50) : 'Dashboard' ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><?= $this->Html->link('EdStock', '/') ?></li>
                    <li class="breadcrumb-item active"><?= substr($pageTitle, -50) ?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
