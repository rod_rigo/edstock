<?php
/**
 * @var  \App\View\AppView $this
 */
$title = substr($this->fetch('title'),6);
?>
<!-- Main Sidebar Container -->

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link" href="javascript:void(0);">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center p-2">
                <img src="<?=$this->Url->assetUrl('/icon.png')?>" height="100" width="100" alt="">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                <h4 class="brand-text font-weight-light">
                    <strong>EdStock</strong>
                </h4>
            </div>
        </div>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a class="text-white">
                    <?=ucwords($auth['fullname'])?>
                </a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <?= $this->Html->link('<i class="nav-icon fas fa-solid fa-user"></i><p> Account</p>', '/secretariat/users/account/'.(intval($auth['id'])), ['class' => 'nav-link ' , 'escape' => false]) ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="nav-icon fas fa-solid fa-chart-line"></i><p> Dashboard</p>', '/secretariat/dashboards', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('dashboards')? 'active': null) , 'escape' => false]) ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p> Procurement</p><i class="right fas fa-angle-right"></i>','',['class'=>'nav-link ','escape'=>false]) ?>
                    <ul class="nav nav-treeview pl-3">
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p>Purchase Requests</p>','/secretariat/requests',['class'=>'nav-link ','escape'=>false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p>Purchase Orders</p>','/secretariat/orders',['class'=>'nav-link ','escape'=>false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p>Inspection & Acceptances Reports</p>','/secretariat/inspections',['class'=>'nav-link ','escape'=>false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="fas fa-brands fa-wpforms nav-icon"></i><p>Requisition & Issue Slips</p>','/secretariat/requisitions',['class'=>'nav-link ','escape'=>false]) ?>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Requests</p>', '/secretariat/request-items', ['class' => 'nav-link ', 'escape' => false]) ?>
                </li>

                <li class="nav-header">SETTINGS</li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="nav-icon fas fa-tv"></i><p> Supplies & Equipment</p>', '/secretariat/items', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Items')? 'active': null), 'escape' => false]) ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="fas fa-pencil nav-icon"></i><p> Inventory Requests</p><i class="right fas fa-angle-right"></i>','',['class'=>'nav-link','escape'=>false]) ?>
                    <ul class="nav nav-treeview pl-3">
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Inventory RIS</p>', '/secretariat/request-item-serieses/index', ['class' => 'nav-link ', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Inventory ICS</p>', '/secretariat/request-item-serieses/ics/', ['class' => 'nav-link ', 'escape' => false]) ?>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <?= $this->Html->link('<i class="fas fa-file nav-icon"></i><p> Reports</p><i class="right fas fa-angle-right"></i>','',['class'=>'nav-link','escape'=>false]) ?>
                    <ul class="nav nav-treeview pl-3">
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Stocks</p>', '/secretariat/stocks/reports', ['class' => 'nav-link ', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Procurements</p>', '/secretariat/requests/reports', ['class' => 'nav-link ', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="nav-icon fas fa-pencil"></i><p>Inventories</p>', '/secretariat/request-items/reports', ['class' => 'nav-link ', 'escape' => false]) ?>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <?= $this->Html->link('<i class="fas fa-sharp fa-building nav-icon"></i><p> Division Details</p><i class="right fas fa-angle-right"></i>','',['class'=>'nav-link','escape'=>false]) ?>
                    <ul class="nav nav-treeview pl-3">
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Departments</p>', '/secretariat/departments', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Departments')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Ranges</p>', '/secretariat/ranges', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Ranges')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Fund Clusters</p>', '/secretariat/fund-clusters', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('FundClusters')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Units</p>', '/secretariat/units', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Units')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Categories</p>', '/secretariat/categories', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Categories')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Sub-Categories</p>', '/secretariat/sub-categories', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('SubCategories')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Offices</p>', '/secretariat/offices', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Offices')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Suppliers</p>', '/secretariat/suppliers', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Suppliers')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p>Procurement Methods</p>', '/secretariat/methods', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Methods')? 'active': null), 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i class="far fa-circle nav-icon"></i><p> Heads</p>', '/secretariat/heads', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Heads')? 'active': null), 'escape' => false]) ?>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="fas fa-users nav-icon"></i><p> Users</p>', '/secretariat/users', ['class' => 'nav-link '.(ucwords($controller) == ucwords('Users')? 'active': null) , 'escape' => false]) ?>
                </li>
                <li class="nav-item">
                    <?= $this->Html->link('<i class="nav-icon fas fa-solid fa-file-alt"></i><p> Logs</p>', '/secretariat/stocks/logs', ['class' => 'nav-link ' . (ucwords($controller) == ucwords('Stocks') && ucwords($action) == ucwords('logs')? 'active': null), 'escape' => false]) ?>
                </li>

            </ul> <!-- Close the <ul> for the first-level menu --><!-- /.nav -->
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>