<?php
/**
 * @var \App\View\AppView $this
 */
?>
<header>
    <!-- Header Start -->
    <div class="header-area header-transparent">
        <div class="main-header ">
            <div class="header-top d-none d-lg-block">
                <div class="container-fluid">
                    <div class="col-xl-12">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="header-info-left">
                                <ul>
                                    <li>(078) 682-0151</li>
                                    <li>deped-santiagocity.com.ph</li>
                                    <li>Mon - Fri 8:00 AM - 5:00 PM, Sat - Sun - CLOSED</li>
                                </ul>
                            </div>
                            <div class="header-info-right">
                                <ul class="header-social">
                                    <li><a href="https://web.facebook.com/SDOsantiagoCitySCTEx/"><i class="fab fa-facebook-f"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom  header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2 col-md-1">
                            <div class="logo">
                                <!-- logo-1 -->
                                <a href="javascript:void(0);" class="big-logo">
                                    <img src="<?=$this->Url->assetUrl('/icon.png')?>" height="80" width="80" alt="">
                                </a>
                                <!-- logo-2 -->
                                <a href="javascript:void(0);" class="small-logo">
                                    <img src="<?=$this->Url->assetUrl('/icon.png')?>" height="80" width="80" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Websites', 'action' => 'index'])?>">Home</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-3">
                            <div class="header-right-btn f-right d-none d-lg-block">
                                <a href="<?=$this->Url->build([
                                    'prefix' => false,
                                    'controller' => 'Users',
                                    'action' => 'login'
                                ])?>" class="btn">Login</a>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>
