<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

// Define the default $cakeDescription
$cakeDescription = 'EdStock';

// Define the $pageTitle (e.g., set it based on your logic)
$pageTitle = $this->fetch('title');

// Check if $pageTitle is set, and if so, use it as the page title
if ($pageTitle === 'Personnel/Dashboards') {
    $title = $cakeDescription . ' : ' . 'Dashboard';
} elseif ($pageTitle === 'Personnel/Departments') {
    $title = $cakeDescription . ' : ' . 'Departments';
} elseif ($pageTitle === 'Personnel/Officers') {
    $title = $cakeDescription . ' : ' . 'Officers';
} elseif ($pageTitle === 'Personnel/Offices') {
    $title = $cakeDescription . ' : ' . 'Offices';
} elseif ($pageTitle === 'Personnel/Requests') {
    $title = $cakeDescription . ' : ' . 'Purchase Requests';
} elseif ($pageTitle === 'Personnel/RequestItems') {
    $title = $cakeDescription . ' : ' . 'Request Item';
}


?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
       <?= h($this->request->getParam('controller')) ?>
   </title>
    <?= $this->Html->meta('icon.png','/icon.png',['type'=>'icon']); ?>
    <?=$this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken'));?>

   <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" rel="stylesheet">
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

   <?= $this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min',
        '/plugins/toastr/toastr.min',
        '/plugins/daterangepicker/daterangepicker',
        '/plugins/datatables-bs4/css/dataTables.bootstrap4.min',
        '/plugins/datatables-responsive/css/responsive.bootstrap4.min',
        '/plugins/datatables-buttons/css/buttons.bootstrap4.min',
        '/plugins/ekko-lightbox/ekko-lightbox',
        '/dist/css/adminlte.min',
        // '/plugins/ion-rangeslider/ion.rangeSlider.min'
        'style',
        '/jquery/css/jquery-ui',
        '/jquery/css/jquery-ui.min',
        '/plugins/icheck-bootstrap/icheck-bootstrap.min',
    ]) ?>

    <?=$this->Html->script([
        'jquery-3.5.1',
        'sweetalert2.all',
        'sweetalert2.all.min',
        'moment',
        'moment.min',
        'chart',
        'chart.min',
        'chartjs-plugin-autocolors',
        '/jquery/js/jquery-ui',
        '/jquery/js/jquery-ui.min',
    ])?>

    <script>
        const mainurl = window.location.origin+'/edstock/personnel/';
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?= $this->element('personnel/navbar')?>
        <?= $this->element('personnel/sidebar')?>

        <div class="content-wrapper">
            <?= $this->element('personnel/breadcrumb') ?>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <?= $this->fetch('content') ?>
                    </div>
                </div>
            </section>
        </div>
        <?= $this->element('personnel/footer')?>
        <?= $this->element('personnel/control-sidebar')?>
    </div>

    <?= $this->Html->script([
        '/plugins/bootstrap/js/bootstrap.bundle.min',
        '/plugins/bs-custom-file-input/bs-custom-file-input.min',
        '/dist/js/adminlte.min'
    ]) ?>
        
    <?= $this->Html->script([
        '/plugins/bootstrap/js/bootstrap.bundle.min',
        '/plugins/daterangepicker/daterangepicker',
        '/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min',
        '/plugins/datatables/jquery.dataTables.min',
        '/plugins/datatables-bs4/js/dataTables.bootstrap4.min',
        '/plugins/datatables-responsive/js/dataTables.responsive.min',
        '/plugins/datatables-responsive/js/responsive.bootstrap4.min',
        '/dist/js/adminlte.min'
    ]) ?>

    </body>
    </html>