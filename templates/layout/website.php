<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

// Define the default $cakeDescription
$cakeDescription = 'EdStock';

// Define the $pageTitle (e.g., set it based on your logic)
$pageTitle = $this->fetch('title');

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= h($cakeDescription) ?>
    </title>
    <?= $this->Html->meta('icon.png','/icon.png',['type'=>'icon']); ?>

    <!-- CSS here -->
    <?=$this->Html->css([
        '/website/assets/css/bootstrap.min',
        '/website/assets/css/owl.carousel.min',
        '/website/assets/css/slicknav',
        '/website/assets/css/animate.min',
        '/website/assets/css/magnific-popup',
        '/website/assets/css/fontawesome-all.min',
        '/website/assets/css/themify-icons',
        '/website/assets/css/slick',
        '/website/assets/css/nice-select',
        '/website/assets/css/style'
,    ])?>

    <?= $this->Html->script([
        '/plugins/jquery/jquery.min',
        'sweetalert2.all',
        'sweetalert2.all.min',
    ]) ?>

    <script>
        const mainurl = window.location.origin+'/edstock/';
    </script>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<!-- Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="<?=$this->Url->assetUrl('/icon.png')?>" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Preloader Start -->
<?=$this->element('website/header')?>
<main>
    <?=$this->Flash->render()?>
    <?= $this->fetch('content') ?>
</main>
<?=$this->element('website/footer')?>

<!-- JS here -->

<!-- counter , waypoint -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

<?=$this->Html->script([
    '/website/assets/js/vendor/modernizr-3.5.0.min',
    '/website/assets/js/popper.min',
    '/website/assets/js/bootstrap.min',
    '/website/assets/js/jquery.slicknav.min',
    '/website/assets/js/owl.carousel.min',
    '/website/assets/js/slick.min',
    '/website/assets/js/gijgo.min',
    '/website/assets/js/wow.min',
    '/website/assets/js/animated.headline',
    '/website/assets/js/jquery.magnific-popup',
    '/website/assets/js/jquery.scrollUp.min',
    '/website/assets/js/jquery.nice-select.min',
    '/website/assets/js/jquery.sticky',
    '/website/assets/js/jquery.counterup.min',
    '/website/assets/js/plugins',
    '/website/assets/js/main',
])?>

</body>
</html>