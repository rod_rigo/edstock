<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FundClustersFixture
 */
class FundClustersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'created' => 1699767361,
                'modified' => 1699767361,
                'deleted' => '2023-11-12 13:36:01',
            ],
        ];
        parent::init();
    }
}
