<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InspectionItemsFixture
 */
class InspectionItemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'inspection_id' => 1,
                'item_id' => 1,
                'quantity' => 1,
                'cost' => 1,
                'total' => 1,
                'created' => 1699845880,
                'modified' => 1699845880,
                'deleted' => '2023-11-13 11:24:40',
            ],
        ];
        parent::init();
    }
}
