<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InspectionsFixture
 */
class InspectionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'request_id' => 1,
                'order_id' => 1,
                'fund_cluster_id' => 1,
                'supplier_id' => 1,
                'office_id' => 1,
                'requisitioning_office' => 'Lorem ipsum dolor sit amet',
                'responsibility_code_center' => 'Lorem ipsum dolor sit amet',
                'is_compete' => 1,
                'created' => 1699843551,
                'modified' => 1699843551,
                'deleted' => '2023-11-13 10:45:51',
            ],
        ];
        parent::init();
    }
}
