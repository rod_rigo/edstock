<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ItemsFixture
 */
class ItemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'description' => 'Lorem ipsum dolor sit amet',
                'category_id' => 1,
                'sub_category_id' => 1,
                'unit_id' => 1,
                'price' => 1,
                'item_code' => 'Lorem ipsum dolor sit amet',
                'stock' => 1,
                'code' => 'Lorem ipsum dolor sit amet',
                'created' => 1702181253,
                'modified' => 1702181253,
                'deleted' => '2023-12-10 12:07:33',
            ],
        ];
        parent::init();
    }
}
