<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MethodsFixture
 */
class MethodsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'created' => 1699765894,
                'modified' => 1699765894,
                'deleted' => '2023-11-12 13:11:34',
            ],
        ];
        parent::init();
    }
}
