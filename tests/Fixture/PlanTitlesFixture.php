<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PlanTitlesFixture
 */
class PlanTitlesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'plan_id' => 1,
                'part_id' => 1,
                'plan_title' => 'Lorem ipsum dolor sit amet',
                'created' => 1700367641,
                'modified' => 1700367641,
                'deleted' => '2023-11-19 12:20:41',
            ],
        ];
        parent::init();
    }
}
