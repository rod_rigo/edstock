<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RangesFixture
 */
class RangesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'price' => 1,
                'created' => 1700286950,
                'modified' => 1700286950,
                'deleted' => '2023-11-18 13:55:50',
            ],
        ];
        parent::init();
    }
}
