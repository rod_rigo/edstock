<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequestItemDetailsFixture
 */
class RequestItemDetailsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'request_item_id' => 1,
                'request_item_series_id' => 1,
                'item_id' => 1,
                'cost' => 1,
                'qty' => 1,
                'total' => 1,
                'created' => '2023-11-21 16:39:33',
                'modified' => '2023-11-21 16:39:33',
                'deleted' => '2023-11-21 16:39:33',
            ],
        ];
        parent::init();
    }
}
