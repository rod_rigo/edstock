<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequestItemSeriesesFixture
 */
class RequestItemSeriesesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'request_item_id' => 1,
                'series_number' => 'Lorem ipsum dolor sit amet',
                'created' => 1700552590,
                'updated' => 1700552590,
                'deleted' => '2023-11-21 15:43:10',
            ],
        ];
        parent::init();
    }
}
