<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequisitionItemsFixture
 */
class RequisitionItemsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'requisition_id' => 1,
                'item_id' => 1,
                'is_available' => 1,
                'quantity' => 1,
                'cost' => 1,
                'total' => 1,
                'remarks' => 'Lorem ipsum dolor sit amet',
                'created' => 1700024862,
                'modified' => 1700024862,
                'deleted' => '2023-11-15 13:07:42',
            ],
        ];
        parent::init();
    }
}
