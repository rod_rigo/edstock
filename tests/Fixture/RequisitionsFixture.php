<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RequisitionsFixture
 */
class RequisitionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'request_id' => 1,
                'inspection_id' => 1,
                'order_id' => 1,
                'fund_cluster_id' => 1,
                'office_id' => 1,
                'responsibility_code_center' => 'Lorem ipsum dolor sit amet',
                'ris_no' => 'Lorem ipsum dolor sit amet',
                'created' => 1700019728,
                'modified' => 1700019728,
                'deleted' => '2023-11-15 11:42:08',
            ],
        ];
        parent::init();
    }
}
