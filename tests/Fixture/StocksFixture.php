<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StocksFixture
 */
class StocksFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'item_id' => 1,
                'description' => 'Lorem ipsum dolor sit amet',
                'price' => 1,
                'last_stocks' => 1,
                'qty' => 1,
                'total_stocks' => 1,
                'created' => 1700707599,
                'modified' => 1700707599,
                'deleted' => '2023-11-23 10:46:39',
            ],
        ];
        parent::init();
    }
}
