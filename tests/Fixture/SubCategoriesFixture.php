<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubCategoriesFixture
 */
class SubCategoriesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'category_id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'created' => 1700281709,
                'modified' => 1700281709,
                'delete' => '2023-11-18 12:28:29',
            ],
        ];
        parent::init();
    }
}
