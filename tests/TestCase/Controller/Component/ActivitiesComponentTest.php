<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ActivitiesComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\ActivitiesComponent Test Case
 */
class ActivitiesComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\ActivitiesComponent
     */
    protected $Activities;

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Activities = new ActivitiesComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Activities);

        parent::tearDown();
    }
}
