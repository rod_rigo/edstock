<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Personnel;

use App\Controller\Personnel\RequestItemsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Personnel\RequestItemsController Test Case
 *
 * @uses \App\Controller\Personnel\RequestItemsController
 */
class RequestItemsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.RequestItems',
        'app.Users',
        'app.Offices',
        'app.FundClusters',
        'app.Departments',
        'app.RequestItemDetails',
    ];

    /**
     * Test index method
     *
     * @return void
     * @uses \App\Controller\Personnel\RequestItemsController::index()
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     * @uses \App\Controller\Personnel\RequestItemsController::view()
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     * @uses \App\Controller\Personnel\RequestItemsController::add()
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     * @uses \App\Controller\Personnel\RequestItemsController::edit()
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     * @uses \App\Controller\Personnel\RequestItemsController::delete()
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
