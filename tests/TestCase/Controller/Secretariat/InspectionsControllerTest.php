<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Secretariat;

use App\Controller\Secretariat\InspectionsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Secretariat\InspectionsController Test Case
 *
 * @uses \App\Controller\Secretariat\InspectionsController
 */
class InspectionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Inspections',
        'app.Requests',
        'app.Orders',
        'app.FundClusters',
        'app.Suppliers',
        'app.Offices',
        'app.InspectionItems',
    ];

    /**
     * Test index method
     *
     * @return void
     * @uses \App\Controller\Secretariat\InspectionsController::index()
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     * @uses \App\Controller\Secretariat\InspectionsController::view()
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     * @uses \App\Controller\Secretariat\InspectionsController::add()
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     * @uses \App\Controller\Secretariat\InspectionsController::edit()
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     * @uses \App\Controller\Secretariat\InspectionsController::delete()
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
