<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Secretariat;

use App\Controller\Secretariat\RequestItemsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Secretariat\RequestItemsController Test Case
 *
 * @uses \App\Controller\Secretariat\RequestItemsController
 */
class RequestItemsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.RequestItems',
        'app.Users',
        'app.Offices',
        'app.Departments',
        'app.RequestItemDetails',
        'app.RequestItemSerieses',
    ];

    /**
     * Test index method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequestItemsController::index()
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequestItemsController::view()
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequestItemsController::add()
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequestItemsController::edit()
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequestItemsController::delete()
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
