<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller\Secretariat;

use App\Controller\Secretariat\RequisitionsController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Secretariat\RequisitionsController Test Case
 *
 * @uses \App\Controller\Secretariat\RequisitionsController
 */
class RequisitionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Requisitions',
        'app.Requests',
        'app.Inspections',
        'app.Orders',
        'app.FundClusters',
        'app.Offices',
        'app.RequisitionItems',
    ];

    /**
     * Test index method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequisitionsController::index()
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequisitionsController::view()
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequisitionsController::add()
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequisitionsController::edit()
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     * @uses \App\Controller\Secretariat\RequisitionsController::delete()
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
