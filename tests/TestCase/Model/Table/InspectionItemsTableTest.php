<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InspectionItemsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InspectionItemsTable Test Case
 */
class InspectionItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InspectionItemsTable
     */
    protected $InspectionItems;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.InspectionItems',
        'app.Inspections',
        'app.Items',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('InspectionItems') ? [] : ['className' => InspectionItemsTable::class];
        $this->InspectionItems = $this->getTableLocator()->get('InspectionItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->InspectionItems);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\InspectionItemsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\InspectionItemsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
