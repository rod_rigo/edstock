<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlanTitlesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlanTitlesTable Test Case
 */
class PlanTitlesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PlanTitlesTable
     */
    protected $PlanTitles;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.PlanTitles',
        'app.Plans',
        'app.Parts',
        'app.Plandetails',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('PlanTitles') ? [] : ['className' => PlanTitlesTable::class];
        $this->PlanTitles = $this->getTableLocator()->get('PlanTitles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->PlanTitles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\PlanTitlesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\PlanTitlesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
