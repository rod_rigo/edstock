<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestItemDetailsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestItemDetailsTable Test Case
 */
class RequestItemDetailsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestItemDetailsTable
     */
    protected $RequestItemDetails;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.RequestItemDetails',
        'app.RequestItems',
        'app.Items',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('RequestItemDetails') ? [] : ['className' => RequestItemDetailsTable::class];
        $this->RequestItemDetails = $this->getTableLocator()->get('RequestItemDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->RequestItemDetails);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemDetailsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemDetailsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
