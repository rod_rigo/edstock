<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestItemSeriesesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestItemSeriesesTable Test Case
 */
class RequestItemSeriesesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestItemSeriesesTable
     */
    protected $RequestItemSerieses;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.RequestItemSerieses',
        'app.RequestItems',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('RequestItemSerieses') ? [] : ['className' => RequestItemSeriesesTable::class];
        $this->RequestItemSerieses = $this->getTableLocator()->get('RequestItemSerieses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->RequestItemSerieses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemSeriesesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemSeriesesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
