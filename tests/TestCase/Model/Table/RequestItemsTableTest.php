<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RequestItemsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RequestItemsTable Test Case
 */
class RequestItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RequestItemsTable
     */
    protected $RequestItems;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.RequestItems',
        'app.Users',
        'app.Offices',
        'app.Departments',
        'app.RequestItemDetails',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('RequestItems') ? [] : ['className' => RequestItemsTable::class];
        $this->RequestItems = $this->getTableLocator()->get('RequestItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->RequestItems);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findSeriesNumber method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemsTable::findSeriesNumber()
     */
    public function testFindSeriesNumber(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\RequestItemsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
