-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2023 at 07:57 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edstock_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifed` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created`, `modifed`, `deleted`) VALUES
(1, 'Expendable', '2023-11-18 12:52:55', '2023-11-18 04:52:55', NULL),
(2, 'Non-Expendable', '2023-11-18 13:04:03', '2023-11-18 05:04:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifed` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created`, `modifed`, `deleted`) VALUES
(1, 'Accounting Unit', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(2, 'Administrative Unit', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(3, 'Supply Unit', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(4, 'Curriculum Implementation Division', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(5, 'Schools Governance and Operations Division', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(6, 'Cashier Unit', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(7, 'Budget Unit', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(8, 'Human Resource Management Office', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(9, 'OSDS', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL),
(10, 'OASDS', '2023-11-12 04:36:44', '2023-11-12 04:36:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fund_clusters`
--

CREATE TABLE `fund_clusters` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `fund_clusters`
--

INSERT INTO `fund_clusters` (`id`, `name`, `created`, `modified`, `deleted`) VALUES
(1, 'DepED Region II - 10001-01', '2023-10-10 19:37:37', '2023-11-12 05:10:06', NULL),
(2, 'MOOE', '2023-10-15 09:35:58', '2023-11-12 05:10:06', NULL),
(3, 'Central Office', '2023-10-15 09:36:07', '2023-11-12 05:10:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heads`
--

CREATE TABLE `heads` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `position` varchar(55) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `heads`
--

INSERT INTO `heads` (`id`, `name`, `position`, `created`, `modified`, `deleted`) VALUES
(1, 'JAMES PAUL R. SANTIAGO', 'ADMINISTRATIVE OFFICER IV', '2023-11-03 14:50:11', '2023-11-13 22:18:07', NULL),
(2, 'FLORDELIZA R. GECOBE', 'SCHOOLS DIVISION SUPERINTENDENT', '2023-11-03 14:50:49', '2023-11-13 22:19:39', NULL),
(4, 'MELANIE O. SO', 'ADMINISTRATIVE OFFICER IV', '2023-11-13 22:18:30', '2023-11-13 22:18:30', NULL),
(5, 'RICARDO Q. CABUGAO JR.', 'INFORMATION TECHNOLOGY OFFICER I', '2023-11-13 22:18:57', '2023-11-13 22:18:57', NULL),
(6, 'JONELL C. MANGULABNAN', 'ADMINISTRATIVE OFFICER II', '2023-11-13 22:21:47', '2023-11-13 22:21:47', NULL),
(7, 'LEONIDA F. CULANG MPA', 'ADMINISTRATIVE OFFICER V', '2023-11-13 22:22:15', '2023-11-13 22:22:15', NULL),
(8, 'CHERY ANN R. SEGUNDO CPA', 'ACCOUNTANT II', '2023-11-13 22:22:39', '2023-11-13 22:22:39', NULL),
(9, 'JONATHAN A. FRONDA PHD, CESO VI', 'ASSISTANT SCHOOLS DIVISION SUPERINTENDENT', '2023-11-13 22:23:51', '2023-11-13 22:23:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

CREATE TABLE `inspections` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `fund_cluster_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `requisitioning_office` varchar(255) DEFAULT NULL,
  `responsibility_code_center` varchar(255) DEFAULT NULL,
  `is_compete` tinyint(4) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_items`
--

CREATE TABLE `inspection_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `inspection_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `quantity` double NOT NULL,
  `cost` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `iar_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `created`, `iar_id`) VALUES
(6, '2023-10-15 10:07:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `stock` double NOT NULL,
  `code` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `description`, `category_id`, `sub_category_id`, `unit_id`, `price`, `stock`, `code`, `created`, `modified`, `deleted`) VALUES
(1, 'Asda', 1, 1, 4, 100, 3, 'SPLV', '2023-11-18 14:46:45', '2023-11-18 14:46:45', NULL),
(2, 'A4 Bond Paper', 1, 1, 3, 169, 5, 'SPLV', '2023-11-18 14:47:36', '2023-11-18 14:47:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `methods`
--

CREATE TABLE `methods` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `methods`
--

INSERT INTO `methods` (`id`, `name`, `created`, `modified`, `deleted`) VALUES
(1, 'CHECK', '2023-11-08 00:04:58', '2023-11-08 00:04:58', NULL),
(2, 'CASH', '2023-11-08 00:05:26', '2023-11-08 00:05:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `name`, `created`, `modified`, `deleted`) VALUES
(7, 'SDO - Santiago City', '2023-11-12 05:28:57', '2023-11-12 05:28:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cost` double NOT NULL,
  `qty` int(11) NOT NULL,
  `total` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `fund_cluster_id` int(11) DEFAULT NULL,
  `po_no` varchar(255) DEFAULT NULL,
  `method_id` int(11) NOT NULL,
  `place_of_delivery` varchar(255) NOT NULL,
  `date_of_delivery` datetime NOT NULL,
  `delivery_term` varchar(255) NOT NULL,
  `payment_term` varchar(255) NOT NULL,
  `fund_available` varchar(25) DEFAULT NULL,
  `ors_burs_no` varchar(255) DEFAULT NULL,
  `date_of_burs` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `plandetails`
--

CREATE TABLE `plandetails` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `jan` int(11) DEFAULT NULL,
  `feb` int(11) DEFAULT NULL,
  `mar` int(11) DEFAULT NULL,
  `apr` int(11) DEFAULT NULL,
  `may` int(11) DEFAULT NULL,
  `jun` int(11) DEFAULT NULL,
  `jul` int(11) DEFAULT NULL,
  `aug` int(11) DEFAULT NULL,
  `sep` int(11) DEFAULT NULL,
  `oct` int(11) DEFAULT NULL,
  `nov` int(11) DEFAULT NULL,
  `decm` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `prepared_by` varchar(55) NOT NULL,
  `position` varchar(55) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `ranges`
--

CREATE TABLE `ranges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `price_to` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ranges`
--

INSERT INTO `ranges` (`id`, `name`, `price`, `price_to`, `created`, `modified`, `deleted`) VALUES
(1, 'SPHV', 5001, 50000, '2023-11-18 14:14:55', '2023-11-18 14:22:31', NULL),
(2, 'SPLV', 1, 5000, '2023-11-18 14:22:50', '2023-11-18 14:22:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `requestdetails`
--

CREATE TABLE `requestdetails` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `cost` double NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `office_id` int(11) NOT NULL,
  `fund_cluster_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `series_number` varchar(255) NOT NULL,
  `pr_no` varchar(255) NOT NULL,
  `responsibility_center_code` varchar(255) DEFAULT NULL,
  `purpose` text NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `request_items`
--

CREATE TABLE `request_items` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `fund_cluster_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `pr_no` varchar(255) NOT NULL,
  `responsibility_center_code` int(255) NOT NULL,
  `purpose` text NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_item_details`
--

CREATE TABLE `request_item_details` (
  `id` int(11) NOT NULL,
  `request_item_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requisitions`
--

CREATE TABLE `requisitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `inspection_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `fund_cluster_id` int(10) UNSIGNED NOT NULL,
  `office_id` int(10) UNSIGNED NOT NULL,
  `responsibility_code_center` varchar(255) DEFAULT NULL,
  `ris_no` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `requisition_items`
--

CREATE TABLE `requisition_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `requisition_id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `is_available` tinyint(4) NOT NULL DEFAULT 1,
  `quantity` double NOT NULL,
  `cost` double NOT NULL,
  `total` double NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `description` varchar(55) NOT NULL,
  `last_stocks` double NOT NULL,
  `qty` double NOT NULL,
  `total_stocks` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `item_id`, `description`, `last_stocks`, `qty`, `total_stocks`, `created`, `modified`, `deleted`) VALUES
(1, 19, 'INSPECTION', 90, 1, 91, '2023-11-13 11:50:34', '2023-11-13 11:50:34', NULL),
(2, 20, 'INSPECTION', 2, 1, 3, '2023-11-13 11:50:34', '2023-11-13 11:50:34', NULL),
(6, 22, 'INSPECTION', 0, 1, 1, '2023-11-13 14:46:56', '2023-11-13 14:46:56', NULL),
(7, 19, 'INSPECTION', 91, 1, 92, '2023-11-13 16:21:26', '2023-11-13 16:21:26', NULL),
(8, 20, 'INSPECTION', 3, 1, 4, '2023-11-13 16:21:26', '2023-11-13 16:21:26', NULL),
(9, 20, 'INSPECTION', 4, 10, 14, '2023-11-13 21:21:26', '2023-11-13 21:21:26', NULL),
(10, 20, 'INSPECTION', 14, 1, 15, '2023-11-15 10:52:39', '2023-11-15 10:52:39', NULL),
(11, 20, 'INSPECTION', 15, 1, 16, '2023-11-15 14:16:08', '2023-11-15 14:16:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `delete` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `created`, `modified`, `delete`) VALUES
(1, 1, 'Office Supply', '2023-11-18 12:54:34', '2023-11-18 13:01:10', NULL),
(3, 1, 'Common Electrical Supplies', '2023-11-18 13:03:15', '2023-11-18 13:03:15', NULL),
(4, 1, 'Medical Supplies', '2023-11-18 13:03:27', '2023-11-18 13:03:27', NULL),
(5, 1, 'Other Supplies', '2023-11-18 13:03:38', '2023-11-18 13:03:38', NULL),
(6, 2, 'Office Equipment', '2023-11-18 13:04:20', '2023-11-18 13:04:20', NULL),
(7, 2, 'IT Equipment', '2023-11-18 13:04:28', '2023-11-18 13:04:28', NULL),
(8, 2, 'Fixtures & Furnitures', '2023-11-18 13:04:52', '2023-11-18 13:04:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `tin_no` varchar(15) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modifed` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `tin_no`, `created`, `modifed`, `deleted`) VALUES
(1, 'SISTAN SUPPLIES DEPOT', 'Santiago City', '941-104-334-000', '2023-11-12 05:22:40', '2023-11-12 05:22:40', NULL),
(2, 'A', 'A', '122142342342', '2023-11-12 05:22:40', '2023-11-12 05:22:40', NULL),
(3, 'Asd', 'Asdas', 'ASD', '2023-11-12 13:27:49', '2023-11-12 05:27:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `created`, `modified`, `deleted`) VALUES
(1, 'pc', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(2, 'bottle', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(3, 'ream', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(4, 'box', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(5, 'liter', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(6, 'pack', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(7, 'person', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(8, 'roll', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(9, 'set', '2023-11-12 05:04:49', '2023-11-12 05:04:49', NULL),
(10, 'Tubes', '2023-11-12 05:04:49', '2023-11-12 13:06:20', NULL),
(11, 'Kg', '2023-11-12 13:06:28', '2023-11-12 13:06:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `bdate` date NOT NULL,
  `tin_no` varchar(55) DEFAULT NULL,
  `plantilla_no` varchar(55) DEFAULT NULL,
  `position` varchar(55) NOT NULL,
  `role` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `token` varchar(255) DEFAULT NULL,
  `contact` varchar(55) NOT NULL,
  `department_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `bdate`, `tin_no`, `plantilla_no`, `position`, `role`, `status`, `token`, `contact`, `department_id`, `created`, `modified`, `deleted`) VALUES
(16, 'jepotsantiago', '$2y$10$8GcQHO1DXyP0HJokTJUo6OifC9pxgyOTKn4V2EKFO/y.P66sYsgti', 'James Paul Ronquillo Santiago', '2023-10-08', '010101010101', '0987654321', 'Administrative Officer IV', 'Super Administrator', 1, '655835a86d53199999999655835a86d53b', '09338146244', 2, '2023-11-12 06:51:53', '2023-11-12 06:51:53', NULL),
(18, 'jeffcalaunan', '$2y$10$IdH9WZ7EZz1eJCRtA//9COejvpiXmVU05gnSYOgtACa7iEhERuX1e', 'Marc Jefferson Calaunan', '1996-03-21', '346686124', 'ADASII-2023-0014', 'Administrative Aide VI', 'Super Administrator', 1, '65585b1239c309999999965585b1239c3f', '09173191479', 3, '2023-11-12 06:51:53', '2023-11-12 06:51:53', NULL),
(20, 'archietzy', '$2y$10$P7k4B0vXSKBSQg21rFQTRO7Pg9Rays7T9OuBDoa58S1UeDoMrehey', 'Archie Raven Molina Raga', '2002-05-26', '122142342342', '342344634234', 'Student', 'User', 1, '655852b71b97399999999655852b71b981', '09452247731', 3, '2023-11-12 06:51:53', '2023-11-12 06:51:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `fund_clusters`
--
ALTER TABLE `fund_clusters`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `heads`
--
ALTER TABLE `heads`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `inspection_items`
--
ALTER TABLE `inspection_items`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `methods`
--
ALTER TABLE `methods`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `plandetails`
--
ALTER TABLE `plandetails`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `ranges`
--
ALTER TABLE `ranges`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `requestdetails`
--
ALTER TABLE `requestdetails`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `request_items`
--
ALTER TABLE `request_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_item_details`
--
ALTER TABLE `request_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requisitions`
--
ALTER TABLE `requisitions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `requisition_items`
--
ALTER TABLE `requisition_items`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fund_clusters`
--
ALTER TABLE `fund_clusters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `heads`
--
ALTER TABLE `heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inspection_items`
--
ALTER TABLE `inspection_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `methods`
--
ALTER TABLE `methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plandetails`
--
ALTER TABLE `plandetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ranges`
--
ALTER TABLE `ranges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `requestdetails`
--
ALTER TABLE `requestdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_items`
--
ALTER TABLE `request_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_item_details`
--
ALTER TABLE `request_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requisitions`
--
ALTER TABLE `requisitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requisition_items`
--
ALTER TABLE `requisition_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
