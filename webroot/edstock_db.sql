/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : edstock_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 28/11/2023 13:00:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activities
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `prefix` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `data_key` int NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of activities
-- ----------------------------
INSERT INTO `activities` VALUES (1, 16, 'Admin', 'Users', 'Account', 16, '{\"id\":16,\"username\":\"jepotsantiago\",\"fullname\":\"James Paul Ronquillo Santiago\",\"bdate\":\"2023-10-08\",\"tin_no\":\"010101010101\",\"plantilla_no\":\"0987654321\",\"position\":\"Administrative Officer IV\",\"role\":\"Super Administrator\",\"status\":1,\"contact\":\"09338146244\",\"department_id\":2,\"created\":\"2023-11-12T06:51:53+08:00\",\"modified\":\"2023-11-12T06:51:53+08:00\",\"deleted\":null}', '2023-11-28 20:02:59', '2023-11-28 20:02:59', NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Expendable', 1, '2023-11-18 20:52:55', '2023-11-18 12:52:55', NULL);
INSERT INTO `categories` VALUES (2, 'Non-Expendable', 0, '2023-11-18 21:04:03', '2023-11-18 13:04:03', NULL);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (1, 'Accounting Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (2, 'Administrative Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (3, 'Supply Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (4, 'Curriculum Implementation Division', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (5, 'Schools Governance and Operations Division', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (6, 'Cashier Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (7, 'Budget Unit', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (8, 'Human Resource Management Office', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (9, 'OSDS', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);
INSERT INTO `departments` VALUES (10, 'OASDS', '2023-11-12 12:36:44', '2023-11-12 12:36:44', NULL);

-- ----------------------------
-- Table structure for fund_clusters
-- ----------------------------
DROP TABLE IF EXISTS `fund_clusters`;
CREATE TABLE `fund_clusters`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fund_clusters
-- ----------------------------
INSERT INTO `fund_clusters` VALUES (1, 'DepED Region II - 10001-01', '2023-10-11 03:37:37', '2023-11-12 13:10:06', NULL);
INSERT INTO `fund_clusters` VALUES (2, 'MOOE', '2023-10-15 17:35:58', '2023-11-12 13:10:06', NULL);
INSERT INTO `fund_clusters` VALUES (3, 'Central Office', '2023-10-15 17:36:07', '2023-11-12 13:10:06', NULL);

-- ----------------------------
-- Table structure for heads
-- ----------------------------
DROP TABLE IF EXISTS `heads`;
CREATE TABLE `heads`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of heads
-- ----------------------------
INSERT INTO `heads` VALUES (1, 'JAMES PAUL R. SANTIAGO', 'ADMINISTRATIVE OFFICER IV', '2023-11-03 22:50:11', '2023-11-14 06:18:07', NULL);
INSERT INTO `heads` VALUES (2, 'FLORDELIZA C. GECOBE PHD, CESO V', 'SCHOOLS DIVISION SUPERINTENDENT', '2023-11-03 22:50:49', '2023-11-19 01:05:18', NULL);
INSERT INTO `heads` VALUES (4, 'MELANIE O. SO', 'ADMINISTRATIVE OFFICER IV', '2023-11-14 06:18:30', '2023-11-14 06:18:30', NULL);
INSERT INTO `heads` VALUES (5, 'RICARDO Q. CABUGAO JR.', 'INFORMATION TECHNOLOGY OFFICER I', '2023-11-14 06:18:57', '2023-11-14 06:18:57', NULL);
INSERT INTO `heads` VALUES (6, 'JONELL C. MANGULABNAN', 'ADMINISTRATIVE OFFICER II', '2023-11-14 06:21:47', '2023-11-14 06:21:47', NULL);
INSERT INTO `heads` VALUES (7, 'LEONIDA F. CULANG MPA', 'ADMINISTRATIVE OFFICER V', '2023-11-14 06:22:15', '2023-11-14 06:22:15', NULL);
INSERT INTO `heads` VALUES (8, 'CHERY ANN R. SEGUNDO CPA', 'ACCOUNTANT II', '2023-11-14 06:22:39', '2023-11-14 06:22:39', NULL);
INSERT INTO `heads` VALUES (9, 'JONATHAN A. FRONDA PHD, CESO VI', 'ASSISTANT SCHOOLS DIVISION SUPERINTENDENT', '2023-11-14 06:23:51', '2023-11-14 06:23:51', NULL);

-- ----------------------------
-- Table structure for inspection_items
-- ----------------------------
DROP TABLE IF EXISTS `inspection_items`;
CREATE TABLE `inspection_items`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `inspection_id` int UNSIGNED NOT NULL,
  `item_id` int UNSIGNED NOT NULL,
  `quantity` double NOT NULL,
  `cost` double NOT NULL,
  `total` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `inspection_items to items`(`item_id` ASC) USING BTREE,
  INDEX `inspection_items to inspections`(`inspection_id` ASC) USING BTREE,
  CONSTRAINT `inspection_items to inspections` FOREIGN KEY (`inspection_id`) REFERENCES `inspections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inspection_items
-- ----------------------------
INSERT INTO `inspection_items` VALUES (1, 1, 2, 1, 169, 169, '2023-11-24 02:34:30', '2023-11-24 02:34:30', NULL);
INSERT INTO `inspection_items` VALUES (2, 1, 4, 1, 8595, 8595, '2023-11-24 02:34:30', '2023-11-24 02:34:30', NULL);
INSERT INTO `inspection_items` VALUES (3, 1, 2, 1, 169, 169, '2023-11-24 07:08:59', '2023-11-24 07:08:59', NULL);
INSERT INTO `inspection_items` VALUES (4, 1, 4, 1, 8595, 8595, '2023-11-24 07:08:59', '2023-11-24 07:08:59', NULL);
INSERT INTO `inspection_items` VALUES (5, 1, 2, 1, 169, 169, '2023-11-24 07:09:07', '2023-11-24 07:09:07', NULL);
INSERT INTO `inspection_items` VALUES (6, 1, 4, 1, 8595, 8595, '2023-11-24 07:09:07', '2023-11-24 07:09:07', NULL);
INSERT INTO `inspection_items` VALUES (7, 2, 4, 1, 8595, 8595, '2023-11-24 08:16:50', '2023-11-24 08:16:50', NULL);
INSERT INTO `inspection_items` VALUES (8, 2, 5, 1, 3500, 3500, '2023-11-24 08:16:50', '2023-11-24 08:16:50', NULL);
INSERT INTO `inspection_items` VALUES (9, 3, 2, 1, 169, 169, '2023-11-24 08:18:59', '2023-11-24 08:18:59', NULL);
INSERT INTO `inspection_items` VALUES (10, 3, 3, 1, 365, 365, '2023-11-24 08:18:59', '2023-11-24 08:18:59', NULL);

-- ----------------------------
-- Table structure for inspections
-- ----------------------------
DROP TABLE IF EXISTS `inspections`;
CREATE TABLE `inspections`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` int UNSIGNED NOT NULL,
  `order_id` int UNSIGNED NOT NULL,
  `fund_cluster_id` int UNSIGNED NOT NULL,
  `supplier_id` int UNSIGNED NOT NULL,
  `office_id` int UNSIGNED NOT NULL,
  `iar_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_compete` tinyint NOT NULL DEFAULT 0,
  `is_inspected` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `inspections to requests`(`request_id` ASC) USING BTREE,
  INDEX `inspections to orders`(`order_id` ASC) USING BTREE,
  INDEX `inspections to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `inspections to suppliers`(`supplier_id` ASC) USING BTREE,
  INDEX `inspections to offices`(`office_id` ASC) USING BTREE,
  CONSTRAINT `inspections to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `inspections to suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inspections
-- ----------------------------
INSERT INTO `inspections` VALUES (1, 2, 1, 2, 6, 7, '2023-11-0001', 1, 1, '2023-11-28 02:34:30', '2023-11-24 07:09:07', NULL);
INSERT INTO `inspections` VALUES (2, 4, 3, 1, 6, 7, '2023-11-0003', 1, 1, '2023-11-24 08:16:50', '2023-11-24 08:16:50', NULL);
INSERT INTO `inspections` VALUES (3, 5, 4, 3, 4, 7, '2023-11-0004', 1, 1, '2023-11-26 08:18:59', '2023-11-24 08:18:59', NULL);

-- ----------------------------
-- Table structure for inventories
-- ----------------------------
DROP TABLE IF EXISTS `inventories`;
CREATE TABLE `inventories`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iar_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of inventories
-- ----------------------------
INSERT INTO `inventories` VALUES (6, '2023-10-15 10:07:40', 0);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category_id` int UNSIGNED NOT NULL,
  `sub_category_id` int UNSIGNED NOT NULL,
  `unit_id` int UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `stock` double NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `items to categories`(`category_id` ASC) USING BTREE,
  INDEX `items to sub_categories`(`sub_category_id` ASC) USING BTREE,
  INDEX `items to units`(`unit_id` ASC) USING BTREE,
  CONSTRAINT `items to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to sub_categories` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `items to units` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (2, 'A4 Bond Paper', 1, 1, 3, 169, 22, 'SPLV', '2023-11-18 22:47:36', '2023-11-26 01:31:26', NULL);
INSERT INTO `items` VALUES (3, 'Epson Ink 003', 1, 1, 2, 365, 18, 'SPLV', '2023-11-18 22:52:44', '2023-11-26 01:31:26', NULL);
INSERT INTO `items` VALUES (4, 'Epson L3210', 2, 7, 12, 8595, 23, 'SPHV', '2023-11-18 22:53:58', '2023-11-26 01:31:26', NULL);
INSERT INTO `items` VALUES (5, 'Fingerprint Scanner', 2, 7, 12, 3500, 0, 'SPLV', '2023-11-23 03:46:08', '2023-11-26 01:31:26', NULL);
INSERT INTO `items` VALUES (6, 'Asda', 2, 6, 2, 0.5, 0, 'SPLV', '2023-11-28 16:34:06', '2023-11-28 16:34:06', NULL);

-- ----------------------------
-- Table structure for methods
-- ----------------------------
DROP TABLE IF EXISTS `methods`;
CREATE TABLE `methods`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of methods
-- ----------------------------
INSERT INTO `methods` VALUES (1, 'CHECK', '2023-11-08 08:04:58', '2023-11-08 08:04:58', NULL);
INSERT INTO `methods` VALUES (2, 'CASH', '2023-11-08 08:05:26', '2023-11-08 08:05:26', NULL);

-- ----------------------------
-- Table structure for offices
-- ----------------------------
DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of offices
-- ----------------------------
INSERT INTO `offices` VALUES (7, 'SDO - Santiago City', '2023-11-12 13:28:57', '2023-11-12 13:28:57', NULL);

-- ----------------------------
-- Table structure for orderdetails
-- ----------------------------
DROP TABLE IF EXISTS `orderdetails`;
CREATE TABLE `orderdetails`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int UNSIGNED NOT NULL,
  `item_id` int UNSIGNED NOT NULL,
  `cost` double NOT NULL,
  `qty` int NOT NULL,
  `total` double NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `orderdetails to orders`(`order_id` ASC) USING BTREE,
  INDEX `orderdetails to items`(`item_id` ASC) USING BTREE,
  CONSTRAINT `orderdetails to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orderdetails to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orderdetails
-- ----------------------------
INSERT INTO `orderdetails` VALUES (1, 1, 2, 169, 1, 169, '2023-11-23 18:34:06', '2023-11-23 18:34:06');
INSERT INTO `orderdetails` VALUES (2, 1, 4, 8595, 1, 8595, '2023-11-23 18:34:06', '2023-11-23 18:34:06');
INSERT INTO `orderdetails` VALUES (3, 2, 2, 169, 1, 169, '2023-11-23 23:01:54', '2023-11-23 23:01:54');
INSERT INTO `orderdetails` VALUES (4, 2, 3, 365, 1, 365, '2023-11-23 23:01:54', '2023-11-23 23:01:54');
INSERT INTO `orderdetails` VALUES (5, 3, 4, 8595, 1, 8595, '2023-11-24 00:14:17', '2023-11-24 00:14:17');
INSERT INTO `orderdetails` VALUES (6, 3, 5, 3500, 1, 3500, '2023-11-24 00:14:17', '2023-11-24 00:14:17');
INSERT INTO `orderdetails` VALUES (7, 4, 2, 169, 1, 169, '2023-11-24 00:18:56', '2023-11-24 00:18:56');
INSERT INTO `orderdetails` VALUES (8, 4, 3, 365, 1, 365, '2023-11-24 00:18:56', '2023-11-24 00:18:56');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` int UNSIGNED NOT NULL,
  `office_id` int UNSIGNED NOT NULL,
  `supplier_id` int UNSIGNED NOT NULL,
  `fund_cluster_id` int UNSIGNED NOT NULL,
  `method_id` int UNSIGNED NOT NULL,
  `po_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `place_of_delivery` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `orders to requests`(`request_id` ASC) USING BTREE,
  INDEX `orders to offices`(`office_id` ASC) USING BTREE,
  INDEX `orders to suppliers`(`supplier_id` ASC) USING BTREE,
  INDEX `orders to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `orders to methods`(`method_id` ASC) USING BTREE,
  CONSTRAINT `orders to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to methods` FOREIGN KEY (`method_id`) REFERENCES `methods` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orders to suppliers` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 2, 7, 6, 2, 2, '2023-11-0001', 'SDO - SANTIAGO CITY', '2023-11-23 18:34:06', '2023-11-23 18:34:06', NULL);
INSERT INTO `orders` VALUES (2, 3, 7, 4, 3, 2, '2023-11-0002', 'SDO - SANTIAGO CITY', '2023-11-23 23:01:54', '2023-11-23 23:01:54', NULL);
INSERT INTO `orders` VALUES (3, 4, 7, 6, 1, 2, '2023-11-0003', 'SDO - SANTIAGO CITY', '2023-11-24 00:14:17', '2023-11-24 00:14:17', NULL);
INSERT INTO `orders` VALUES (4, 5, 7, 4, 3, 2, '2023-11-0004', 'SDO - SANTIAGO CITY', '2023-11-24 00:18:56', '2023-11-24 00:18:56', NULL);

-- ----------------------------
-- Table structure for parts
-- ----------------------------
DROP TABLE IF EXISTS `parts`;
CREATE TABLE `parts`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `plan_id` int UNSIGNED NOT NULL,
  `part` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parts to plans`(`plan_id` ASC) USING BTREE,
  CONSTRAINT `parts to plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of parts
-- ----------------------------
INSERT INTO `parts` VALUES (5, 1, 'PART 1\r\n', '2023-11-19 23:50:54', '2023-11-19 23:52:20', NULL);
INSERT INTO `parts` VALUES (8, 2, 'PART 1. AVAILABLE AT PS DBM', '2023-11-20 00:09:40', '2023-11-20 00:09:40', NULL);
INSERT INTO `parts` VALUES (9, 2, 'ALCOHOL OR ACETORE', '2023-11-20 00:09:54', '2023-11-20 00:09:54', NULL);

-- ----------------------------
-- Table structure for plan_titles
-- ----------------------------
DROP TABLE IF EXISTS `plan_titles`;
CREATE TABLE `plan_titles`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `plan_id` int UNSIGNED NOT NULL,
  `part_id` int UNSIGNED NOT NULL,
  `plan_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `plan_title to plans`(`plan_id` ASC) USING BTREE,
  INDEX `plan_titles to parts`(`part_id` ASC) USING BTREE,
  CONSTRAINT `plan_title to plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plan_titles to parts` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plan_titles
-- ----------------------------
INSERT INTO `plan_titles` VALUES (1, 1, 5, 'asdasda', '2023-11-20 00:05:16', '2023-11-20 00:05:16', NULL);
INSERT INTO `plan_titles` VALUES (2, 2, 8, 'ALCOHOL OR ACETORE', '2023-11-20 00:10:35', '2023-11-20 17:19:40', NULL);
INSERT INTO `plan_titles` VALUES (3, 2, 8, 'Furnitures', '2023-11-20 17:20:10', '2023-11-20 17:20:10', NULL);
INSERT INTO `plan_titles` VALUES (4, 1, 5, 'ASDASDASDDD', '2023-11-27 21:41:05', '2023-11-27 21:41:05', NULL);

-- ----------------------------
-- Table structure for plandetails
-- ----------------------------
DROP TABLE IF EXISTS `plandetails`;
CREATE TABLE `plandetails`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `plan_id` int UNSIGNED NOT NULL,
  `plan_title_id` int UNSIGNED NOT NULL,
  `part_id` int UNSIGNED NOT NULL,
  `code` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `item_id` int NOT NULL,
  `qty` int NULL DEFAULT NULL,
  `cost` double NULL DEFAULT NULL,
  `total` double NULL DEFAULT NULL,
  `jan` int NULL DEFAULT NULL,
  `feb` int NULL DEFAULT NULL,
  `mar` int NULL DEFAULT NULL,
  `apr` int NULL DEFAULT NULL,
  `may` int NULL DEFAULT NULL,
  `jun` int NULL DEFAULT NULL,
  `jul` int NULL DEFAULT NULL,
  `aug` int NULL DEFAULT NULL,
  `sep` int NULL DEFAULT NULL,
  `oct` int NULL DEFAULT NULL,
  `nov` int NULL DEFAULT NULL,
  `decm` int NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `plandetails to plans`(`plan_id` ASC) USING BTREE,
  INDEX `plandetails to plan_titles`(`plan_title_id` ASC) USING BTREE,
  INDEX `plandetails to parts`(`part_id` ASC) USING BTREE,
  CONSTRAINT `plandetails to parts` FOREIGN KEY (`part_id`) REFERENCES `parts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plandetails to plan_titles` FOREIGN KEY (`plan_title_id`) REFERENCES `plan_titles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `plandetails to plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plandetails
-- ----------------------------
INSERT INTO `plandetails` VALUES (1, 2, 2, 8, '44', 3, 22, 365, 8030, 2, 2, 2, 2, 2, 3, 2, 2, 1, 1, 2, 1, '2023-11-20 13:09:19', '2023-11-20 21:09:19', NULL);
INSERT INTO `plandetails` VALUES (2, 1, 1, 5, '12121', 3, 1, 365, 365, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2023-11-27 21:40:52', '2023-11-27 21:40:52', NULL);
INSERT INTO `plandetails` VALUES (3, 1, 4, 5, 'qwq', 4, 5, 8595, 42975, 1, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, '2023-11-27 14:50:48', '2023-11-27 22:50:48', NULL);
INSERT INTO `plandetails` VALUES (4, 1, 1, 5, 'asdsda', 4, 1, 8595, 8595, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2023-11-27 21:43:46', '2023-11-27 21:43:46', NULL);

-- ----------------------------
-- Table structure for plans
-- ----------------------------
DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `prepared_by` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of plans
-- ----------------------------
INSERT INTO `plans` VALUES (1, 'Furnitired', 'Asda', 'Johndoe', 'Head', 1, '2023-11-19 23:24:27', '2023-11-24 03:41:08', NULL);
INSERT INTO `plans` VALUES (2, 'Fhdsserht', 'MOOE', 'Jepot Santiago', 'Administrative Officer IV', 0, '2023-11-20 00:08:01', '2023-11-20 00:08:01', NULL);

-- ----------------------------
-- Table structure for ranges
-- ----------------------------
DROP TABLE IF EXISTS `ranges`;
CREATE TABLE `ranges`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` double NOT NULL,
  `price_to` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ranges
-- ----------------------------
INSERT INTO `ranges` VALUES (1, 'SPHV', 5001, 50000, '2023-11-18 22:14:55', '2023-11-18 22:22:31', NULL);
INSERT INTO `ranges` VALUES (2, 'SPLV', 1, 5000, '2023-11-18 22:22:50', '2023-11-18 22:22:50', NULL);

-- ----------------------------
-- Table structure for request_item_details
-- ----------------------------
DROP TABLE IF EXISTS `request_item_details`;
CREATE TABLE `request_item_details`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_item_id` int UNSIGNED NOT NULL,
  `request_item_series_id` int UNSIGNED NULL DEFAULT NULL,
  `item_id` int UNSIGNED NOT NULL,
  `cost` double NOT NULL,
  `qty` int NOT NULL,
  `total` int NOT NULL,
  `is_approved` tinyint NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requestitems to requests`(`request_item_id` ASC) USING BTREE,
  INDEX `requestitems to items`(`item_id` ASC) USING BTREE,
  INDEX `request_items_details to request_detail_serieses`(`request_item_series_id` ASC) USING BTREE,
  CONSTRAINT `request_item_details to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `request_item_details to request_items` FOREIGN KEY (`request_item_id`) REFERENCES `request_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `request_items_details to request_detail_serieses` FOREIGN KEY (`request_item_series_id`) REFERENCES `request_item_serieses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request_item_details
-- ----------------------------
INSERT INTO `request_item_details` VALUES (1, 1, 1, 2, 169, 1, 169, 1, '2023-11-25 17:15:13', '2023-11-25 17:30:48', NULL);
INSERT INTO `request_item_details` VALUES (2, 1, 1, 3, 365, 1, 365, 1, '2023-11-25 17:15:13', '2023-11-25 17:30:48', NULL);
INSERT INTO `request_item_details` VALUES (3, 1, 2, 4, 8595, 1, 8595, 1, '2023-11-25 17:15:13', '2023-11-25 17:30:48', NULL);
INSERT INTO `request_item_details` VALUES (4, 1, 2, 5, 3500, 1, 3500, 1, '2023-11-25 17:15:13', '2023-11-25 17:30:48', NULL);

-- ----------------------------
-- Table structure for request_item_serieses
-- ----------------------------
DROP TABLE IF EXISTS `request_item_serieses`;
CREATE TABLE `request_item_serieses`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_item_id` int UNSIGNED NOT NULL,
  `series_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_expendable` tinyint NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `request_items`(`request_item_id` ASC) USING BTREE,
  CONSTRAINT `request_items` FOREIGN KEY (`request_item_id`) REFERENCES `request_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request_item_serieses
-- ----------------------------
INSERT INTO `request_item_serieses` VALUES (1, 1, '2023-11-0001', 1, '2023-11-26 01:15:13', '2023-11-25 17:15:13', NULL);
INSERT INTO `request_item_serieses` VALUES (2, 1, '2023-11-0001', 0, '2023-11-26 01:15:13', '2023-11-25 17:15:13', NULL);

-- ----------------------------
-- Table structure for request_items
-- ----------------------------
DROP TABLE IF EXISTS `request_items`;
CREATE TABLE `request_items`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `office_id` int UNSIGNED NOT NULL,
  `department_id` int UNSIGNED NOT NULL,
  `series_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests to users`(`user_id` ASC) USING BTREE,
  INDEX `requests to offices`(`office_id` ASC) USING BTREE,
  INDEX `requests to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `request_items to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `request_items to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `request_items to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of request_items
-- ----------------------------
INSERT INTO `request_items` VALUES (1, 21, 7, 1, '0', 'ASDAS', 3, '2023-11-25 17:15:13', '2023-11-25 17:31:26', NULL);

-- ----------------------------
-- Table structure for requestdetails
-- ----------------------------
DROP TABLE IF EXISTS `requestdetails`;
CREATE TABLE `requestdetails`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` int UNSIGNED NOT NULL,
  `item_id` int UNSIGNED NOT NULL,
  `cost` double NOT NULL,
  `qty` int NOT NULL,
  `total` int NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requestitems to requests`(`request_id` ASC) USING BTREE,
  INDEX `requestitems to items`(`item_id` ASC) USING BTREE,
  CONSTRAINT `requestitems to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requestitems to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requestdetails
-- ----------------------------
INSERT INTO `requestdetails` VALUES (5, 2, 2, 169, 1, 169, '2023-11-22 22:17:58', '2023-11-22 22:17:58', NULL);
INSERT INTO `requestdetails` VALUES (6, 2, 4, 8595, 1, 8595, '2023-11-22 22:17:58', '2023-11-22 22:17:58', NULL);
INSERT INTO `requestdetails` VALUES (7, 3, 2, 169, 1, 169, '2023-11-23 22:55:07', '2023-11-23 22:55:07', NULL);
INSERT INTO `requestdetails` VALUES (8, 3, 3, 365, 1, 365, '2023-11-23 22:55:07', '2023-11-23 22:55:07', NULL);
INSERT INTO `requestdetails` VALUES (9, 4, 4, 8595, 1, 8595, '2023-11-23 23:12:24', '2023-11-23 23:12:24', NULL);
INSERT INTO `requestdetails` VALUES (10, 4, 5, 3500, 1, 3500, '2023-11-23 23:12:24', '2023-11-23 23:12:24', NULL);
INSERT INTO `requestdetails` VALUES (11, 5, 2, 169, 1, 169, '2023-11-24 00:08:20', '2023-11-24 00:08:20', NULL);
INSERT INTO `requestdetails` VALUES (12, 5, 3, 365, 1, 365, '2023-11-24 00:08:20', '2023-11-24 00:08:20', NULL);

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int UNSIGNED NOT NULL,
  `office_id` int UNSIGNED NOT NULL,
  `fund_cluster_id` int UNSIGNED NOT NULL,
  `department_id` int UNSIGNED NOT NULL,
  `series_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `purpose` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` int NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests to users`(`user_id` ASC) USING BTREE,
  INDEX `requests to offices`(`office_id` ASC) USING BTREE,
  INDEX `requests to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `requests to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `requests to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES (2, 18, 7, 2, 10, '2023-11-0001', 'Gfmjxyhk', 1, '2023-11-22 22:17:58', '2023-11-23 18:34:06', NULL);
INSERT INTO `requests` VALUES (3, 24, 7, 3, 2, '2023-11-0002', 'Asdas', 1, '2023-11-23 22:55:07', '2023-11-23 23:01:54', NULL);
INSERT INTO `requests` VALUES (4, 24, 7, 1, 1, '2023-11-0003', 'Asda', 1, '2023-11-23 23:12:24', '2023-11-24 00:14:17', NULL);
INSERT INTO `requests` VALUES (5, 24, 7, 3, 2, '2023-11-0004', 'Asda', 1, '2023-11-24 00:08:20', '2023-11-24 00:18:56', NULL);

-- ----------------------------
-- Table structure for requisition_items
-- ----------------------------
DROP TABLE IF EXISTS `requisition_items`;
CREATE TABLE `requisition_items`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `requisition_id` int UNSIGNED NOT NULL,
  `item_id` int UNSIGNED NOT NULL,
  `is_available` tinyint NOT NULL DEFAULT 1,
  `quantity` double NOT NULL,
  `cost` double NOT NULL,
  `total` double NOT NULL,
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requisition_items to requisitions`(`requisition_id` ASC) USING BTREE,
  INDEX `requisition_items to items`(`item_id` ASC) USING BTREE,
  CONSTRAINT `requisition_items to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisition_items to requisitions` FOREIGN KEY (`requisition_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requisition_items
-- ----------------------------
INSERT INTO `requisition_items` VALUES (2, 2, 2, 1, 1, 169, 169, 'COMPLETE', '2023-11-24 07:10:50', '2023-11-24 07:11:18', NULL);
INSERT INTO `requisition_items` VALUES (3, 2, 4, 0, 1, 8595, 8595, 'COMPLETE', '2023-11-24 07:10:50', '2023-11-24 07:10:50', NULL);
INSERT INTO `requisition_items` VALUES (4, 3, 4, 1, 1, 8595, 8595, 'COMPLETE', '2023-11-24 08:16:55', '2023-11-24 08:16:55', NULL);
INSERT INTO `requisition_items` VALUES (5, 3, 5, 1, 1, 3500, 3500, 'COMPLETE', '2023-11-24 08:16:55', '2023-11-24 08:16:55', NULL);
INSERT INTO `requisition_items` VALUES (6, 4, 2, 0, 1, 169, 169, 'COMPLETE', '2023-11-24 08:19:03', '2023-11-24 08:19:03', NULL);
INSERT INTO `requisition_items` VALUES (7, 4, 3, 0, 1, 365, 365, 'COMPLETE', '2023-11-24 08:19:03', '2023-11-24 08:19:03', NULL);

-- ----------------------------
-- Table structure for requisitions
-- ----------------------------
DROP TABLE IF EXISTS `requisitions`;
CREATE TABLE `requisitions`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` int UNSIGNED NOT NULL,
  `inspection_id` int UNSIGNED NOT NULL,
  `order_id` int UNSIGNED NOT NULL,
  `fund_cluster_id` int UNSIGNED NOT NULL,
  `office_id` int UNSIGNED NOT NULL,
  `ris_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requisitions to requests`(`request_id` ASC) USING BTREE,
  INDEX `requisitions to orders`(`order_id` ASC) USING BTREE,
  INDEX `requisitions to fund_clusters`(`fund_cluster_id` ASC) USING BTREE,
  INDEX `requisitions to offices`(`office_id` ASC) USING BTREE,
  INDEX `requisitions to inspections`(`inspection_id` ASC) USING BTREE,
  CONSTRAINT `requisitions to fund_clusters` FOREIGN KEY (`fund_cluster_id`) REFERENCES `fund_clusters` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to inspections` FOREIGN KEY (`inspection_id`) REFERENCES `inspections` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to offices` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requisitions to requests` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requisitions
-- ----------------------------
INSERT INTO `requisitions` VALUES (2, 2, 1, 1, 2, 7, '2023-11-0001', '2023-11-24 07:10:50', '2023-11-24 07:11:18', NULL);
INSERT INTO `requisitions` VALUES (3, 4, 2, 3, 1, 7, '2023-11-0003', '2023-11-24 08:16:55', '2023-11-24 08:16:55', NULL);
INSERT INTO `requisitions` VALUES (4, 5, 3, 4, 3, 7, '2023-11-0004', '2023-11-24 08:19:03', '2023-11-24 08:19:03', NULL);

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int UNSIGNED NOT NULL,
  `description` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` double NOT NULL,
  `last_stocks` double NOT NULL,
  `qty` double NOT NULL,
  `total_stocks` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stocks to items`(`item_id` ASC) USING BTREE,
  CONSTRAINT `stocks to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stocks
-- ----------------------------
INSERT INTO `stocks` VALUES (1, 2, 'RELEASED (OUT)', 0, 23, 1, 22, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (2, 3, 'RELEASED (OUT)', 0, 19, 1, 18, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (3, 4, 'RELEASED (OUT)', 0, 24, 1, 23, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (4, 5, 'RELEASED (OUT)', 0, 1, 1, 0, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);

-- ----------------------------
-- Table structure for sub_categories
-- ----------------------------
DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE `sub_categories`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sub_categories to categories`(`category_id` ASC) USING BTREE,
  CONSTRAINT `sub_categories to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sub_categories
-- ----------------------------
INSERT INTO `sub_categories` VALUES (1, 1, 'Office Supply', '2023-11-18 20:54:34', '2023-11-18 21:01:10', NULL);
INSERT INTO `sub_categories` VALUES (3, 1, 'Common Electrical Supplies', '2023-11-18 21:03:15', '2023-11-18 21:03:15', NULL);
INSERT INTO `sub_categories` VALUES (4, 1, 'Medical Supplies', '2023-11-18 21:03:27', '2023-11-18 21:03:27', NULL);
INSERT INTO `sub_categories` VALUES (5, 1, 'Other Supplies', '2023-11-18 21:03:38', '2023-11-18 21:03:38', NULL);
INSERT INTO `sub_categories` VALUES (6, 2, 'Office Equipment', '2023-11-18 21:04:20', '2023-11-18 21:04:20', NULL);
INSERT INTO `sub_categories` VALUES (7, 2, 'IT Equipment', '2023-11-18 21:04:28', '2023-11-18 21:04:28', NULL);
INSERT INTO `sub_categories` VALUES (8, 2, 'Fixtures & Furnitures', '2023-11-18 21:04:52', '2023-11-18 21:04:52', NULL);

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tin_no` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES (1, 'SISTAN SUPPLIES DEPOT', 'Santiago City', '941-104-334-000', '2023-11-12 13:22:40', '2023-11-12 13:22:40', NULL);
INSERT INTO `suppliers` VALUES (4, 'LJ SPEED ENTERPRISES, BUILDERS & CONSTRUCTION SUPPLY', 'Santiago City', '339-453-188-001', '2023-11-19 01:00:47', '2023-11-18 17:00:47', NULL);
INSERT INTO `suppliers` VALUES (5, 'ROANNE 888 GLASSWARE CORP', 'Santiago City', '762-810-689-000', '2023-11-19 01:02:28', '2023-11-18 17:02:28', NULL);
INSERT INTO `suppliers` VALUES (6, 'FARMACIA NAVARRO, INC.', 'Santiago City', '004-062-346-000', '2023-11-19 01:04:03', '2023-11-18 17:04:03', NULL);

-- ----------------------------
-- Table structure for units
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES (1, 'pc', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (2, 'bottle', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (3, 'ream', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (4, 'box', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (5, 'liter', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (6, 'pack', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (7, 'person', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (8, 'roll', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (9, 'set', '2023-11-12 13:04:49', '2023-11-12 13:04:49', NULL);
INSERT INTO `units` VALUES (10, 'Tubes', '2023-11-12 13:04:49', '2023-11-12 21:06:20', NULL);
INSERT INTO `units` VALUES (11, 'Kg', '2023-11-12 21:06:28', '2023-11-12 21:06:28', NULL);
INSERT INTO `units` VALUES (12, 'Unit', '2023-11-18 22:53:28', '2023-11-18 22:53:28', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bdate` date NOT NULL,
  `tin_no` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `plantilla_no` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `position` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `contact` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `department_id` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (16, 'jepotsantiago', '$2y$10$GPu/Gmow06mG/EajJKHDpeRAtdBN1R4MLBYRYxFMB1PvgB4ujw9cq', 'James Paul Ronquillo Santiago', '2023-10-08', '010101010101', '0987654321', 'Administrative Officer IV', 'Super Administrator', 1, '656533ec2651599999999656533ec26528', '09338146244', 2, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);
INSERT INTO `users` VALUES (18, 'jeffcalaunan', '$2y$10$sReXyQIwA/o3jm2nwFxKhuCa2qQhEaOr5y1km.3dGlwBnz0xpUEji', 'Marc Jefferson Calaunan', '1996-03-21', '346686124', 'ADASII-2023-0014', 'Administrative Aide VI', 'Super Administrator', 1, '655f35b9581e999999999655f35b9581f3', '09173191479', 3, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);
INSERT INTO `users` VALUES (20, 'archietzy', '$2y$10$GpPmWoXu32tttFTYld4abegfA2p1CA/g6PRY/desYnAW7d0Y1F6AK', 'Archie Raven Molina Raga', '2002-05-26', '122142342342', '342344634234', 'Student', 'User', 1, '65619748ec54d9999999965619748ec557', '09452247731', 3, '2023-11-12 14:51:53', '2023-11-12 14:51:53', NULL);
INSERT INTO `users` VALUES (21, 'gianromero', '$2y$10$W6.JVXgamzhsLnechH25FOAKbG7J44E4OTP.J8amVSQ30ipv1nHde', 'Gian Samuel Romero', '2002-05-26', '111111111111', 'ADASII-2023-018', 'Administrative Officer II', 'User', 1, '65643d2b7748f9999999965643d2b7749a', '09050240221', 8, '2023-11-18 16:16:51', '2023-11-18 16:16:51', NULL);
INSERT INTO `users` VALUES (22, 'loidahermosura', '$2y$10$VJ0vGdjtne5Q2/IpZJXAKulCH4XouGEVLbEgxoXkivyyz2Ev3p7ke', 'Loida Flores Hermosura', '2023-11-23', '135549831', 'ADASII-2023-018', 'Administrative Officer V', 'Super Administrator', 1, '655f370fcba8299999999655f370fcba8b', '09173191479', 1, '2023-11-23 19:26:10', '2023-11-23 19:26:10', NULL);
INSERT INTO `users` VALUES (23, 'jheasvdr', '$2y$10$woL3Xq3.0wtg7BhYJM3fVee8s3BriKPhySRbcf.5P15bxUWO.AqVK', 'Jhea Jay Edullantes Saavedra', '2002-05-28', '12904313233', 'ADASII-2023-018', 'Administrative Aide VI', 'Administrator', 1, NULL, '09050240221', 2, '2023-11-23 20:26:44', '2023-11-23 20:26:44', NULL);
INSERT INTO `users` VALUES (24, 'admin', '$2y$10$J6qVLmDDIICtbv7u5kFace92NqAb83RYKi3u01cdUH8.j87pNvBWS', 'admin', '2023-11-23', 'asda', 'asda', 'asda', 'Administrator', 1, '6563ffca6b29c999999996563ffca6b2a6', 'asdasd', 2, '2023-11-23 20:31:03', '2023-11-23 20:31:03', NULL);
INSERT INTO `users` VALUES (25, 'imeepilien', '$2y$10$2EAtx9JpMn.aBB4O6kNG0OPfMOAElwq4G640O7KLoWY1//Q6g06S6', 'Imee Marie Pilien', '2023-11-15', '2154313256', 'ADASII-2023-018', 'Administrative Assistant III', 'Administrator', 1, '656175563f6c899999999656175563f6d4', '09173191479', 3, '2023-11-23 20:56:55', '2023-11-23 20:56:55', NULL);

SET FOREIGN_KEY_CHECKS = 1;
