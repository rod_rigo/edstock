/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : edstock_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 28/11/2023 14:39:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for stocks
-- ----------------------------
DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int UNSIGNED NOT NULL,
  `description` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `price` double NOT NULL,
  `last_stocks` double NOT NULL,
  `qty` double NOT NULL,
  `total_stocks` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stocks to items`(`item_id` ASC) USING BTREE,
  CONSTRAINT `stocks to items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stocks
-- ----------------------------
INSERT INTO `stocks` VALUES (1, 2, 'RELEASED (OUT)', 0, 23, 1, 22, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (2, 3, 'RELEASED (OUT)', 0, 19, 1, 18, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (3, 4, 'RELEASED (OUT)', 0, 24, 1, 23, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);
INSERT INTO `stocks` VALUES (4, 5, 'RELEASED (OUT)', 0, 1, 1, 0, '2023-11-26 01:31:26', '2023-11-26 01:31:26', NULL);

SET FOREIGN_KEY_CHECKS = 1;
